<?php
namespace payment\alipay;
use \Payment\Contracts\IPayNotify;

class callback implements IPayNotify
{
	//处理返回回调callback
	public function handle(string $channel, string $notifyType, string $notifyWay, array $notifyData)
	{
        $config = array('tradeid'=>$notifyData['trade_no'],'paymoney'=>$notifyData['total_amount'],'orderid'=>$notifyData['out_trade_no'] );
        $notify = new \payment\notify();
		$notify->run($config);
        return true;
	}
	
}