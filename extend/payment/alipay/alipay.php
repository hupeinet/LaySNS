<?php

namespace payment\alipay;

use Exception;
use InvalidArgumentException;


class alipay
{
    //处理请求
    public function submit($payconfig, $params)
    {
        $overtime=!@$payconfig['overtime']?300:$payconfig['overtime'];
        $config = [
            'app_id' => $payconfig['app_id'],
            'sign_type' => 'RSA2',
            'ali_public_key' => $payconfig['ali_public_key'],
            'rsa_private_key' => $payconfig['rsa_private_key'],
            'return_url' => $params['return_url'],
            'notify_url' => $params['notify_url'],
            'fee_type' => 'CNY',
            'limit_pay' => [

            ]
        ];

        $payData = [

            'body'         => 'ali qr pay',
            'subject'      => '测试支付宝扫码支付',
            'trade_no'     => $params['order_no'],
            'time_expire'  => time() + 1000, // 表示必须 1000s 内付款
            'amount'       =>  $params['amount']
        ];

        try {
            $client = new \Payment\Client(\Payment\Client::ALIPAY, $config);
            $result = $client->pay(\Payment\Client::ALI_CHANNEL_QR, $payData);
            if (isset($result['qr_code'])) {
                $subjump = 0;
                $url = $result['qr_code'];
            }else{
                $subjump = 1;
                $url = $result;
            }

            $result = array(
                'paymethod' => 'alipay',
                'payname' => '支付宝支付',
                'type' => 0, //1 手机 0 电脑
                'subjump' => $subjump,
                'url' => $url,
                'subjumpurl' => $url,
                'overtime' => $overtime,
                'money' => $params['amount']
            );

            return array('code' => 1, 'msg' => 'success', 'data' => $result);

        } catch (InvalidArgumentException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (\Payment\Exceptions\GatewayException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (\Payment\Exceptions\ClassNotFoundException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (Exception $e) {
            return array('code' => 1000, 'msg' => $e->getMessage(), 'data' => '');
        }
    }

    public function notify($payconfig)
    {
        try {
            file_put_contents(YEWU_FILE, CUR_DATETIME . '-支付宝付款回调' . json_encode($_POST) . PHP_EOL, FILE_APPEND);
            $config = [
                'use_sandbox' => false,
                'app_id' => $payconfig['app_id'],
                'sign_type' => 'RSA2',
                'ali_public_key' => $payconfig['ali_public_key'],
                'rsa_private_key' => $payconfig['rsa_private_key'],
                'return_raw' => true
            ];
            $callback = new callback();
            $client = new \Payment\Client(\Payment\Client::ALIPAY, $config);
            $ret=$client->notify($callback);

        } catch (\Exception $e) {
            file_put_contents(YEWU_FILE, CUR_DATETIME . '支付宝回调错误-' . $e->getMessage() . PHP_EOL, FILE_APPEND);
            exit;
        }
        file_put_contents(YEWU_FILE, CUR_DATETIME . '支付宝回调返回结果：' . $ret . PHP_EOL, FILE_APPEND);

        return $ret;
    }
}