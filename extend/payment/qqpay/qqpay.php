<?php
namespace payment\qqpay;

class qqpay
{
    const API_URL_PREFIX = 'https://qpay.qq.com';                                       //接口API URL前缀
    const UNIFIEDORDER_URL = "/cgi-bin/pay/qpay_unified_order.cgi";                     //下单地址URL
    const ORDERQUERY_URL = "/cgi-bin/pay/qpay_order_query.cgi";                         //查询订单URL
    const CLOSEORDER_URL = "/cgi-bin/pay/qpay_close_order.cgi";                         //关闭订单URL
    const TRANSFERS_URL = "https://api.qpay.qq.com/cgi-bin/epay/qpay_epay_b2c.cgi";     //企业付款到零钱
    const REFUND_URL = "https://api.qpay.qq.com/cgi-bin/pay/qpay_refund.cgi";           //退款接口

    public function submit($payconfig,$params)
    {

        $config = [
            'trade_type' =>  "NATIVE",
            'body' =>  $params['body'],
            'mch_id' =>  $payconfig['mch_id'],
            'fee_type' =>  'CNY',
            'total_fee' =>  $params['amount']*100,
            'out_trade_no' =>  $params['order_no'],
            'spbill_create_ip' =>  request()->ip(),
            'nonce_str' => $this->genRandomString(),
            'notify_url' => $params['notify_url']
        ];

        if(@$payconfig['overtime']){
            $timeExpireSecond=$payconfig['overtime'];
            $config['time_start']=date('YmdHis');
            $config['time_expire']=date('YmdHis', strtotime( "+$timeExpireSecond Second",time()));
        }


        $config['sign'] = $this->makeSign($config,$payconfig['api_key']);
        $xml = $this->data_to_xml($config);
        $response = $this->postXmlCurl($xml, self::API_URL_PREFIX.self::UNIFIEDORDER_URL);
        $result = $this->xml_to_data($response);

        if(!empty($result['result_code']) && !empty($result['err_code']))
        {
            $result['err_msg'] = $this->error_code($result['err_code']);
        }


        if (isset($result['code_url'])) {
            $subjump = 0;
            $url = $result['code_url'];
        }else{
            $subjump = 1;
            $url = $result;
        }

        $result = array(
            'paymethod' => 'qqpay',
            'payname' => 'QQ钱包支付',
            'type' => 0, //1 手机 0 电脑
            'subjump' => $subjump,
            'url' => $url,
            'subjumpurl' => $url,
            'overtime' => !@$payconfig['overtime']?300:$payconfig['overtime'],
            'money' => $params['amount']
        );

        return array('code' => 1, 'msg' => 'success', 'data' => $result);

    }


    /**
     * 验证
     * @return array 返回数组格式的notify数据
     */
    public function notify($payconfig)
    {
        // 获取xml
        $xml = file_get_contents('php://input', 'r');
        $this->config['key'] = $payconfig['api_key'];
        // 转成php数组
        $data = $this->xml_to_data($xml);
        file_put_contents(YEWU_FILE, 'QQ支付返回值:' . json_encode($data), FILE_APPEND);
        // 保存原sign
        $data_sign = $data['sign'];
        // sign不参与签名
        unset($data['sign']);
        $sign = $this->makeSign($data,$payconfig['api_key']);
        // 判断签名是否正确  判断支付状态
        if ($sign === $data_sign && $data['trade_state'] == 'SUCCESS') {
            $result = $data;
            //处理业务逻辑
            $config = array(
                'tradeid'=>$data['transaction_id'],
                'paymoney'=>$data['total_fee']/100,
                'orderid'=>$data['out_trade_no']);
            $notify = new \payment\notify();
            $notify->run($config);
        } else {
            $result = false;
        }

        // 返回状态给微信服务器
        if ($result) {
            $str = '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
        } else {
            $str = '<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[签名失败]]></return_msg></xml>';
        }
        file_put_contents(YEWU_FILE, CUR_DATETIME . '--返回状态给QQ服务器:' . $str . PHP_EOL, FILE_APPEND);

        return $str;
    }


    /*
   * 输出xml字符
   * @param  array $params  参数名称
   * return xml
   */
    public function data_to_xml($params)
    {
        if(!is_array($params)|| count($params) <= 0)
        {
            return false;
        }
        $xml = "<xml>";
        foreach ($params as $key=>$val)
        {
            if (is_numeric($val)){
                $xml.="<".$key.">".$val."</".$key.">";
            }else{
                $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
            }
        }
        $xml.="</xml>";
        return $xml;
    }

    public function xml_to_data($xml)
    {
        if (PHP_VERSION_ID < 80000) {
            //禁止引用外部xml实体
            libxml_disable_entity_loader(true);
        }
        $result = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $result;
    }

    public function makeSign($data,$key)
    {
        // 去空
        $data = array_filter($data);
        //签名步骤一：按字典序排序参数
        ksort($data);
        //将数组转成url形式
        $string_a = http_build_query($data);
        $string_a = urldecode($string_a);
        //签名步骤二：在string后加入KEY
        $string_sign_temp = $string_a . "&key=" . $key;
        //签名步骤三：MD5加密
        $sign = md5($string_sign_temp);
        // 签名步骤四：所有字符转为大写
        $result = strtoupper($sign);
        return $result;
    }

    /**
     * 产生一个指定长度的随机字符串,并返回给用户
     * @param int $len 产生字符串的长度
     * @return string 随机字符串
     */
    private function genRandomString($len = 32)
    {
        $chars = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
            "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
            "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
            "3", "4", "5", "6", "7", "8", "9"
        );
        $charsLen = count($chars) - 1;
        // 将数组打乱
        shuffle($chars);
        $output = "";
        for ($i = 0; $i < $len; $i++) {
            $output .= $chars[mt_rand(0, $charsLen)];
        }
        return $output;
    }

    private function postXmlCurl($xml, $url, $useCert = false, $mchId='', $second = 30){
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if($useCert == true){
            //设置证书
            //使用证书：cert 与 key 分别属于两个.pem文件
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');

            curl_setopt($ch,CURLOPT_SSLCERT, dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_cert.pem');
            //默认格式为PEM，可以注释
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY,dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_key.pem');

        }
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            return false;
        }
    }

    /*
    * 错误代码
    * @param  $code       服务器输出的错误代码
    * return string
    */
    private function error_code($code){
        $errList = array(
            'LACK_PARAMS'           =>  '缺少必要的请求参数',
            'NOAUTH'                =>  '商户未开通此接口权限',
            'NOTENOUGH'             =>  '用户帐号余额不足',
            'ORDERNOTEXIST'         =>  '订单号不存在',
            'ORDERPAID'             =>  '商户订单已支付，无需重复操作',
            'ORDERCLOSED'           =>  '当前订单已关闭，无法支付',
            'SYSTEMERROR'           =>  '系统错误!系统超时',
            'APPID_NOT_EXIST'       =>  '参数中缺少APPID',
            'MCHID_NOT_EXIST'       =>  '参数中缺少MCHID',
            'APPID_MCHID_NOT_MATCH' =>  'appid和mch_id不匹配',
            'LACK_PARAMS'           =>  '缺少必要的请求参数',
            'OUT_TRADE_NO_USED'     =>  '同一笔交易不能多次提交',
            'SIGNERROR'             =>  '参数签名结果不正确',
            'XML_FORMAT_ERROR'      =>  'XML格式错误',
            'REQUIRE_POST_METHOD'   =>  '未使用post传递参数 ',
            'POST_DATA_EMPTY'       =>  'post数据不能为空',
            'NOT_UTF8'              =>  '未使用指定编码格式',
            'REALNAME_CHECK_ERROR'  =>  '实名检查失败',
        );
        if(array_key_exists($code , $errList)){
            return $errList[$code];
        }
        return $code;
    }
}