<?php
namespace payment\wxpay;
use \Payment\Contracts\IPayNotify;

class callback implements IPayNotify
{
	//处理返回回调callback
	public function handle(string $channel, string $notifyType, string $notifyWay, array $notifyData)
	{
		$paymoney = $notifyData['total_fee']/100;
		$config = array('tradeid'=>$notifyData['transaction_id'],'paymoney'=>$paymoney,'orderid'=>$notifyData['out_trade_no']);
		$notify = new \payment\notify();
		$notify->run($config);
        return true;
	}
	
}