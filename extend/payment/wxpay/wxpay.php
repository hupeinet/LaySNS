<?php

namespace payment\wxpay;

use Exception;
use InvalidArgumentException;
use payment\notify;

class wxpay
{
    //处理请求
    public function submit($payconfig, $params)
    {
        $mchId=$payconfig['mch_id'];
        $config = [
            'app_id' => $payconfig['app_id'],
            'sign_type' => 'MD5',
            'mch_id' => $mchId,
            'md5_key' => $payconfig['api_key'],
            'notify_url' => $params['notify_url'],
            'redirect_url' => $params['return_url'],
            'app_key_pem' => dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_key.pem',
            'app_cert_pem' => dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_cert.pem',
            'fee_type' => 'CNY',
            'limit_pay' => [
            ], // 用户不可用指定渠道支付当有多个渠道时用“,”分隔
        ];

        $payData = [
            'trade_no' => $params['order_no'],
            'amount' => $params['amount'],
            'subject' => $params['subject'],
            'body' => $params['body'],
            'time_expire' => time() + (!@$payconfig['overtime']?300:$payconfig['overtime']), // 表示必须 1000s 内付款
            'client_ip'    => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1', // 客户地址
        ];
        if(isset($params['openid'])){
            $payData['openid']       = 'o528m5zHeBZ-pEwUaNUoqUqJQXbY';
        }

        try {
            $client = new \Payment\Client(\Payment\Client::WECHAT, $config);
            $result = $client->pay(\Payment\Client::WX_CHANNEL_QR, $payData);

            if (isset($result['code_url'])) {
                $subjump = 0;
                $url = $result['code_url'];
            }else{
                $subjump = 1;
                $url = $result;
            }

            $result = array(
                'paymethod' => 'wxpay',
                'payname' => '微信支付',
                'type' => 0, //1 手机 0 电脑
                'subjump' => $subjump,
                'url' => $url,
                'subjumpurl' => $url,
                'overtime' => !@$payconfig['overtime']?300:$payconfig['overtime'],
                'money' => $params['amount']
            );

            return array('code' => 1, 'msg' => 'success', 'data' => $result);

        } catch (InvalidArgumentException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (\Payment\Exceptions\GatewayException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (\Payment\Exceptions\ClassNotFoundException $e) {
            return array('code' => 1001, 'msg' => $e->getMessage(), 'data' => '');
        } catch (Exception $e) {
            return array('code' => 1000, 'msg' => $e->getMessage(), 'data' => '');
        }
    }

    public function notify($payconfig)
    {
        try {
            $mchId=$payconfig['mch_id'];
            $config = [
                'app_id' => $payconfig['app_id'],
                'sign_type' => 'MD5',
                'mch_id' => $payconfig['mch_id'],
                'md5_key' => $payconfig['api_key'],
                'app_key_pem' => dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_key.pem',
                'app_cert_pem' => dirname(__FILE__) .'/pem/'.$mchId.'/apiclient_cert.pem',
                'fee_type' => 'CNY',
                'limit_pay' => [
                ], // 用户不可用指定渠道支付当有多个渠道时用“,”分隔
            ];

            $callback = new callback();
            $client = new \Payment\Client(\Payment\Client::WECHAT, $config);
            $ret=$client->notify($callback);
        } catch (InvalidArgumentException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Payment\Exceptions\GatewayException $e) {
            echo $e->getMessage();
            exit;
        } catch (\Payment\Exceptions\ClassNotFoundException $e) {
            echo $e->getMessage();
            exit;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
        file_put_contents(YEWU_FILE, CUR_DATETIME . '微信回调返回结果：' . $ret . PHP_EOL, FILE_APPEND);

        return $ret;
    }
}