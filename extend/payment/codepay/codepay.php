<?php

namespace payment\codepay;

use \payment\notify;

class codepay
{

    private $apiHost="http://api5.xiuxiu888.com/creat_order/?";
    private $paytypes =[
        'alipay'=>['id'=>1,'name'=>'支付宝'],
        'qqpay'=>['id'=>2,'name'=>'QQ'],
        'wxpay'=>['id'=>3,'name'=>'微信'],
    ];

    //处理请求
    public function submit($payconfig,$params)
    {
        $config = array(
            "id" => (int)$payconfig['app_id'],//平台ID号
            "type" => $this->paytypes[$params['pay_type']]['id'],//支付方式
            "price" => (float)$params['amount'],//原价
            "pay_id" => $params['order_no'], //可以是用户ID,站内商户订单号,用户名
            "outTime" => $payconfig['overtime'],//二维码超时设置
            "page" => 4,//订单创建返回JS 或者JSON
            "return_url" => $params['return_url'],
            "notify_url" => $params['notify_url'],
            "pay_type" => 1,//支付宝使用官方接口
            "user_ip" => request()->ip(),//付款人IP
            "qrcode_url" =>'',//本地化二维码
        );

        try{
            $back = $this->_create_link($config, $payconfig['app_secret']); //生成支付URL
            if (function_exists('file_get_contents')) { //如果开启了获取远程HTML函数 file_get_contents
                $codepay_json = file_get_contents($back['url']); //获取远程HTML
            } else if (function_exists('curl_init')) {
                $ch = curl_init(); //使用curl请求
                curl_setopt($ch, CURLOPT_URL, $back['url']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                $codepay_json = curl_exec($ch);
                curl_close($ch);
            }
            $codepay_data = json_decode($codepay_json,true);
            if(is_array($codepay_data)){
                if($codepay_data['status']<0){
                    return array('code'=>1002,'msg'=>$codepay_data['msg'],'data'=>'');
                }else{
                    $qr = $codepay_data ? $codepay_data['qrcode'] : '';
                    $money = isset($codepay_data['money'])?$codepay_data['money']:$params['amount'];
                    //计算关闭时间
                    $closetime = (int)($codepay_data['endTime']-$codepay_data['serverTime']-3);
                    $result = array(
                        'type'=>0,
                        'subjump'=>0,
                        'paymethod'=>$params['pay_type'],
                        'qr'=>$qr,
                        'payname'=>$this->paytypes[$params['pay_type']]['name'],
                        'overtime'=>$closetime,
                        'money'=>$money
                    );
                    return array('code'=>1,'msg'=>'success','data'=>$result);
                }
            }else{
                return array('code'=>1001,'msg'=>"支付接口请求失败",'data'=>'');
            }
        } catch (\Exception $e) {
            return array('code'=>1000,'msg'=>$e->getMessage(),'data'=>'');
        }
    }


    //处理返回
    public function notify($payconfig)
    {
        $params = $_POST;
        ksort($params); //排序post参数
        reset($params); //内部指针指向数组中的第一个元素
        $sign = $urls= '';
        foreach ($params AS $key => $val) {
            if ($val == '') continue;
            if ($key != 'sign') {
                if ($sign != '') {
                    $sign .= "&";
                    $urls .= "&";
                }
                $sign .= "$key=$val"; //拼接为url参数形式
                $urls .= "$key=" . urlencode($val); //拼接为url参数形式
            }
        }
        if (!$params['pay_no'] || md5($sign . $payconfig['app_secret']) != $params['sign']) { //不合法的数据 KEY密钥为你的密钥
            return 'error|Notify: auth fail';
        } else { //合法的数据
            //业务处理
            $config = array(
                'tradeid'=>$params['pay_no'],
                'paymoney'=>$params['money'],
                'orderid'=>$params['pay_id']
            );
            $business = new \payment\notify();
            $data = $business->run($config);
            if($data['code']>1){
                return 'error|Notify: '.$data['msg'];
            }else{
                return 'success';
            }
        }
    }


    /**
     * 加密函数
     * @param $params 需要加密的数组
     * @param $codepay_key //码支付密钥
     * @param string $host //使用哪个域名
     * @return array
     */
    private function _create_link($params, $codepay_key, $host = "")
    {
        ksort($params); //重新排序$data数组
        reset($params); //内部指针指向数组中的第一个元素
        $sign = '';
        $urls = '';
        foreach ($params AS $key => $val) {
            if ($val == '') continue;
            if ($key != 'sign') {
                if ($sign != '') {
                    $sign .= "&";
                    $urls .= "&";
                }
                $sign .= "$key=$val"; //拼接为url参数形式
                $urls .= "$key=" . urlencode($val); //拼接为url参数形式
            }
        }

        $key = md5($sign . $codepay_key);//开始加密
        $query = $urls . '&sign=' . $key; //创建订单所需的参数
        $apiHost = ($host ? $host : $this->apiHost); //网关
        $url = $apiHost . $query; //生成的地址
        return array("url" => $url, "query" => $query, "sign" => $sign, "param" => $urls);
    }
}
