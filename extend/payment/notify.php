<?php
namespace payment;
use app\common\model\Pay;
use app\common\model\Order;
class notify
{
    //处理返回
    public function run(array $params)
    {
        //订单号
        $tradeid = $params['tradeid'];
        //本站订单号
        $orderid = $params['orderid'];
        $pay = new Pay();
        file_put_contents(YEWU_FILE, CUR_DATETIME . '-处理后的数据:' . json_encode($params) . PHP_EOL, FILE_APPEND);

        try {
            $payItem = $pay->where(array('order_no' => $orderid))->find();
            if (!empty($payItem)) {
                if ($payItem['status'] > 0) {
                    $data = array('code' => -1, 'msg' => '订单已处理,请勿重复推送');
                    return $data;
                } else if ($params['paymoney'] < $payItem['money']) {
                    //原本检测支付金额是否与订单金额一致,但由于码支付这样的收款模式导致支付金额有时会与订单不一样,所以这里进行小于判断;
                    //所以,在这里如果存在类似码支付这样的第三方支付辅助工具时,有变动金额时,一定要做递增不能递减
                    $data = array('code' => 1005, 'msg' => '支付金额小于订单金额');
                    return $data;
               }
                    $update = array('status' => 1, 'pay_time' => time(), 'trade_no' => $tradeid);
                    $u = $payItem->save($update);
                    if (!$u) {
                        $data = array('code' => 1004, 'msg' => '更新失败');
                    } else {
                       Order::processing($orderid);
                       $data = array('code' => 1, 'msg' => '成功');
                    }

            } else {
                $data = array('code' => 1003, 'msg' => '订单号不存在');
            }
        } catch (\Exception $e) {
            file_put_contents(YEWU_FILE, CUR_DATETIME . '-reuslt:-notify' . $e->getMessage() . PHP_EOL, FILE_APPEND);
            $data = array('code' => 1001, 'msg' => $e->getMessage());
        }
        return true;
    }
}