<?php
namespace sms\txsms;
use org\Http;

class txsms
{

    const API_URL='https://yun.tim.qq.com/v5/tlssmssvr/sendsms';
    protected $config;


    public function __construct($config)
    {
        $this->config=$config;
    }



    public function send($phoneNumber,$content,$code='',$tplId='')
    {

        $random = rand(100000, 999999);
        $curTime = time();
//        * @param int    $type        短信类型，0 为普通短信，1 营销短信
//    * @param string $nationCode  国家码，如 86 为中国
//    * @param string $phoneNumber 不带国家码的手机号
//    * @param string $msg         信息内容，必须与申请的模板格式一致，否则将返回错误
//    * @param string $extend      扩展码，可填空串
//    * @param string $ext         服务端原样返回的参数，可填空串

        // 按照协议组织 post 包体
        $data = new \stdClass();
        $tel = new \stdClass();
        $tel->nationcode = "86";
        $tel->mobile = "".$phoneNumber;

        $data->tel = $tel;
        $data->type = 0;
        $data->msg = $content;
        $data->sig = hash("sha256",
            "appkey=".$this->config['app_key']."&random=".$random."&time="
            .$curTime."&mobile=".$phoneNumber, FALSE);
        $data->time = $curTime;


        $wholeUrl = self::API_URL . "?sdkappid=" . $this->config['app_id'] . "&random=" . $random;
        $result = Http::post($wholeUrl,json_encode($data));
        $data=json_decode($result,true);
        if ($data['result']==0) {
            return array('code' => 1, 'msg' => '发送成功');
        } else {
            return array('code' => 0, 'msg' => $data['errmsg']);
        }
    }

}