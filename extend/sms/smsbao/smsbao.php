<?php
namespace sms\smsbao;
use org\Http;

class smsbao
{

    const API_URL='http://api.smsbao.com/sms';
    protected $username;
    protected $password;

    public function __construct($options)
    {
        $this->username=$options['username'];
        $this->password=md5($options['password']);
    }

    protected static $errorCode = array(
        0 => '发送成功',
        30 => '短信平台密码错误',
        40 => '短信平台账号不存在',
        41 => '短信平台余额不足',
        42 => '短信平台帐户已过期',
        43 => 'IP地址受短信平台限制',
        50 => '内容含有敏感词',
        51 => '手机号码不正确',
        -2 => '服务器空间不支持',
        -1 => '发送短信参数不全'
    );


    public function send($toObject,$content,$code='',$tplId='')
    {

        $url = self::API_URL. "?u=" . $this->username . "&p=" .$this->password. "&m=" . $toObject . "&c=" . urlencode($content);
        $htd = new Http();
        $data = $htd->get($url);
        if (is_numeric($data) && $data == 0) {
            return array('code' => 1, 'msg' => '发送成功');
        } else {
            return array('code' => 0, 'msg' => self::$errorCode[$data] . '，请联系站长');
        }
    }

}