<?php

return array (
  'download_type' => 
  array (
    'title' => '下载模式:',
    'type' => 'radio',
    'options' => 
    array (
      0 => '游客模式',
      1 => '回复下载',
      2 => '积分下载',
    ),
    'value' => '2',
    'tips' => '如果启用游客模式，则之前的积分都会失效',
  ),
  'comment_down_ext' => 
  array (
    'title' => '回复下载要求:',
    'type' => 'radio',
    'options' => 
    array (
      0 => '无需审核',
      1 => '必须审核',
    ),
    'value' => '1',
    'tips' => '开启回复下载模式下生效',
  ),
  'lanzou_direct_url' => 
  array (
    'title' => '蓝奏网盘直连解析:',
    'type' => 'radio',
    'options' => 
    array (
      0 => '关闭',
      1 => '开启',
    ),
    'value' => '0',
    'tips' => '开启后可以直接下载',
  ),
  'allow_upload' => 
  array (
    'title' => '允许上传:',
    'type' => 'checkbox',
    'options' => 
    array (
      1 => '前台用户允许',
      2 => '后台用户允许',
    ),
    'value' => '2',
  ),
  'showname' => 
  array (
    'title' => '展示页的名称:',
    'type' => 'text',
    'value' => '下载地址',
  ),
  'linkanalyze' => 
  array (
    'title' => '网址识别：',
    'type' => 'textarea',
    'value' => '百度网盘|pan.baidu.com|bd.png
蓝奏网盘|lanzou|lz.png
闪电网盘|shandian|sd.png
微云网盘|weiyun.com|wy.png
阿里云盘|aliyundrive|ali.png
天翼云盘|cloud.189.cn|189_cn.png',
    'tip' => '示例【网盘名称|地址链接|图标（图标放在static中）】：百度网盘|baidu.com/pan|bdp.png',
    'cols' => '100',
    'rows' => '6',
  ),
);
