<?php
namespace addons\attachlink;
use app\common\library\PluginMenu;
use addons\attachlink\model\Attachlink;
use think\Addons;
use think\facade\Cache;


class plugin extends Addons
{
    const NAME = 'attachlink';
    const TITLE = '附件链接插件';
    const DESCRIPTION = '全网首创，易管理、可无限增删改文章的附件链接';
    const ICON = 'fa-list';

    public $info = array(
        'name' => SELF::NAME,
        'title' => SELF::TITLE,
        'description' => self::DESCRIPTION,
        'status' => 1,
        'author' => '云阳',
        'version' => '3.4',
        'url' => 'https://bbs.laysns.com/app/7250a226ee11c4b92f0ac3e3aeeff4f5.html',
        'hooks' => 'link_add,link_save,link_show',
        'hooks_desc' => '添加附件链接,保存附件链接,显示附件链接'
    );

    public function install()
    {
        $menu = [
            [
                'title' => SELF::TITLE,
                'icon' => SELF::ICON,
                'remark' => SELF::NAME,
                'sublist' => [
                    ['href' => '/addons/attachlink/Admin/index', 'title' => '附件链接管理', 'remark' => SELF::NAME],
                    ['href' => '/addons/attachlink/Admin/batchAdd', 'title' => '批量添加附件链接', 'remark' => SELF::NAME],
                ]
            ]
        ];
        PluginMenu::create($menu);
        return true;
    }

    public function uninstall()
    {
        PluginMenu::delete(SELF::NAME);
        return true;
    }


    public function link_add($param)
    {
        $attachlink=array('score' => 0, 'download_type'=>-1,'remark' => '','urls'=>[]);
        if (!empty($param) && $param['subject_id'] > 0) {
            $map['subject_id'] = $param['subject_id'];
            $map['subject'] = $param['subject'];
            $find = (new Attachlink())->where($map)->find();
            if($find){
                $attachlink=$find;
            }
        }
        $this->assign('attachlink',$attachlink);
        $this->assign('k',count($attachlink['urls']));
        return $this->fetch('index/add');
    }

    public function link_save($params)
    {
        if(!isset($params['subject_id'])){
            return;
        }
        $res=(new Attachlink())->saveData($params);
        if($res) {
            $cacheName = 'attachlink_' . $params['subject'] .'_'.$params['subject_id'];
            if (Cache::has($cacheName)) {
                Cache::delete($cacheName);
            }
            $row = (new Attachlink())
                ->where('subject_id',$params['subject_id'])
                ->where('subject',$params['subject'])
                ->find();
            if ($row && isset($params['download_type'])) {
                $row->download_type = $params['download_type'];
                $row->save();
            }

        }
        return true;
    }


    public function link_show($param)
    {
        $subject=$param['subject'];
        $subjectId=$param['subject_id'];
        $attachlinkConfig=$this->getConfig();
        $item=Attachlink::getItem($subjectId,$subject,$attachlinkConfig);
        if($item&&$item['download_type']!=-1){
            $attachlinkConfig['download_type']=$item['download_type'];
        }
        $this->assign('attachlinkConfig',$attachlinkConfig);
        $this->assign('attachlink', $item);
        echo $this->fetch('index/show');
    }

}
