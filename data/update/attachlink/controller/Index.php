<?php
namespace addons\attachlink\controller;
use addons\attachlink\library\lanzou;
use addons\attachlink\model\Attachlink;
use addons\attachlink\model\AttachlinkDownload;
use addons\attachlink\Plugin;
use app\common\model\Comment;
use app\user\model\UserRichesLog;
use think\Addons;

class Index extends Plugin
{
    protected $forum_model;
    protected $category_model;
    public function download()
    {
        $config = $this->getConfig();
        $uid = cacheUserid();
        $downloadType = $config['download_type'];
        $linkidStr = input('linkid');
        $linkKeyArr = explode('-', $linkidStr);
        $linkKey = $linkKeyArr[0];
        $attachinfo = Attachlink::getDataByKey($linkKey);
        if($attachinfo&&$attachinfo['download_type']!=-1){
            $downloadType=$attachinfo['download_type'];
        }
        if (!strpos($linkidStr, '-')) {
            return fail('链接地址不合规');
        }
        if ($downloadType == 2 && !$uid) {
            return fail('必须登录之后才能下载');
        }

        $urlInfo=array_values($attachinfo['urls']);
        $linkId = $linkKeyArr[1];
        if (!$attachinfo || !isset($urlInfo[$linkId])) {
            return fail('链接不存在');
        }
        $model = new Attachlink();
        $subjectId = $attachinfo['subject_id'];
        $subject = $attachinfo['subject'];
        $id = $attachinfo['id'];
        $zuid = $attachinfo['user_id'];
        $msg='';
        if ($downloadType == 1 && $zuid !== $uid) {

            $map['subject_id'] = $subjectId;
            $map['subject'] = $subject;
            if($config['comment_down_ext']) {
                $map['status'] = 1;
            }
            if (!$uid) {
                $map['ip2long'] = ip2long(request()->ip());
            } else {
                $map['user_id'] = $uid;
            }
            $count = (new Comment())->where($map)->count();
            if (!$count) {
                return fail('评论后才可以下载');
            }
        } elseif ($downloadType == 2 && $zuid !== $uid) {
            //判断当前用户是否可以免积分下载
            $score = $attachinfo['score'];
            $map3['subject_id'] = $id;
            $map3['subject'] = 'ATTACHLINK';
            $map3['user_id'] = $uid;
            $map3['is_add'] = 0;
            $downlog = (new UserRichesLog())->where($map3)->count();
            if (!$downlog && $score > 0) {
                $point = cacheUser()['score'];
                if ($point < $score) {
                    return fail('积分不足');
                } elseif ($score > 0) {
                    $msg='扣除'.$score.'积分';
                    UserRichesLog::note($score, $uid, '下载附件' . $subject . '_' . $subjectId,0,$id,'ATTACHLINK');
                    if($zuid) {
                        UserRichesLog::note($score, $zuid, '附件收益' . $subject . '_' . $subjectId, 1, $id, 'ATTACHLINK');
                    }
                }
            }
        }
        $link = str_replace('lanzous','lanzoux',$urlInfo[$linkId]['url']);
        if (strpos($link, 'lanzou') !== false&&$config['lanzou_direct_url']) {
            $ret = (new lanzou())->getUrl($link, @$urlInfo['code']);
            if ($ret['code'] == 1) {
                $link = $ret['data']['url'];
            }
        }
        $model->where('id', $id)->inc('download')->update();
        (new AttachlinkDownload())->save([
            'user_id'=>$uid,
            'attachlink_id'=>$id,
            'ip2long'=>ip2long(request()->ip()),
        ]);

        return success('链接地址获取成功'.$msg, ['url' => $link]);
    }
}
