DROP TABLE IF  EXISTS `__PREFIX__plugin_attachlink`;
CREATE TABLE `__PREFIX__plugin_attachlink` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `link_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `download_type` tinyint(1) NULL DEFAULT -1,
  `subject_id` int(10) NOT NULL DEFAULT 0,
  `user_id` int(10) NULL DEFAULT 0,
  `subject` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'ARTICLE',
  `score` int(5) NOT NULL DEFAULT 0,
  `status` tinyint(1) NULL DEFAULT 1,
  `urls` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '链接信息',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `download` int(9) NOT NULL DEFAULT 0 COMMENT '下载次数',
  `create_time` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key`(`link_key`) USING BTREE,
  INDEX `subject_id`(`subject_id`, `subject`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF  EXISTS `__PREFIX__plugin_attachlink_download`;
CREATE TABLE  `__PREFIX__plugin_attachlink_download` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
  `attachlink_id` int(10) NULL DEFAULT 0,
  `user_id` int(11) NULL DEFAULT NULL,
  `ip2long` char(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;