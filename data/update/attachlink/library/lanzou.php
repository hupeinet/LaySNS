<?php
//开源项目 https://gitee.com/liukuaizhuan/lanzou
//可上传的 https://gitee.com/mayoushang/lancou
namespace addons\attachlink\library;
class lanzou
{
    public function getUrl($url, $pwd = '',$type='')
    {
        $download='';
        if (!empty($url)) {
            $b = 'com/';
            $c = '/';
            $id = $this->GetBetween($url, $b, $c);
            $d = 'https://www.lanzoux.com/tp/' . $id;
            $lanzou = $this->curl($d);
            if (strpos($lanzou,'文件取消分享了') || empty($lanzou)) {
                $Json = array(
                    "code" => 201,
                    "msg" => '文件取消分享了',
                );
            }
            else{
                if (strpos($lanzou,'输入密码') && empty($pwd)) {
                    $Json = array('code' => 202,'msg' => '请输入密码');
                }
                else{
                    preg_match("/<div class=\"md\">(.*?)<span class=\"mtt\">/", $lanzou, $name);
                    preg_match('/时间:<\\/span>(.*?)<span class=\\"mt2\\">/', $lanzou, $time);
                    preg_match('/发布者:<\\/span>(.*?)<span class=\\"mt2\\">/', $lanzou, $author);
                    preg_match('/var domianload = \'(.*?)\';/', $lanzou, $down1);
                    preg_match('/domianload \+ \'(.*?)\'/', $lanzou, $down2);
                    preg_match('/var downloads = \'(.*?)\'/', $lanzou, $down3);
                    preg_match('/<div class=\\"md\\">(.*?)<span class=\\"mtt\\">\\((.*?)\\)<\\/span><\\/div>/', $lanzou, $size);
                    $pwdurl='';
                    if (!empty($down2)){
                        $download = $this->getRedirect($down1[1] . $down2[1]);
                    }
                    else{
                        $download = $this->getRedirect($down1[1] . $down3[1]);
                    }
                    if (!empty($pwd)) {
                        preg_match('/sign\':\'(.*?)\'/', $lanzou, $sign);
                        $post_data = array('action' => 'downprocess', 'sign' => $sign[1], 'p' => $pwd);
                        $pwdurl = $this->send_post('https://wwa.lanzoux.com/ajaxm.php', $post_data);
                        $obj = json_decode($pwdurl, true);
                        $download = $this->getRedirect($obj['dom'] . '/file/' . $obj['url']);
                    }
                    $Json = array(
                        "code" => 1,
                        "data" => array(
                            "name" => $name[1],
                            "author" => $author[1],
//                            "time" => $time[1],
                            "size" => $size[2],
                            "url" => $download
                        )
                    );
                    if(strpos($pwdurl,'"zt":0') !== false) {
                        $Json = array(
                            "code" => 202,
                            "msg" => '密码不正确',
                        );
                    }
                }
            }
        }
        else{
            $Json = array(
                'code' => 201,
                'msg' => '请输入需要解析的蓝奏链接'
            );
        }
        if($type == 'down'){
            header("Location:{$download}");
        }
        if (!empty($Json)) {
            return $Json;
        }
    }


    function send_post($url, $post_data)
    {
        $postdata = http_build_query($post_data);
        $options = array('http' => array(
            'method' => 'POST',
            'header' => 'Referer: https://www.lanzoux.com/\\r\\n' . 'Accept-Language:zh-CN,zh;q=0.9\\r\\n',
            'content' => $postdata,
            'timeout' => 15 * 60,
        ));
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    function curl($url){
        $header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
        $header[] = "Accept-Encoding: gzip, deflate, sdch, br";
        $header[] = "Accept-Language: zh-CN,zh;q=0.8";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    function GetBetween($content, $start, $end)
    {
        $r = explode($start, $content);
        if (isset($r[1])) {
            $r = explode($end, $r[1]);
            return $r[0];
        }
        return '';
    }

    function getRedirect($url,$ref=''){
        $headers = array(
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: zh-CN,zh;q=0.9',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'Pragma: no-cache',
            'Upgrade-Insecure-Requests: 1',
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
        if ($ref) {
            curl_setopt($curl, CURLOPT_REFERER, $ref);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($curl);
        $url=curl_getinfo($curl);
        curl_close($curl);
        return $url["redirect_url"];
    }

    //获取随机ip
    private function Rand_IP()
    {
        $ip2id = round(rand(600000, 2550000) / 10000);
        $ip3id = round(rand(600000, 2550000) / 10000);
        $ip4id = round(rand(600000, 2550000) / 10000);
        $arr_1 = array("218", "218", "66", "66", "218", "218", "60", "60", "202", "204", "66", "66", "66", "59", "61", "60", "222", "221", "66", "59", "60", "60", "66", "218", "218", "62", "63", "64", "66", "66", "122", "211");
        $randarr = mt_rand(0, count($arr_1) - 1);
        $ip1id = $arr_1[$randarr];
        return $ip1id . "." . $ip2id . "." . $ip3id . "." . $ip4id;
    }
}
