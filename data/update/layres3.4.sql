/*PHPmyadmin执行 如有报错就一行一行删除，删除一行点击执行，直到不报错为止，报错是因为有的字段已经存在了*/
ALTER TABLE `laysns_article_category`
MODIFY COLUMN `detail_template` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'html' AFTER `list_template`;


CREATE TABLE `laysns_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属会员 0属于系统消息',
  `to_uid` int(11) NOT NULL DEFAULT 0 COMMENT '发送对象 0发送给全部',
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态 1 显示  2 隐藏',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `to_uid`(`to_uid`) USING BTREE
) ENGINE = MyISAM  CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息表' ROW_FORMAT = Dynamic;

CREATE TABLE `laysns_message_read`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '会员',
  `message_id` int(11) NOT NULL DEFAULT 0 COMMENT '消息对象',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '消息状态 0未读 1已读 -1删除',
  `create_time` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息读取表' ROW_FORMAT = Dynamic;
INSERT INTO `laysns_system_menu` VALUES (null, 11, '消息管理', 'fa fa-list', 'main.message/index', '', '_self', 0, 1, '', 1623986562, 1623986596, 0);
delete from `laysns_system_config` where name='version';
insert into `laysns_system_config` values (null,'version','j/0Afx6Ep5NJtGr0+NGiHBWg7MCodoxSmiKDkPS1z6D04Cpd2DTj9kaTwYYmDLX1xmpWYXUNBQa0qRvtAZMjdd/mOjU1UTODOLnwQfAK75WmXSLlxTYzS4k0mwkFplr1',1622647238,1622647238);
