<?php
return array(
'title'=>'主体默认模板',		//模板名称
'author'=>'LaySNS',			//模板作者
'version'=>'1.0',			//模板版本
'url'=>'https://bbs.laysns.com/app/5c4a30e7a207135faf79b6d7694e521e.html',	            //应用中心详情页地址
'demo_url'=>'https://sns.laysns.com/',
'description'=>'只为那些不变的承诺，我来了'			//模板版权/介绍
);