/*首页轮播图*/
layui.use([ 'carousel', 'flow',], function () {
    var carousel = layui.carousel;
    var flow = layui.flow;
    flow.lazyimg();
    carousel.render({
        elem: '#index-lb'

        , width: '100%'

        , height: '100%'

        , interval: 5000

    });

});