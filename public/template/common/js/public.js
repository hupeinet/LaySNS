var config = {
	api: {
		login_out: '/api/login/out.html',//注销登录
		login_check: "/api/login/check",//检测登录状态
		login: "/api/login/index.html", //登录API接口
		send_code: '/api/login/sendmsg.html',//发送验证码API接口短信和邮箱通用
		bind_subject: '/api/login/bindsubject.html',
		register: "/api/login/register.html",
		forgetpass: "/api/login/forgetpass.html",
		unbind_applogin: '/api/login/unbind_applogin',
		bind: '/api/login/bind',
		changepass: "/api/user/change_password.html",
		contribute: "/api/user/contribute.html",
		editor_upload_img:"/admin/ajax/editorUploadImg",
		editor_upload_file:"/admin/ajax/editorUploadFile",
		user_common_del: '/api/user/common_del',
		user_changeinfo: '/api/user/changeinfo',
		trim: "/user/index/trim.html",
		add_view: "/api/index/add_view.html",
		poster: "/api/index/poster.html",
		emoji_dir: "/static/common/images/emoji",
		order_create:"/index/order/create",
		paysubmit:"/pay/submit",

		comment: '/api/user/comment.html',
		activate: "/user/activate.html",
		index_page: "/api/index/today_list_index_page.html",
		online_rank: "/api/index/tr_time_list.html",

		upload: '/api/user/upload.html',
		user_praise: '/api/user/praise.html',
		user_favorite: '/api/user/favorite.html',
		comment_data: "/api/user/comment_data.html",
		my_user_list: '/index/mapi/user_list_page.html',
		my_comment_list: '/index/mapi/user_pl_page.html',
		ck_phone_msg: '/api/index/ck_code_msg',

		complete_info: "/user/user/w_info.html",
		qqnick: "/index/mapi/qqnick.html",
		safe: "/user/user/safe.html",
	}
};


var layer,element,form,laypage;
layui.use(['element','form','layer','laytpl','laypage'], function() {
	element = layui.element;
	form = layui.form;
	laypage = layui.laypage;
});

var login_data = isLogin(true);
var html='<i class="layui-icon layui-icon-username"></i>' +
	'                <div class="show-data">' +
	'                    <div class="show">' +
	'                        <i></i>' +
	'                        <span>登录后，享受更多优质服务哦</span>' +
	'                        <button onclick="openLoginK();">登录</button>' +
	'                    </div>' +
	'                </div>';

if(login_data.status){
	/*导航栏*/
	html ='<img class="ico" src="'+login_data.avatar+'" onerror="this.src=\'/static/common/images/avatars/default.png\'"/>' +
		'                <div class="show-data">' +
		'                    <div class="show logined">' +
		'                        <i></i>' +
		'                        <span class="logined_text">欢迎回来，'+login_data.nickname+'</span>' +
		'                        <span class="logined_text share">' +
		'<b class="hot"></b>' +
		'您已投稿分享过 <font>'+login_data.posted_num+'</font> 次 ，' +
		'共有 <font>'+login_data.praise_num+'</font> 位老铁为你点赞' +
		'</span>' +
		'                        <div class="logined_btn layui-clear">' +
		'                            <a href="/user/index.html">个人中心</a>' +
		'                            <a href="javaScript:exit();">退出</a>' +
		'                        </div>' +
		'                    </div>' +
		'                </div>';

	/*我的信息*/
	$(".lg_no").addClass("lg_no_hide");
	$(".lg_tr").addClass("lg_tr_show");
	$(".lg_tr .u_nick").html(login_data.nick);
	$(".lg_tr .user-img").attr("src",login_data.avatar)

}
$(".header-user").html(html);



function castnum(number) {
	return number;
}
function getDateDiff(dateTimeStamp) {
	if (dateTimeStamp !== '刚刚') {
		dateTimeStamp = new Date(dateTimeStamp).getTime();
	}
	var result = 0;
	var minute = 1000 * 60;
	var hour = minute * 60;
	var day = hour * 24;
	var halfamonth = day * 15;
	var month = day * 30;
	var now = new Date().getTime();
	var diffValue = now - dateTimeStamp;
	if (diffValue < 0) {
		return;
	}
	var monthC = diffValue / month;
	var weekC = diffValue / (7 * day);
	var dayC = diffValue / day;
	var hourC = diffValue / hour;
	var minC = diffValue / minute;
	if (monthC >= 1) {
		result = "" + parseInt(monthC) + "月前";
	} else if (weekC >= 1) {
		result = "" + parseInt(weekC) + "周前";
	} else if (dayC >= 1) {
		result = "" + parseInt(dayC) + "天前";
	} else if (hourC >= 1) {
		result = "" + parseInt(hourC) + "小时前";
	} else if (minC >= 1) {
		result = "" + parseInt(minC) + "分钟前";
	} else
		result = "刚刚";
	return result;
}
function renderTemplate(data,templateId, resultContentId,countId,after){

		if(countId!==null){
			$( "#"+countId).html(data.pager.count);
		}
	layui.use(['laytpl','element'], function () {

		layui.laytpl($("#" + templateId).html()).render(data, function (html) {
			if(after==null) {
				$(resultContentId).html(html);
			}else if(after=='append'){
				$(resultContentId).append(html);
			}else{
				$(resultContentId).after(html);
			}
			// layui.element.render();

		});
	});

};

function renderPageData(url,laypageDivId, templateId, resultContentId,countId,pageParams){
    var pages = {
        pageIndex : 1,
        pageSize : 10
    }
	// if (pageParams==null){
		pageParams = Object.assign(pages,pageParams);
	// }
	$.ajax({
		url : url,
		method : 'post' ,
		data : pageParams, //JSON.stringify(datasub)
		async : true ,
		complete : function (XHR, TS){},
		error : function (XMLHttpRequest, textStatus, errorThrown) {
			if ( "error" ==textStatus){
				alert( "服务器未响应，请稍候再试" );
			} else {
				alert( "操作失败，textStatus=" +textStatus);
			}
		},
		success : function (data) {
			var jsonObj;
			if ( 'object' == typeof data){
				jsonObj = data;
			} else {
				jsonObj = JSON.parse(data);
			}
			// console.log(jsonObj);
            if(jsonObj.code!=1){
                alert(jsonObj.msg);
                return false;
            }
			renderTemplate(jsonObj.data,templateId, resultContentId,countId);

			//重新初始化分页插件
			layui.use([ 'form' , 'laypage' ], function (){
				var laypage=layui.laypage;
				var pager=jsonObj.data.pager;
				laypage.render({
					elem : laypageDivId,
					curr : pager.pageIndex,
					limit: pager.pageSize,
					count : pager.count,
					theme: '#1E9FFF',
					jump: function (obj, first){ //obj是一个object类型。包括了分页的所有配置信息。first一个Boolean类，检测页面是否初始加载。非常有用，可避免无限刷新。
						pageParams.pageIndex = obj.curr;
						pageParams.pageSize = pager.pageSize;
						if (!first){
							renderPageData(url,laypageDivId, templateId, resultContentId,countId,pageParams);
						}
					}
				});
			});
		}
	});
};

$(document).ready(function() {
	var NLNUM = $('.index-news-list .page a');
	NLNUM.click(function() {
		NLNUM.removeClass('current');
		$(this).addClass('current');
		var NUMB = $(this).index() + 1;
		if ($(this).attr('class') == 'current') {
			$('.block').css({
				'display': 'none'
			});
			$('.new-list-' + String(NUMB)).css({
				'display': 'block'
			})
			$(window).trigger('scroll');
		} else {
			$('.new-list .block').css({
				'display': 'none'
			})
		}
	});
});

String.prototype.isPhone= function () {
	return /^0?(13|14|15|17|18|19)[0-9]{9}$/.test(this);};String.prototype.isEmail =function () {return /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/.test(this);};

$.fn.field = function () {
	var arr_data = $(this).serializeArray();
	var formData = {};
	if (arr_data.length > 0) {
		arr_data.forEach(function (item) {
			formData[item.name] = item.value;
		});
	}
	return formData;
};
/** * * @param {Object} config * @returns {boolean} */
$.fn.verify = function (config) {
	var arr_data = $(this).field();
	for (var key in config) {
		var item = $(this).find("[name=" + key + "]");
		msg = config[key](arr_data[key], item);
		if (typeof (msg) !== 'undefined') {
			item.focus();
			msg !== 1 && layer.msg(msg);
			return false;
		}
	}
	return true;
};
/**
 * 倒计时
 * @param {Object} option
 */
$.fn.timeOut = function (option) {
	if (typeof (option) !== "object") {
		console.error("倒计时参数异常");
	}

	var btn = this;//document.querySelector(option.btn || '');

	var time = option.time || 60;
	var txt = option.text || "{s}秒";
	var objtxt = function (t) {
		btn.html(t);
	};
	console.log(btn[0].id)
	btn.on('click', function () { //touchstart

		if (!btn.disabled && !btn.hasClass(option.class || "disabled")) {

			var sd = function () {
				var timer = setInterval(function () {
					sd = null;
					var innerHTML = '';
					if (time < 1) {
						clearInterval(timer);
						btn[0].removeAttribute('disabled');
						btn.removeClass(option.class || "disabled");
						innerHTML = option.endtxt || "重新发送";
						time = option.time || 60;
						option.end && option.end();
						btn.html(innerHTML);
						return;
					} else {
						btn.addClass(option.class || "disabled");
						btn[0].setAttribute('disabled', true);
						innerHTML = txt.replace("{s}", time);
					}
					btn.html(innerHTML);
					time--;
				}, 1000);
			};
			option.send && option.send(sd, objtxt);
		}
	});
};
function switchTop() {
	var top = $("header");
	if (top.offset().top > 70) {
		top.addClass("su");
	} else {
		top.removeClass("su");
	}
}

$(window).scroll(function() {
	switchTop();
});

$(function() {
	switchTop();
});


$(function() {
	$('.article-body').find('img').each(function() {
		var _this = $(this);
		var _src = _this.attr("src");
		var _alt = _this.attr("alt");
		_this.wrap('<a data-fancybox="images"  href="' + _src + '" data-caption="' + _alt + '"></a>');
	})
})
$('.list-article-tab > .type > a').on('mouseover', function() {
	$(this).addClass('hover').siblings().removeClass('hover');
	$(this).parents('.list-article-tab').find('.plate-list').eq($(this).index()).addClass('ing').siblings('.plate-list').removeClass('ing');
	$(window).trigger('scroll');
});
$('.list-soft-tab > .type > a').on('mouseover', function() {
	$(this).addClass('hover').siblings().removeClass('hover');
	$(this).parents('.list-soft-tab').find('.plate-list').eq($(this).index()).addClass('ing').siblings('.plate-list').removeClass('ing');
	$(window).trigger('scroll');
});

$('.site-list-box > .type > a').on('mouseover', function() {
	$(this).find("a:first").addClass("hover")
	$(this).addClass('hover').siblings().removeClass('hover');
	$(this).parents('.site-list-box').find('.plate-list').eq($(this).index()).addClass('ing').siblings('.plate-list').removeClass('ing');
	$(window).trigger('scroll');
});
$(".site-list-box > .type").each(function() {
	$(this).find("a:first").addClass("hover");
	$(this).find("ul:first").addClass("ing");
})
$(".site-list-box.list").each(function() {
	$(this).find("ul:first").addClass("ing");
})
$(".hot-soft .type a").hover(function(e) {
	$(".hot-soft .type .hover").removeClass("hover");
	$(this).addClass("hover"); /*计算当前应该偏移的高度*/
	var index = $(".hot-soft .type .hover").index();
	var x = $(".hot-soft").width() * index + parseInt($(".hot-soft").css("margin-right")) * index;
	$(".hot-soft .soft-plate").css("transform", "translateX(-" + x + "px)");
});
$(".hot-soft .plate-list ul").each(function() {
	$(this).find("li:first").addClass("now"), $(this).find("li").hover(function() {
		$(this).addClass("now").siblings().removeClass("now")
	})
})
$(".list-soft-tab .type,.list-article-tab .type").each(function() {
	$(this).find("a:first").addClass("hover");
})
$(".list-soft-tab .list-soft-plate,.list-article-tab .list-article-plate").each(function() {
	$(this).find(".plate-list:first").addClass("ing");
})

$(function() {
	$("header .menu").click(function() {
		$("header nav ul").slideToggle("slow");
		$("header .menu").toggleClass("xz");
	})

})
$("header .night").hover(function() {
	openMsg();
}, function() {
	layer.close(subtips);
});

function openMsg() {
	subtips = layer.tips('开启/关闭夜间模式', 'header .night', {
		tips: [1, '#333'],
		time: 30000
	});
}

function opensearcbox() {
	var search = $("#search");
	// layui.use(['layer', 'form'], function() {
		var search_index = layer.open({
			type: 1,
			skin: "searchskin",
			content: search,
			btn: 0,
			close: 0,
			title: false,
			area: ["678px", "390px"],
			end: function() {
				$("#searchBox").hide();
			}
		});
		$("#searchBox").show();
		$(".closesearch").click(function(e) {
			layer.close(search_index);
		});
	// });
}
$(document).ready(function() {
	//获取日期
	var myDate = new Date;
	var year = myDate.getFullYear(); //获取当前年
	var mon = myDate.getMonth() + 1; //获取当前月
	var date = myDate.getDate(); //获取当前日
	var week = myDate.getDay();
	var weeks = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	//	$(".home-time").html(year + "年" + mon + "月" + date + "日 " + weeks[week]);
	//夜间模式
	$('.night').click(function() {
		if ($(this).hasClass('fa-moon-o')) {
			$(this).addClass('fa-sun-o');
			$(this).removeClass('fa-moon-o')
		} else {
			$(this).removeClass('fa-sun-o');
			$(this).addClass('fa-moon-o')
		}
	})
	if (is_switch_day_night) {
		if (document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") === '') {
			if (all_night_theme || new Date().getHours() > 22 || new Date().getHours() < 6) {
				document.body.classList.add('night');
				document.cookie = "night=1;path=/";
				//console.log('夜间模式开启');
			} else {
				document.body.classList.remove('night');
				document.cookie = "night=0;path=/";
				//console.log('夜间模式关闭');
			}
		} else {
			var night = document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '0';
			if (night == '0') {
				document.body.classList.remove('night');
			} else if (night == '1') {
				document.body.classList.add('night');
			}
		}
	}
});
var loginStatus = false;
function isLogin(option) {
	if (!loginStatus) {
		request({
			url: config.api.login_check,
			async: false,
			loading: false,
			success: function (result) {
				loginStatus = option ? result.data : result.data.status;
			}
		});
	}
	return loginStatus;
}
/*退出当前账号*/
function exit() {
	// layer.confirm("确定要退出登录吗?", function () {
		request({
			url: config.api.login_out, done: function () {
				loginStatus = false;
			}
		});
	// });
}
$(".header-user").on("click", ".layui-icon-username", function () {
	if ($(".show-data .logined").length > 0) {
		location = "/user/index.html";
	} else {
		openLoginK();
	}
});
function openLoginK() {
	location = "/user/login/index.html";
}
function request(option) {
	if (typeof (option) !== 'object') {
		console.warn("option is not a 'object'");
		return false;
	}
	if (typeof (layer) === 'undefined') {
		layui.use('layer', ajx(true));
	} else {
		ajx();
	}
	if (typeof (option.loading) !== 'boolean') {
		var loading = layer.load(1);
	}

	function ajx(o) {
		if (o) {
			layer = layui.layer;
		}
		$.ajax({
			url: option.url || location.pathname,
			data: option.data || null,
			dataType: option.dataType || 'JSON',
			type: option.type || 'post',
			async: typeof (option.async) === 'boolean' ? option.async : true,
			success: option.success || function (res) {
				if (res.data) {
					var delay = res.data.delay || 0;
					delay && (delay *= 1000);
					res.data.redirect && (setTimeout(function () {
						location = res.data.redirect;
					}, delay));
					res.data.reload && (option.reload = parseFloat(res.data.reload));
					if (res.data.alert) {
						res.msg && layer.open({
							type: 0,
							shadeClose: true,
							shade: ["0.6", "#7186a5"],
							skin: 'atuikeLayerSkin1',
							content: res.msg
						});
					}
				}
				if (!res.data || !res.data.alert) {
					var cfg = typeof (res.data.icon) !== "boolean" ? {
						// icon: (res.code || 0),
						// offset: '20%'
					} : {};
					res.msg && layer.msg(res.msg, cfg);
				}
				option.done && option.done(res);
			},
			complete: function () {
				if (typeof (option.loading) !== 'boolean') {
					layer.close(loading);
				}
				setTimeout(function () {
					var ret = option.reload || false;
					if (ret) {
						ret = (typeof (ret === 'number')) ? ret : 0;
						setTimeout(function () {
							location.reload();
						}, ret * 1000);
					}
				}, 10);
			},
			error: option.error || function (e) {
				layer.closeAll();
				layer.msg('网络异常:' + e.statusText || e.statusMessage);
			}
		});
	}
}

function switchNightMode() {
	var night = document.cookie.replace(/(?:(?:^|.*;\s*)night\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '0';
	if (night == '0') {
		document.body.classList.add('night');
		document.cookie = "night=1;path=/"
		//console.log('夜间模式开启');
	} else {
		document.body.classList.remove('night');
		document.cookie = "night=0;path=/"
		//console.log('夜间模式关闭');
	}
}

if ($(".backtop").length > 0) {
	backtopS();
	$(window).scroll(function(e) {
		backtopS();
	});

	function backtopS() {
		var scroH = $(this).scrollTop();
		var footerHeight = 0;
		if ($('.footer').length > 0) {
			mTop = $('.footer')[0].offsetTop;
			footerHeight = footerHeight + $(".footer").outerHeight();
		} else {
			mTop = $('.footer')[0].offsetTop;
		}
		footerHeight = $(".footer").outerHeight() - parseInt($("footer").css("margin-top")) - parseInt($("footer").css("margin-top")) - parseInt($("footer").css("margin-top"));
		sTop = $(window).scrollTop();
		result = mTop - sTop - parseInt($("footer").css("margin-top"));
		if (scroH > 200) {
			$(".backtop").fadeIn(400);
			if (scroH >= $("body").height() - $(window).height() - footerHeight) {
				$(".backtop").css("bottom", $(window).height() - result);
			} else {
				$(".backtop").css("bottom", "");
			}
		} else {
			$(".backtop").fadeOut(400);
		}
	}
}
$(".backtop").click(function(e) {
	$('body,html').animate({
		scrollTop: 0
	}, 800);
});

function search() {
	var q = document.getElementById("q").value;
	window.location.href = "/search/" + q + ".html";
}
$(document).ready(function() {
	$("img.lazy").lazyload({
		effect: 'fadeIn',
		threshold: '400',
		failurelimit: '30',
		skip_invisible: 'false'
	});
	$("img.lazy2").lazyload({
		effect: 'fadeIn',
		event: 'mouseover',
		threshold: '400',
		failurelimit: '6'
	});
});

function ffix() {
	var u;
	if (floatFix) {
		var n = $(".right.public .box"),
			t = $(window).scrollTop(),
			f = n.offset().top - t,
			i = n.height(),
			r = $(window).height();
		i + 40 > r ? floatTop < t - (i + 40 - r) ? n.hasClass("fix-bottom") || n.addClass("fix-bottom") : n.hasClass("fix-bottom") && n.removeClass("fix-bottom") : floatTop < 60 + t ? n.hasClass("fix-top") || n.addClass("fix-top") : n.hasClass("fix-top") && n.removeClass("fix-top");
		u = $(window).height() - $(".footer").offset().top + $(window).scrollTop() + 20;
		u > 0 ? n.hasClass("at-bottom") || n.addClass("at-bottom") : n.hasClass("at-bottom") && n.removeClass("at-bottom");
		lastScrollTop = t
	}
}

// r-list 切换
$(".r-list .bar li").hover(function () {
	var tabList = $(this).parent().parent();
	tabList.find(".bar li").removeClass("sel");
	//$("#rank .bar li").removeClass("sel");
	$(this).addClass("sel");

	tabList.find(".bd").removeClass("sel");
	//$("#rank .bd").removeClass("sel");
	//$("#rank .cm").removeClass("sel");
	var id = "#d-" + $(this).data("id");
	tabList.find(id).addClass("sel");
});
$(".article-tit .info span:last-child").click(function() {
	var target_top = $(".article-pinglun").offset().top;
	$("html,body").animate({scrollTop: target_top}, 1000);
});


var emojiLists=[{name:"\u963f\u9c81",path:"aru/",maxNum:164,excludeNums:[],file:".png",placeholder:"[aru_{alias}]"},{name:"QQ",path:"qq/",file:".gif",placeholder:"[{alias}]",emoji:{doge:"doge","\u659c\u773c\u7b11":"xieyanxiao","\u6258\u816e":"tuosai","\u5c0f\u7ea0\u7ed3":"xiaojiujie","\u7b11\u54ed":"xiaoku","\u55b7\u8840":"penxue","\u5fae\u7b11":"weixiao","\u6487\u5634":"piezui","\u8272":"se","\u53d1\u5446":"fadai","\u5f97\u610f":"deyi","\u6d41\u6cea":"liulei","\u5bb3\u7f9e":"haixiu","\u95ed\u5634":"bizui",
		"\u7761":"shui","\u5927\u54ed":"daku","\u5c34\u5c2c":"ganga","\u5472\u7259":"ziya","\u53d1\u6012":"fanu","\u8c03\u76ae":"tiaopi","\u60ca\u8bb6":"jingya","\u96be\u8fc7":"nanguo","\u9177":"ku","\u51b7\u6c57":"lenhan","\u6293\u72c2":"zhuakuang","\u5410":"tu","\u5077\u7b11":"touxiao","\u53ef\u7231":"keai","\u767d\u773c":"baiyan","\u50b2\u6162":"aoman","\u9965\u997f":"jie","\u56f0":"kun","\u60ca\u6050":"jingkong","\u6d41\u6c57":"liuhan","\u61a8\u7b11":"hanxiao","\u5927\u5175":"dabing","\u594b\u6597":"fendou",
		"\u5492\u9a82":"zhouma","\u7591\u95ee":"yiwen","\u5618":"xu","\u6655":"yun","\u6298\u78e8":"zhemo","\u8870":"fade","\u9ab7\u9ac5":"kulou","\u6572\u6253":"qiaoda","\u518d\u89c1":"zaijian","\u64e6\u6c57":"cahan","\u62a0\u9f3b":"koubi","\u9f13\u638c":"guzhang","\u55c5\u5927\u4e86":"qiudale","\u574f\u7b11":"huaixiao","\u5de6\u54fc\u54fc":"zuohengheng","\u53f3\u54fc\u54fc":"youhengheng","\u54c8\u6b20":"haqian","\u9119\u89c6":"bishi","\u59d4\u5c48":"weiqu","\u53ef\u601c":"kelian","\u9634\u9669":"yinxian",
		"\u4eb2\u4eb2":"qinqin","\u5413":"xia","\u5feb\u54ed\u4e86":"kuaikule","\u83dc\u5200":"caidao","\u897f\u74dc":"xigua","\u5564\u9152":"pijiu","\u7bee\u7403":"lanqiu","\u4e52\u4e53":"pingpong","\u5496\u5561":"kafei","\u996d":"fan","\u732a\u5934":"zhutou","\u73ab\u7470":"meigui","\u51cb\u8c22":"diaoxie","\u5fc3":"xin","\u5fc3\u788e":"xinsui","\u86cb\u7cd5":"dangao","\u95ea\u7535":"shandian","\u70b8\u5f39":"zhadan","\u5200":"dao","\u8db3\u7403":"zuqiu","\u74e2\u866b":"piaochong","\u4fbf\u4fbf":"bianbian",
		"\u591c\u665a":"yewan","\u592a\u9633":"taiyang","\u793c\u7269":"liwu","\u62e5\u62b1":"yongbao","\u5f3a":"qiang","\u5f31":"ruo","\u63e1\u624b":"woshou","\u80dc\u5229":"shengli","\u62b1\u62f3":"baoquan","\u52fe\u5f15":"gouyin","\u62f3\u5934":"quantou","\u5dee\u52b2":"chajin","\u7231\u4f60":"aini",NO:"NO",OK:"OK","\u7231\u60c5":"aiqing","\u98de\u543b":"feiwen","\u53d1\u8d22":"facai","\u5e05":"shuai","\u96e8\u4f1e":"yusan","\u9ad8\u94c1\u5de6\u8f66\u5934":"gaotiezuochetou","\u8f66\u53a2":"chexiang","\u9ad8\u94c1\u53f3\u8f66\u5934":"gaotieyouchetou",
		"\u7eb8\u5dfe":"zhijin","\u53f3\u592a\u6781":"youtaiji","\u5de6\u592a\u6781":"zuotaiji","\u732e\u543b":"xianwen","\u8857\u821e":"jiewu","\u6fc0\u52a8":"jidong","\u6325\u52a8":"huidong","\u8df3\u7ef3":"tiaosheng","\u56de\u5934":"huitou","\u78d5\u5934":"ketou","\u8f6c\u5708":"zhuanquan","\u6004\u706b":"ouhuo","\u53d1\u6296":"fadou","\u8df3\u8df3":"tiaotiao","\u7206\u7b4b":"baojin","\u6c99\u53d1":"shafa","\u94b1":"qian","\u8721\u70db":"lazhu","\u67aa":"gun","\u706f":"deng","\u9999\u8549":"xiangjiao",
		"\u543b":"wen","\u4e0b\u96e8":"xiayu","\u95f9\u949f":"naozhong","\u56cd":"xi","\u68d2\u68d2\u7cd6":"bangbangtang","\u9762\u6761":"miantiao","\u8f66":"che","\u90ae\u4ef6":"youjian","\u98ce\u8f66":"fengche","\u836f\u4e38":"yaowan","\u5976\u74f6":"naiping","\u706f\u7b3c":"denglong","\u9752\u86d9":"qingwa","\u6212\u6307":"jiezhi","K\u6b4c":"Kge","\u718a\u732b":"xiongmao","\u559d\u5f69":"hecai","\u8d2d\u7269":"gouwu","\u591a\u4e91":"duoyun","\u97ad\u70ae":"bianpao","\u98de\u673a":"feiji","\u6c14\u7403":"qiqiu"}},
	{name:"\u5fae\u535a",path:"weibo/",file:".png",placeholder:"[wb_{alias}]",emoji:{doge:"doge",miao:"miao",dog1:"dog1","\u4e8c\u54c8":"erha",dog2:"dog2",dog3:"dog3",dog4:"dog4",dog5:"dog5",dog6:"dog6",dog7:"dog7",dog8:"dog8",dog9:"dog9",dog10:"dog10",dog11:"dog11",dog12:"dog12",dog13:"dog13",dog14:"dog14",dog15:"dog15","\u5e76\u4e0d\u7b80\u5355":"bingbujiandan","\u5141\u60b2":"yunbei","\u5403\u74dc":"chigua","\u8d39\u89e3":"feijie","\u7b11\u800c\u4e0d\u8bed":"xiaoerbuyu","\u61a7\u61ac":"chongjing",
			"\u8214\u5c4f":"tianping","\u6c61":"wu","\u8dea\u4e86":"guile","\u62b1\u62b1":"baobao","\u644a\u624b":"tanshou","\u7231\u4f60":"aini","\u5965\u7279\u66fc":"aoteman","\u62dc\u62dc":"baibai","\u60b2\u4f24":"beishang","\u9119\u89c6":"bishi","\u95ed\u5634":"bizui","\u998b\u5634":"chanzui","\u5403\u60ca":"chijing","\u6253\u54c8\u6c14":"dahaqi","\u6253\u8138":"dalian","\u9876":"ding","\u80a5\u7682":"feizao","\u611f\u5192":"ganmao","\u9f13\u638c":"guzhang","\u54c8\u54c8":"haha","\u5bb3\u7f9e":"haixiu","\u5475\u5475":"hehe",
			"\u9ed1\u7ebf":"heixian","\u54fc":"heng","\u82b1\u5fc3":"huaxin","\u6324\u773c":"jiyan","\u53ef\u7231":"keai","\u53ef\u601c":"kelian","\u9177":"ku","\u56f0":"kun","\u61d2\u5f97\u7406\u4f60":"landelini","\u6cea":"lei","\u7537\u5b69\u513f":"nanhaier","\u6012":"nu","\u6012\u9a82":"numa","\u5973\u5b69\u513f":"nvhaier","\u94b1":"qian","\u4eb2\u4eb2":"qinqin","\u50bb\u773c":"shayan","\u751f\u75c5":"shengbing","\u795e\u517d":"shenshou","\u5931\u671b":"shiwang","\u8870":"shuai","\u7761\u89c9":"shuijiao",
			"\u601d\u8003":"sikao","\u592a\u5f00\u5fc3":"taikaixin","\u5077\u7b11":"touxiao","\u5410":"tu","\u5154\u5b50":"tuzi","\u6316\u9f3b\u5c4e":"wabishi","\u59d4\u5c48":"weiqu","\u7b11\u54ed":"xiaoku","\u718a\u732b":"xiongmao","\u563b\u563b":"xixi","\u5618":"xu","\u9634\u9669":"yinxian","\u7591\u95ee":"yiwen","\u53f3\u54fc\u54fc":"youhengheng","\u6655":"yun","\u6293\u72c2":"zhuakuang","\u732a\u5934":"zhutou","\u6700\u53f3":"zuiyou","\u5de6\u54fc\u54fc":"zuohengheng","\u7ed9\u529b":"geili","\u4e92\u7c89":"hufen",
			"\u56e7":"jiong","\u840c":"meng","\u795e\u9a6c":"shenma",v5:"v5","\u56cd":"xi","\u7ec7":"zhi"}},{name:"\u8d34\u5427",path:"newtieba/",file:".png",placeholder:"#({alias})",emoji:{"\u5475\u5475":"hehe","\u54c8\u54c8":"hahaxiao","\u5410\u820c":"tushe","\u592a\u5f00\u5fc3":"taikaixin","\u7b11\u773c":"xiaoyan","\u82b1\u5fc3":"huaxin","\u5c0f\u4e56":"xiaoguai","\u4e56":"guai","\u6342\u5634\u7b11":"wuzuixiao","\u6ed1\u7a3d":"huaji","\u4f60\u61c2\u7684":"nidongde","\u4e0d\u9ad8\u5174":"bugaoxing","\u6012":"nu",
			"\u6c57":"han","\u9ed1\u7ebf":"heixian","\u6cea":"lei","\u771f\u68d2":"zhenbang","\u55b7":"pen","\u60ca\u54ed":"jingku","\u9634\u9669":"yinxian","\u9119\u89c6":"bishi","\u9177":"ku","\u554a":"a","\u72c2\u6c57":"kuanghan",what:"what","\u7591\u95ee":"yiwen","\u9178\u723d":"suanshuang","\u5440\u54a9\u7239":"yamiedie","\u59d4\u5c48":"weiqu","\u60ca\u8bb6":"jingya","\u7761\u89c9":"shuijiao","\u7b11\u5c3f":"xiaoniao","\u6316\u9f3b":"wabi","\u5410":"tu","\u7280\u5229":"xili","\u5c0f\u7ea2\u8138":"xiaohonglian",
			"\u61d2\u5f97\u7406":"landeli","\u52c9\u5f3a":"mianqiang","\u7231\u5fc3":"aixin","\u5fc3\u788e":"xinsui","\u73ab\u7470":"meigui","\u793c\u7269":"liwu","\u5f69\u8679":"caihong","\u592a\u9633":"taiyang","\u661f\u661f\u6708\u4eae":"xingxingyueliang","\u94b1\u5e01":"qianbi","\u8336\u676f":"chabei","\u86cb\u7cd5":"dangao","\u5927\u62c7\u6307":"damuzhi","\u80dc\u5229":"shengli",haha:"haha",OK:"OK","\u6c99\u53d1":"shafa","\u624b\u7eb8":"shouzhi","\u9999\u8549":"xiangjiao","\u4fbf\u4fbf":"bianbian","\u836f\u4e38":"yaowan",
			"\u7ea2\u9886\u5dfe":"honglingjin","\u8721\u70db":"lazhu","\u97f3\u4e50":"yinyue","\u706f\u6ce1":"dengpao"}},{name:"\u65e7\u8d34\u5427",path:"tieba/",file:".png",placeholder:"#[{alias}]",emoji:{"\u5475\u5475":"hehe","\u54c8\u54c8":"hahaxiao","\u5410\u820c":"tushe","\u554a":"a","\u9177":"ku","\u6012":"nu","\u5f00\u5fc3":"kaixin","\u6c57":"han","\u6cea":"lei","\u9ed1\u7ebf":"heixian","\u9119\u89c6":"bishi","\u4e0d\u9ad8\u5174":"bugaoxing","\u771f\u68d2":"zhenbang","\u94b1":"qian","\u7591\u95ee":"yiwen",
			"\u9634\u9669":"yinxian","\u5410":"tu","\u54a6":"yi","\u59d4\u5c48":"weiqu","\u82b1\u5fc3":"huaxin","\u547c":"hu","\u7b11\u773c":"xiaoyan","\u51b7":"len","\u592a\u5f00\u5fc3":"taikaixin","\u6ed1\u7a3d":"huaji","\u52c9\u5f3a":"mianqiang","\u72c2\u6c57":"kuanghan","\u4e56":"guai","\u7761\u89c9":"shuijiao","\u60ca\u54ed":"jingku","\u751f\u6c14":"shengqi","\u60ca\u8bb6":"jingya","\u55b7":"pen","\u73ab\u7470":"meigui","\u793c\u7269":"liwu","\u5f69\u8679":"caihong","\u94b1\u5e01":"qianbi","\u706f\u6ce1":"dengpao",
			"\u8336\u676f":"chabei","\u97f3\u4e50":"yinyue",haha:"haha","\u80dc\u5229":"shengli","\u5927\u62c7\u6307":"damuzhi","\u5f31":"ruo"}}];
