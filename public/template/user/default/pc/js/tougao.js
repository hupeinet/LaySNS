
var editorList = document.querySelectorAll(".editor");
if (editorList.length > 0) {
	$.each(editorList, function (i, v) {
		var $this=$(this);
		$this.hide();
		var editorId=$this.attr("name");
		$(this).before('<div  id="'+editorId+'"></div>');
		const editor = new wangEditor( '#'+editorId);
		//TODO 上传代码
		editor.config.uploadImgServer = config.api.editor_upload_img
		editor.config.uploadFileName = 'file'
		editor.config.uploadImgHooks = {

			fail: function(xhr, editor, resData) {
				console.log(resData.msg)
			},
			// 上传图片超时
			timeout: function(xhr) {
				console.log('timeout')
			}
		}
		editor.config.uploadVideoServer = config.api.editor_upload_file
		editor.config.uploadVideoName = 'file'
		editor.config.uploadVideoHooks = {

			fail: function(xhr, editor, resData) {
				layer.msg(resData.msg);
			},
			// 上传图片超时
			timeout: function(xhr) {
				console.log('timeout')
			}
		}
		editor.config.onchange = function (html) {
			// 第二步，监控变化，同步更新到 textarea
			$this.val(html)
		}
		editor.create();
		editor.txt.html($this.val());
	});
}

var _name = 'USER_TG_FORM_DATA_CACHE';
var btnText = $(".atuikeBtnSkin1").html();
layui.use(['form', 'layer', 'upload', 'element'], function () {
	var form = layui.form;
	var layer = layui.layer;
	var upload = layui.upload;
	var element = layui.element;
	form.verify({
		title:function (val,item) {
			if (val.length<2){
				return "标题过于简短的教程不会被收录";
			}
		}
	});

	$("#notice1").mouseover(function() {
		layer.tips('如文章中有图片，可以不必传。系统会默认读取内容中第一张的图片为封面图', this, {
			tips: [1, "#000"],'time':3000
		});
	});
	//监听提交
	form.on('submit(submitAll)', function (data) {

		if($(".atuikeBtnSkin1").html()!=btnText){
			return false;
		}

		// var content = editor.getContent();
		var auto_pass = $("input[name=auto_pass]");
		// if (( data.field.type==='text' && editor.getContentTxt().length<10 )){
		//     layer.msg('文章过于简短,',{icon:5});
		//     return false;
		// }
		if (data.field.auto_pass &&auto_pass.data('exempt')<1){
			layer.msg('免审次数不足,请使用普通投稿方式!',{icon:5});
			return false;
		}
		// data.field.content= content;
		$(".atuikeBtnSkin1").html('<i class="layui-icon layui-icon-loading layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="color:#FFF;font-size:24px;"></i>');
		request({
			url:config.api.contribute,
			data:data.field ,
			success:function (result) {
				var open_data ={
					type: 0,
					shadeClose: true,
					skin: 'atuikeLayerSkin1',
					content: result.msg
				};

				if(result.code===1){
					auto_pass.data('exempt',result.exempt_num);
					$("#exempt").text(result.exempt_num);
					result.code===1&&( open_data.btn=['我知道了','取消']);
					open_data.yes = function(){
						location.reload();
					};
					form_reset(data);
				}
				layer.open(open_data);
				$(".atuikeBtnSkin1").html(btnText);
			}
		});
		return false
	});

	//奖励开关
	form.on('switch(payopen)', function (data) {
		if (data.elem.checked) {
			$(".pays").slideDown();
		} else {
			$(".pays").slideUp(100);
		}
	});

	//纯文章和附件
	form.on('radio(typeradio)', function (data) {
		if (data.value === "file") {
			$(".zips").slideDown();
		} else {
			$(".zips").slideUp(100);
		}
	});

	//监听上传封面
	var uploadImg = $(".uploadImg");
	var uploadInst = upload.render({
		elem: uploadImg,
		auto: false,
		accept:'image',
		//acceptMime:'image/*',
		choose: function (obj) {
			uploadImg.html('<i class="layui-icon layui-icon-loading layui-icon layui-anim layui-anim-rotate layui-anim-loop"></i>');
			obj.preview(function(index, file, result){
				$("input[name=cover_img]").val(encodeURIComponent(result));
			});
			setTimeout(function () {
				uploadImg.css({'border-color':'#00ca5f','color':'#00ca5f'}).html("封面已选择");
			}, 1000);
		}
	});

	uploadImg.hover(function () {
		var box = $("div.coverBox");
		if (uploadImg.siblings("input[name=cover_img]").val().length>10){
			box.fadeIn(200);   box.find("img")[0].src=  decodeURIComponent(uploadImg.siblings("input[name=cover_img]").val());
		}
	},function () {
		$("div.coverBox").fadeOut(100)
	});

	//监听上传附件

	var pro = $(".jdt .layui-progress-bar");
	var fz = parseInt("200")*1024;
	var upload_element =  $(".zipupload");
	var softloadInst = upload.render({
		elem: upload_element,
		url: config.api.upload,
		auto: true,
		// field:'file_name',
        data: {
		  id: 1
        },
		size:fz,
		accept: 'file',
		acceptMime:'application/zip,application/rar,video/mp4,application/vnd.android.package-archive',
		exts: 'zip|rar|mp4|apk',
		progress: function (value) {	 //上传进度回调 value进度值
			var bfb = value + "%";
			pro.width(bfb );  upload_element.css('color','#00ca5f').html(bfb);
			if ( value >= 100) {
				$(".jdt").fadeOut(500);
			}
		},
		choose: function () {
			$(".zipupload").html("上传中..");
			$(".jdt").slideDown(300);
		},
		done: function (result) {
			layer.msg(result.msg,{icon:result.code});
			if(result.code===1){
				$('#down_link1').val(result.data.url);
				upload_element.css({'border-color':'#00ca5f','color':'#00ca5f'}).html("<i class='layui-icon layui-icon-ok-circle'></i>上传成功");
			}else{
				upload_element.css({'border-color':'#e73f49','color':'#e73f49'}).html("<i class='layui-icon layui-icon-face-surprised'></i>上传失败");
			}
			setTimeout(function () {
				pro.width(0);
			},100)
		}
	});

	function form_reset(data) {

		var thisForm = $(data.elem).parents(".layui-form");
		thisForm.find(".layui-form-item").each(function (i,e) {
			$(e).find("input[type=text],input[type=hidden]").val("");
			$(e).find(".layui-form-onswitch").trigger("click");
			$(e).find(".uploadImg").css({'color':'#888','border-color':'#888'}).html('<i class="layui-icon layui-icon-picture-fine"></i>上传');
			$(e).find(".layui-form-radio").eq(0).trigger("click");
			$(e).find('.zipupload').css({'color':'#888','border-color':'#888'}).html('<i class="layui-icon layui-icon-upload-drag"></i>上传附件');
		});
		$("#myEditor").html("");
		layui.data(_name,null);
		form.render();
	}

});


/**
 * 设置缓存
 * */
// setInterval(function () {
//     var formData = $("#list_form").field();
//     var cacheData = {};
//     if (formData.title && editor.hasContents()){
//         cacheData.title = formData.title;
//         cacheData.content= editor.getContent();
//         layui.data(_name, {key:_name, value:cacheData});
//     }
// },3000);

/**
 * 恢复缓存
 */
var cacheData = layui.data(_name)[_name];
if (cacheData){
	$(".bj-content").append('<div class="contentTask">检测到您之前可能有编辑的文案，您可以选择<a class="hfContent" onclick="javaScript:restoreFormData();">恢复</a></div>');
}
function restoreFormData() {
	cacheData = layui.data(_name)[_name];
	$("#list_form").find("input[name=title]").val(cacheData.title);
	$("#myEditor").html(cacheData.content);
	$(".contentTask").remove();
}