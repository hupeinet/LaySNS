
	layui.use('form', function(){
		var form = layui.form;
		//监听提交
		form.on('submit(login)', function(data){
			if($("input[name='object']").eq(0).val() == ""){
				layer.msg('请输入用户名');
				$("input[name='object']").focus();return false;
			}

			$.ajax({
				cache: true,
				type: "POST",
				url:config.api.login,
				data:data.field,
				success: function(res) {
					if(res.code==1){
						layer.msg(res.msg, {icon:16, shade: 0.1, time:3000});

						setTimeout(function () { location.href=res.data.redirect }, 1000);
					}else{
						$('#captchaPic').click();
						layer.msg(res.msg);
					}
				}
			});
			return false;
		});


		form.on('submit(register)', function(data){
			if($("input[name='username']").eq(0).val() == ""){
				layer.msg('请输入用户名');
				$("input[name='username']").focus();
			}else if($("input[name='password']").eq(0).val() == ""){
				layer.msg('请输入密码');
				$("input[name='password']").focus();
			}
			else if($("input[name='captcha']").eq(0).val() == ""){
				layer.msg('请输入验证码');
				$("input[name='captcha']").focus();
			}else{
				$.ajax({
					cache: true,
					type: "POST",
					url:config.api.register,
					data:data.field,
					success: function(res) {
						if(res.code==1){
							layer.msg(res.msg, {icon:16, shade: 0.1, time:3000});
							setTimeout(function () {
								location.href = res.data.redirect;
							}, 1000);
						}else{
							layer.msg(res.msg);
						}
						$('#captchaPic').trigger("click");
						$('#captcha').val("");
					}
				});
			}
			return false;
		});

		form.on("submit(forget)", function (data) {
			request({
				url: config.api.forgetpass,
				data: data.field,
				done: function (res) {
					$("#captchaPic").click();
				}
			});
			return false;
		});


		form.on("submit(change_pwd)", function (data) {

			request({
				url: config.api.changepass,
				data: data.field,
				done: function (res) {
					$("#captchaPic").click();
				}
			});
			return false;

			alert(1);
			if($("input[name='pwd']").eq(0).val() == ""){
				layer.msg('请输入新密码');
				$("input[name='pwd']").focus();
			}
			else if($("input[name='pwdok']").eq(0).val() == ""){
				layer.msg('输入确认新密码');
				$("input[name='pwdok']").focus();
			}else{
				$.ajax({
					cache: true,
					type: "POST",
					url:"/user/resetpassword.php",
					data:$('#form1').serialize(),// 你的form里面的id
					success: function(datas) {
						if(datas=="success"){
							layer.msg('修改成功，请牢记新密码！', {icon:1, shade: 0.1, time:3000});

							setTimeout(function () { window.location.href = "/user/"; }, 1000);
						}else{
							layer.msg(datas);
						}
					}
				});
			}
		});


		form.on('submit(changeinfo)', function (data) {
			request({
				url:config.api.user_changeinfo,
				data:data.field,
				success:function (data) {
					data.msg&&layer.msg(data.msg);
					// data.msg&&layer.open({
					//     type: 0,
					//     shadeClose: true,
					//     skin: 'atuikeLayerSkin1',
					//     content: data.msg
					// });
				}
			});


			return false;
		});

	});





function logintypechange(obj) {

	if($('#login_type').val()=='common'){

		$('#password-div').hide();
		$('#code-div').show();
		$('#login_type').val('code');
		$('#object').attr('placeholder','手机号或邮箱');
		$(obj).html('密码登录');
	}else{
		$('#password-div').show();
		$('#code-div').hide();
		$('#login_type').val('common');
		$('#object').attr('placeholder','用户名/手机号/邮箱');
		$(obj).html('验证码登录');
	}
}

$('#send_code').on('click', function () {
	var obj = $(this);
	var options = {
		seconds: 60
		, type: 'LOGIN'
		, object: $('#object').val()
		, captcha: $('#captcha').val()
	};

	var url = config.api.send_code;
	var loading = layer.load(0, {shade: false, time: 2 * 1000});
	$.post(url, options, function (data) {
		layer.close(loading);
		if (data.code == 1) {
			layer.msg(data.msg);
			countDown(60, obj);
		} else {
			layer.alert(data.msg);
			$('#captchaPic').trigger("click");
		}
	});
});
function countDown(seconds, obj) {

	if (seconds > 1) {

		seconds--;

		$(obj).attr('disabled', true).html(seconds + "秒");//禁用按钮

		// 定时1秒调用一次

		setTimeout(function () {

			countDown(seconds, obj);

		}, 1000);

	} else {

		$(obj).attr('disabled', false).html("发送验证码");//启用按钮
		$('#captchaPic').trigger("click");
		$('#captcha').val("");
	}
}



function resetpwd(){
	if($("input[name='mail']").eq(0).val() == ""){
		layer.msg('请输入您的邮箱');
		$("input[name='mail']").focus();
	}
	else if($("input[name='vdcode']").eq(0).val() == ""){
		layer.msg('请输入验证码');
		$("input[name='vdcode']").focus();
	}else{
		$.ajax({
			cache: true,
			type: "POST",
			url:"/user/resetpassword.php",
			data:$('#form1').serialize(),// 你的form里面的id
			success: function(datas) {
				if(datas=="success"){
					layer.msg('提交成功，已发送验证邮件！', {icon:1, shade: 0.1, time:3000});

					//setTimeout(function () { window.location.href = "/user/"; }, 5000);
				}else{
					layer.msg(datas);
				}
			}
		});
	}
}


function upinfo(){
	if($("input[name='email']").eq(0).val() == ""){
		layer.msg('请输入您的邮箱');
		$("input[name='email']").focus();
	}
	else if($("input[name='qq']").eq(0).val() == ""){
		layer.msg('请输入QQ');
		$("input[name='qq']").focus();
	}
	else if($("input[name='vdcode']").eq(0).val() == ""){
		layer.msg('请输入验证码');
		$("input[name='vdcode']").focus();
	}else{
		$.ajax({
			cache: true,
			type: "POST",
			url:"/user/myinfo.php",
			data:$('#save').serialize(),// 你的form里面的id
			success: function(datas) {
				if(datas=="success"){
					layer.msg('资料保存成功！', {icon:1, shade: 0.1, time:3000});

					setTimeout(function () { window.location.reload() }, 1000);
				}else{
					layer.msg(datas);
				}
			}
		});
	}
}
function reguser(){

	if($("input[name='uname']").eq(0).val() == ""){
		layer.msg('请输入您的昵称');
		$("input[name='uname']").focus();
	}else if($("input[name='email']").eq(0).val() == ""){
		layer.msg('请输入您的邮箱');
		$("input[name='email']").focus();
	}
	else if($("input[name='qq']").eq(0).val() == ""){
		layer.msg('请输入QQ');
		$("input[name='qq']").focus();
	}
	else if($("input[name='vdcode']").eq(0).val() == ""){
		layer.msg('请输入验证码');
		$("input[name='vdcode']").focus();
	}else{
		$.ajax({
			cache: true,
			type: "POST",
			url:"/user/reg_new.php",
			data:$('#regUser').serialize(),// 你的form里面的id
			success: function(datas) {
				if(datas=="success"){
					layer.msg('资料保存成功！', {icon:1, shade: 0.1, time:3000});

					setTimeout(function () { window.location.href = "/user/"; }, 1000);
				}else{
					layer.msg(datas);
				}
			}
		});
	}
}
function xgpwd(){
	if($("input[name='userpwd']").eq(0).val() == ""){
		layer.msg('请输入新密码');
		$("input[name='userpwd']").focus();
	}
	else if($("input[name='userpwdok']").eq(0).val() == ""){
		layer.msg('请输入确认密码');
		$("input[name='userpwdok']").focus();
	}
	else if($("input[name='vdcode']").eq(0).val() == ""){
		layer.msg('请输入验证码');
		$("input[name='vdcode']").focus();
	}else{
		$.ajax({
			cache: true,
			type: "POST",
			url:"/user/mypassword.php",
			data:$('#xgpwd').serialize(),// 你的form里面的id
			success: function(datas) {
				if(datas=="success"){
					layer.msg('修改成功，请牢记新密码！', {icon:1, shade: 0.1, time:3000});

					setTimeout(function () { window.location.reload() }, 1000);
				}else{
					layer.msg(datas);
				}
			}
		});
	}
}
var bodyBgs = [];
    bodyBgs[0] = "/static/images/01.jpg";
    bodyBgs[1] = "/static/images/02.jpg";
    bodyBgs[2] = "/static/images/03.jpg";
    bodyBgs[3] = "/static/images/04.jpg";
    bodyBgs[4] = "/static/images/05.jpg";
    var randomBgIndex = Math.round( Math.random() * 4 );
//输出随机的背景图
   document.write('<style>.user-login .box{background:url(' + bodyBgs[randomBgIndex] + ') center;background-size: cover;}</style>');
