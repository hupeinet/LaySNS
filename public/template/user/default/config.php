<?php
return array(
'title'=>'用户中心默认模板',		                                             //模板名称
'author'=>'云阳',			                                                 //模板作者
'version'=>'1.0',			                                                 //模板版本
'url'=>'https://bbs.laysns.com/app/fee34a165ef730163ad3a150f80cd762.html',	 //应用中心详情页地址
'demo_url'=>'http://sns.laysns.com/user',	                                 //演示地址
'description'=>'Laysns资源系统全新打造，从心出发'                                 //模板版权/介绍
);