define(["jquery", "easy-admin"], function ($, ea) {

    var element = layui.element,
    table = layui.table,
    laytpl = layui.laytpl;
    var param=window.location.search.substring(1);
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'main.collect/list?'+param,
        down_url: ea.url('main.collect/download'),

    };

    var Controller = {

        index: function () {

                ea.request.get({
                    url: 'main.collect/gather',
                    prefix: true
                }, function (res) {

                    if (res.code == 1) {

                        laytpl($("#temp").html()).render(res.data, function (html) {
                            $("#ul-collapse").html(html);

                            element.init();

                            $(".view-li").on('click', function () {
                                var site_name = $(this).attr("data-site");
                                var site_type = $(this).data("sitetype");
                                var name = $(this).attr("data-name");
                                var index = layer.open({
                                    type: 2,
                                    title: '[' + name + ']' + ' 资源列表',
                                    content: '/admin/main.collect/list?sitetype='+site_type+'&site_name=' + site_name,
                                    area: ['50%', '70%'],
                                });
                                layer.full(index);
                            })
                            $(".timing").on('click', function () {
                                layer.alert(jobUrl+'&site_name='+$(this).data('sitename'));
                            })
                        });
                    } else {
                        layer.msg(res.msg, {icon: 2, anim: 6});
                    }

            });

            ea.listen();
        },
        log: function () {
            ea.table.render({

                init: {
                    table_elem: '#currentTable',
                    table_render_id: 'currentTableRenderId',
                    index_url: 'main.collect/log',
                    delete_url: 'main.collect/delete',
                    export_url: 'main.collect/export',
                },
                toolbar: ['refresh',
                    'delete', 'export'],

                limit: 20,
                cols: [[
                    {type: "checkbox"},
                    {field: 'uniquekey', width: 80, title: '资源ID'},
                    {field: 'title', width: 280, title: '标题'},
                    {field: 'subject', width: 280, title: '类型'},
                    {field: 'url', minWidth: 80, title: '原文链接'},
                    {field: 'create_time', minWidth: 80, title: '采集时间', search: 'range'},
                    {
                        width: 100,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'delete'
                        ]
                    }
                ]],
            });
            ea.listen();
        },
        list: function () {
            ea.table.render({
                init: init,
                toolbar: [],
                defaultToolbar:false,
                limit:20,
                cols: [[
                    {type: "checkbox"},
                    {field: 'uniquekey', width: 80, title: '资源ID'},
                    {field: 'cover_img', minWidth: 80, title: '封面', templet: ea.table.image},
                    {field: 'title', width: 280, title: '标题'},
                    {field: 'category', width: 280, title: '分类'},
                    // {field: 'source_url', minWidth: 80, title: '原文链接'},
                     {field: 'ico', minWidth: 80, title: '站标'},
                     {field: 'logo', minWidth: 80, title: '网站LOGO'},


                    {field: 'publish_time', minWidth: 180, title: '发布时间', search: 'range'},
                    {field: 'collected', title: '采集状态', align: 'center',
                        templet: function (d) {
                            return "<div id='" + d.uniquekey + "'>"+(d.collected==1?'已采集':'未采集')+"</div>";
                        }
                    },
                    {title: "操作", width: 150,  align: "center", templet: "#operat"}
                ]],
                done: function(index, layero){




                if(sitetype!= 'site'){//此处test为你的条件值
                    //
                    $("[data-field='ico']").css('display','none'); //关键代码
                    $("[data-field='logo']").css('display','none'); //关键代码

                }
                }





            });

            $('body').on('click', '.showdetail', function () {
               ea.open($(this).data('title'),$(this).data('url'),'80%','90%');
            })

            $('body').on('click', '[data-table-down]', function () {
                var tableId = $(this).attr('data-table-down');
                var sitetype = $(this).data('sitetype');

                var checkStatus = table.checkStatus(tableId),
                    url = $(this).attr('data-url'),
                    data = checkStatus.data;
                if (data.length <= 0) {
                    ea.msg.error('请选择要采集的数据');
                    return false;
                }
                var ids = [];
                $.each(data, function (i, v) {
                    ids.push(v.uniquekey);
                });
                layer.confirm('确定采集？', function () {
                    $item = $(this);
                    var disabled = $item.attr('data-disabled');
                    if (1 == disabled) {
                        return false;
                    }
                    $item.addClass("layui-disabled").attr('data-disabled', 1);
                    $item.find('span').html("正在发送请求...");
                    $.post(init.down_url, {uniquekeys: ids,sitetype:sitetype},
                        function (res) {
                            if (res.code) {
                                uniquekeys = res.uniquekeys;
                                var loading = layer.msg('正在下载(<font id="upgrade_backup_table">第' + (res.uniquekey.id+1) + '条，共'+res.uniquekey.count+'条</font>)……<font id="upgrade_backup_speed">0.01</font>%',{
                                    icon: 1,
                                    time: 3600000,//1小时后后自动关闭
                                    shade: [0.2]  //0.1透明度的白色背景
                                });

                                $item.find('span').html(res.msg);
                                backup(res.uniquekey);
                                window.onbeforeunload = function () {
                                    return "正在下载，请不要关闭！"
                                }
                            } else {
                                layer.msg(res.msg, {icon: 2, time: 1500});
                                $item.removeClass("layui-disabled").attr('data-disabled', 0);
                                $item.find('span').html("立即下载");
                            }
                        }, "json");
                    return false;

                    function backup(tab, status) {
                        status && showmsg(tab.id, "开始下载……(0%)");
                        $.post(init.down_url, tab, function (data) {
                            if (data.code) {
                                if (tab.uniquekey) {
                                    showmsg(tab.id, data.msg);
                                    $('#upgrade_backup_table').html('第' + (tab.id+1) + '条，共'+tab.count+'条');
                                    $('#upgrade_backup_speed').html(tab.speed);
                                    $item.find('span').html('正在下载……' + tab.speed + '%');
                                } else {
                                    $item.find('span').html('进入下载……');
                                }
                                if (!$.isPlainObject(data.uniquekey)) {

                                    $item.removeClass("layui-disabled").attr('data-disabled', 0);
                                    $item.find('span').html("开始下载");
                                    setTimeout(function () {
                                        layer.closeAll();
                                        ea.msg.success('全部下载完成！');
                                    }, 1000);
                                    window.onbeforeunload = function () {
                                        return null
                                    }
                                    return;
                                }
                                backup(data.uniquekey, tab.id != data.uniquekey.id);
                            } else {
                                layer.closeAll();
                                $item.removeClass("layui-disabled").attr('data-disabled', 0);
                                $item.find('span').html("立即下载");
                            }
                        }, "json");
                    }

                    function showmsg(id, msg) {
                        $("#" + uniquekeys[id]).html(msg);
                    }

                });
                return false;
            });
            ea.listen();
        },
    };
    return Controller;
});