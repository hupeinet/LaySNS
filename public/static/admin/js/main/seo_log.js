define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'main.seo_log/index',
        add_url: 'main.seo_log/add',
        edit_url: 'main.seo_log/edit',
        delete_url: 'main.seo_log/delete',
        modify_url: 'main.seo_log/modify'
    };

    var Controller = {

        index: function () {

            ea.table.render({
                init: init,
                toolbar: ['refresh'
                ],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},
                    {field: 'channel', width: 80, title: '通道'},
                    {field: 'batch_id', minWidth: 80, title: '批次号'},
                    {field: 'subject_title', title: '标题'},
                    {field: 'url', title: '链接'},
                    // {field: 'included', minWidth: 80, title: '是否收录', search: 'select'},
                    {field: 'create_time', minWidth: 80, title: '推送时间', search: 'range'},
                    {
                        width: 80,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'delete'
                        ]
                    }
                ]]
                // , done: function(res, curr, count){
                //     $("[data-field='url']").children().each(function () {
                //
                //         var url=$(this).text();
                //
                //         ea.request.get({
                //             url: 'main.seoLog/checkSL'+'?url='+url,
                //             prefix: true,
                //         }, function (res) {
                //             console.log(res);
                //             // alert()
                //             // ea.msg.success(res.msg, function () {
                //             //     window.location = ea.url('login/index');
                //             // })
                //         });
                //
                //         // $(this).parent('td').next('td').children('div').text('未收录')
                //
                //
                //         // if ($(this).text() == '0') {
                //         //     $(this).text('未收录');
                //         // } else if ($(this).text() == '1') {
                //         //     $(this).text('已收录');
                //         // }
                //     });
                //     // $(".layui-table-total div").each(function (i,item) {
                //     //     var div_text = $(item).html();
                //     //     if(div_text != ""&&div_text != '合计') {
                //     //         $(item).html(total);
                //     //     }
                //     // });
                //     // var urlArray = [];
                //     // $.each(res.data, function (index, val) {
                //     //     urlArray.push(val.url)
                //     // });
                //     // console.log(urlArray);
                // }
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        }
    };
    return Controller;
});