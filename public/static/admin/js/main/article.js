define(["jquery", "easy-admin", 'vue','xmSelect'], function ($, ea,Vue) {
    var form = layui.form;
    var table = layui.table;
    var laytpl = layui.laytpl;

    var element = layui.element;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'main.article/index',
        add_url: 'main.article/add',
        edit_url: 'main.article/edit',
        delete_url: 'main.article/delete',
        export_url: 'main.article/export',
        modify_url: 'main.article/modify'
    };

    var Controller = {

        index: function () {
            var category={};
            $.get({
                url: '/api/index/getCategory',
                async:false,
            },function (res) {
                category=res.data;
            });
            var userTable=ea.table.render({
                init: init,
                toolbar: ['refresh','add','delete','export',
                    [{
                        text: '修改分类',
                        url: 'main.article/changeCategory',
                        method: 'click',
                        auth: 'changeCategory',
                        class: 'layui-btn  layui-btn-sm',
                        icon: 'fa fa-hourglass',
                        extend: 'data-changecategory="' + init.table_render_id + '"',
                    }]
                ],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID',searchOp:'='},
                    {field: 'sort', width: 80, title: '排序', edit: 'text'},
                    {field: 'title', minWidth: 80, title: '标题'},
                    {field: 'category_id', minWidth: 120,title:'分类', templet: function(d){
                            return '<div id="XM-' + d.id + '"  data-value="'+d.category_id+'"></div>'
                        } },
                    {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'check_status', title: '审核状态', width: 85, search: 'select', selectList: {'-1':'无需审核',0: '未审核', 1: '已审核'}},
                    {field: 'view', minWidth: 80, title: '点击数'},

                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            'delete'
                        ]
                    }
                ]], done: function(res) {

                    var cells = document.querySelectorAll('div[lay-id="currentTable"] .layui-table-cell');
                    for (var i = 0; i < cells.length; i++) {
                        //cells[i].style.overflow = 'unset';
                        cells[i].style.height = 'auto';
                    }

                    res.data.forEach(item =>  {

                        var obj='#XM-' + item.id;
                        var xm = xmSelect.render({
                            el: obj,
                            radio:true,
                            initValue: [$(obj).data('value')],
                            clickClose: true,
                            on: function(data){

                                $.post("/admin/main.article/modify",{
                                    id:item.id,
                                    field:'category_id',
                                    value:data.arr[0].value
                                },function (res) {
                                    if(res.code==1){
                                        layer.tips('修改成功', obj);
                                    }
                                });

                            },

                            model: {
                                type: 'fixed',
                                label: {


                                    text: {
                                        //左边拼接的字符
                                        left: '',
                                        //右边拼接的字符
                                        right: '',
                                        //中间的分隔符
                                        separator: ', ',
                                    },
                                }
                            },
                            data: json2Select(category)
                        })

                        item.__xm = xm;
                    })
                }
            });
            document.querySelector('.layui-table-body').addEventListener('scroll', () => {
                xmSelect.get().forEach(function(item){
                    item.calcPosition();
                })
            })

            laytpl($("#temp").html()).render(data1.data, function (html) {


               $("#test11").html(html);

                element.init();
            });

            $('body').on('click', '[data-changecategory]', function () {
                var tableId = $(this).attr('data-changecategory'),
                    url = $(this).attr('data-url');
                tableId = tableId || init.table_render_id;
                url = url != undefined ? ea.url(url) : window.location.href;
                var checkStatus = table.checkStatus(tableId),
                    data = checkStatus.data;
                if (data.length <= 0) {
                    ea.msg.error('请勾选需要修改分类的数据');
                    return false;
                }
                var ids = [];
                $.each(data, function (i, v) {
                    ids.push(v.id);
                });

                layer.open({
                    title: $(this).data('title'),
                    type: 1,
                    shade: 0.5,
                    area: ["320px","400px"],
                    content:$("#test11").html(),
                    btn:['确定','取消'],
                    success:function(){
                        form.render('select');
                        form.on('select(aihao)', function(data){
                            $('#rolParameterText').val(data.value);
                        });
                    },
                    yes: function (index,layero){

                        ea.request.post({
                                    url: url,
                                    data: {
                                        id: ids,
                                        category_id:$('#rolParameterText').val()
                                    },
                                }, function (res) {
                                    ea.msg.success(res.msg, function () {
                                       layer.close(index);
                                        userTable.reload();
                                    });
                                });

                    },cancel: function (layero, index){

                        $('#rolParameterText').val('');



                    },btn2: function (layero, index){

                        $('#rolParameterText').val('');

                    }
                });
                return false;
            });
            ea.listen();

        },add: function () {
            ea.request.get({
                url: '../api/index/getTagSelect',
                prefix: true,
            }, function (res) {
                if(res.code==1){

                    var json=res.data;

                    var xmSelects = document.querySelectorAll("[data-xmSelect]");
                    if (xmSelects.length > 0) {

                        $.each(xmSelects, function (i, v) {

                            var json2=json2Select(json);
                            if($(this).data('selectjson')){
                                json2=json2Select($(this).data('selectjson'));
                            }


                            var xm=xmSelect.render({
                                el:this,
                                name:$(this).attr('data-xmSelect'),
                                data:json2,
                                // radio:radio,
                                filterable: true,
                                create: function(val, arr){
                                    //返回一个创建成功的对象, val是搜索的数据, arr是搜索后的当前页面数据
                                    return {
                                        name: val,
                                        value: val
                                    }
                                }
                            });


                        });
                    }
                }
            })

            ea.listen();
        },
        edit: function () {

            ea.request.get({
                url: '../api/index/getTagSelect',
                prefix: true,
            }, function (res) {
                if(res.code==1){

                    var json=res.data;

                    var xmSelects = document.querySelectorAll("[data-xmSelect]");
                    if (xmSelects.length > 0) {

                        $.each(xmSelects, function (i, v) {
                            var arr=[];
                            var str=$(this).data('value');
                            if(str!=''){
                                arr=str.toString().split(",");
                                arr.forEach(function(k) {
                                    json.push({
                                        'id':k,
                                        'title':k
                                    });
                                })
                            }
                            var json2=json2Select(json);
                            if($(this).data('selectjson')){
                                json2=json2Select($(this).data('selectjson'));
                            }
                            var selecttype=$(this).attr('data-selecttype');
                            var radio=false;
                            if(selecttype=='radio'){
                                radio=true;
                            }
                            var xm=xmSelect.render({
                                el:this,
                                name:$(this).attr('data-xmSelect'),
                                data:json2,
                                // radio:radio,
                                filterable: true,
                                create: function(val, arr){
                                    //返回一个创建成功的对象, val是搜索的数据, arr是搜索后的当前页面数据
                                    return {
                                        name: val,
                                        value: val
                                    }
                                }
                            });
                            if(arr.length !==0 ){
                                xm.setValue(arr);
                            }

                        });
                    }
                }
            })



            ea.listen();
        },
    };

    return Controller;
});