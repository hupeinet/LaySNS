define(["jquery", "easy-admin",'xmSelect'], function ($, ea) {

    var element = layui.element,
    table = layui.table,
    laytpl = layui.laytpl;
    var param=window.location.search.substring(1);


    var Controller = {

        index: function () {
            var init={
                table_elem: '#currentTable',
                table_render_id: 'currentTableRenderId',
                index_url: 'main.collectCategory/index',
                delete_url: 'main.collectCategory/delete',
                export_url: 'main.collectCategory/export',
                modify_url: 'main.collectCategory/modify',
            };
            var category={};
            $.get({
                url: '/api/index/getCategory',
                async:false,
            },function (res) {
                category=res.data;
           });

            ea.table.render({
                init: init,
                toolbar: ['refresh',
                    'delete', 'export'],

                limit: 20,
                cols: [[
                    {type: "checkbox"},

                    {field: 'title', width: 280, title: '采集的栏目'},
                    {field: 'subject', width: 280, title: '类型'},
                    {field: 'category_id', minWidth: 120,title:'分类', templet: function(d){
                            return '<div id="XM-' + d.id + '"  data-value="'+d.category_id+'"></div>'
                        } },
                    {field: 'create_time', minWidth: 80, title: '采集时间', search: 'range'},
                    {
                        width: 100,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'delete'
                        ]
                    }
                ]],
                done: function(res) {
                    //修改一些css样式, 这里虽然能够使用, 但是还是不太友好, 努力中...
                    var cells = document.querySelectorAll('div[lay-id="currentTable"] .layui-table-cell');
                    for (var i = 0; i < cells.length; i++) {
                        //cells[i].style.overflow = 'unset';
                        cells[i].style.height = 'auto';
                    }

                    res.data.forEach(item =>  {

                        var obj='#XM-' + item.id;
                        var xm = xmSelect.render({
                            el: obj,
                            radio:true,
                            initValue: [$(obj).data('value')],
                            clickClose: true,
                            on: function(data){

                                $.post("/admin/main.collectCategory/modify",{
                                    id:item.id,
                                    field:'category_id',
                                    value:data.arr[0].value
                                },function (res) {
                                    if(res.code==1){
                                        layer.tips('修改成功', obj);
                                    }
                                });

                            },
                            model: {
                                type: 'fixed',
                                label: {
                                    type: 'fixed',
                                    text: {
                                        //左边拼接的字符
                                        left: '',
                                        //右边拼接的字符
                                        right: '',
                                        //中间的分隔符
                                        separator: ', ',
                                    },
                                }
                            },
                            data: json2Select(category)
                        })

                        item.__xm = xm;
                    })
                }
            });
            //监听单元格编辑
            ea.table.listenEdit(init, 'currentTable', init.table_render_id, true);
            ea.listen();
        }
    };
    return Controller;
});