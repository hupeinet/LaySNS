define(["jquery", "easy-admin", 'vue','xmSelect'], function ($, ea,Vue) {
    var form = layui.form;
    var table = layui.table;
    var laytpl = layui.laytpl;
    var element = layui.element;

    var Controller = {

       article: function () {

            ea.request.get({
                url: '../api/index/getCategory',
                prefix: true,
            }, function (res) {
                   if(res.code==1){
                   var json=json2Select(res.data);
                   var xmSelects = document.querySelectorAll("[data-xmSelect]");
                   if (xmSelects.length > 0) {

                       $.each(xmSelects, function (i, v) {
                            var arr=[];
                            var str=$(this).data('value');
                            if(str!=''){
                                arr=str.toString().split(",");
                           }
                           var xm=xmSelect.render({
                               el:this,
                               autoRow: true,
                               name:$(this).attr('data-xmSelect'),
                               data:json
                           });
                            if(arr.length !==0 ){
                                xm.setValue(arr);
                            }

                       });
                   }
               }
            });
            ea.listen();
        },comment: function () {
            ea.listen();
        },seo: function () {
            ea.listen();
        },friendly_link: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };

    return Controller;
});