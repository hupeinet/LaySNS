define(["jquery", "easy-admin"], function ($, ea) {

    let table = layui.table;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'addon.plugin/index',
    };

    var Controller = {
        index: function () {
            var util = layui.util;

            var pluginTable = ea.table.render({
                init: init,
                toolbar: ['refresh'],
                page: false,
                limit: 'all',

                cols: [[
                    {field: 'title', width: 180, title: '插件名称', search: false},
                    {field: 'name', width: 80, title: '标识', search: false},
                    {field: 'description', minWidth: 280, title: '描述', search: 'range'},
                    {field: 'status', minWidth: 80, title: '状态', search: 'range'},
                    {field: 'author', minWidth: 80, title: '作者', search: 'range'},
                    {field: 'version', minWidth: 80, title: '版本', search: false},
                    {field: 'statusCn', minWidth: 80, title: '状态', search: 'range'},
                    {field: 'button', minWidth: 210, title: '操作', search: false},
                ]],
            });
            table.on("tool(currentTableRenderId_LayFilter)", function (obj) {
                let data = obj.data;
                addon_deal(obj.event, data.name);
            });

            //安装、卸载、配置、设置状态、下载插件总入口
            function addon_deal(action, name) {
                var objTitle = '安装';
                var confirmOpen = 1;
                switch (action) {
                    case "install":
                        break;
                    case "uninstall":
                        objTitle = '卸载吗？卸载之后数据不会丢但是重装会清空，确定';
                        break;
                    case "config":
                        objTitle = '配置';
                        confirmOpen = 2;
                        break;
                    case "state":
                        objTitle = '改变状态';
                        confirmOpen = 0;
                        break;
                    case "download":
                        objTitle = '下载';
                        break;
                    case "delete":
                        objTitle = '删除';
                        break;
                    case "update":
                        objTitle = '更新';
                        break;
                }
                if (confirmOpen == 2) {

                    // var index = layer.open({
                    //     title: objTitle,
                    //     type: 2,
                    //     area: [width, height],
                    //     content: action+"?name="+name,
                    //     maxmin: true,
                    //     moveOut: true,
                    //     success: function (layero, index) {
                    //
                    //     }
                    // });

                    ea.open("配置", action + "?name=" + name, "90%", "90%", function (layero) {
                        //let iframeWin = window[layero.find("iframe")[0]["name"]];
                        //iframeWin.initForm(data);
                    }, function () {
                        // pluginTable.reload();
                        // userTablebd.reload();
                    })
                } else {
                    layer.confirm("确定要" + objTitle + "吗？", function () {
                        ea.request.get({
                            url: action,
                            data: {
                                name: name
                            },
                        }, function (res) {
                            if (res.code == 1) {
                                ea.msg.success(res.msg, function () {
                                    pluginTable.reload();
                                });
                            } else {
                                ea.alert(res.msg);
                            }
                        });
                    })
                }
            }

            ea.listen();
        },
        online: function () {
            ea.table.render({

                init: {
                    table_elem: '#currentTable',
                    table_render_id: 'currentTableRenderId',
                    index_url: 'addon.plugin/online',
                    down_url: 'addon.plugin/download',
                    delete_url: 'main.comment/delete',
                },
                toolbar: ['refresh'],

                limit: 20,
                cols: [[
                    {field: 'title', width: 180, title: '插件名称', search: false},
                    {field: 'name', width: 80, title: '标识', search: false},
                    {field: 'description', minWidth: 280, title: '描述'},
                    {field: 'author', minWidth: 80, title: '作者', search: false},
                    {field: 'version', minWidth: 80, title: '版本', search: false},
                    {
                        width: 250,
                        title: '操作',
                        align: 'center',
                        templet: function (d) {
                            var tit = d.is_down == 2 ? '升级' : (d.is_down == 1 ? '已下载' : '下载');
                            var disabled = d.is_down == 1 ? 'layui-btn-disabled' : '';
                            var down = d.is_down == 2 ? 'update' : (d.is_down == 1 ? '' : 'download');
                            var installBtn=d.version!='未知'?('<a class="layui-btn ' + disabled + '  layui-btn-xs" href="javascript:void(0)" data-name="'+d.name+'" data-ver="' + d.ver_id + '"  lay-event="' + down + '">' + tit + '</a>'):''

                            return '<a class="layui-btn layui-btn-primary layui-btn-xs" target="_blank" href="' + d.url + '">详情</a>' +installBtn;
                        }
                    }
                ]],
            });
            table.on("tool(currentTableRenderId_LayFilter)", function (obj) {
                let data = obj.data;
                var verId = $(this).data('ver');
                var name = $(this).data('name');

                if (obj.event) {
                    ea.request.get({
                        url: 'addon.plugin/download',
                        prefix: true,
                        data: {
                            type:'addons',
                            name:name,
                            ver_id:verId
                        },
                    }, function (res) {
                        if (res.code == 1) {
                            ea.msg.success(res.msg, function () {
                                location.reload();
                            });
                        } else {
                            ea.alert(res.msg);
                        }
                    });
                }
            });
            ea.listen();
        }
    };

    return Controller;
});