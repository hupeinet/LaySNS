define(["jquery", "easy-admin"], function ($, ea) {

    let laytpl = layui.laytpl;
    var Controller = {
        index: function () {

            $('body').on('click', '.action', function () {
                var name=$(this).data('name');
                var action=$(this).data('action');
                var controller=$(this).data('controller');
                layer.confirm('确定'+(action=='setUse'?'启用':'删除')+'此模板？', function () {
                    ea.request.get({
                        url: 'addon.template/'+action,
                        prefix: true,
                        data: {
                            type: 'template',
                            name: name,
                            controller: controller
                        },
                    }, function (res) {
                        if (res.code == 1) {
                            ea.msg.success(res.msg, function () {
                                location.reload();
                            });
                        } else {
                            layer.alert(res.msg);
                        }
                    });
                })
            })


            ea.listen();
        },
        online: function () {

            ea.request.get({
                url: 'addon.template/online',
                prefix: true
            }, function (res) {

                if (res.code == 1) {

                    laytpl($("#temp").html()).render(res.data, function (html) {
                        $("#themeList").html(html);
                    })
                }
            });

            $('body').on('click', '.download', function () {
                var name=$(this).data('name');

                var verId=$(this).data('ver');
                layer.confirm('下载在线模板会覆盖本地的同名模板，请自己提前备份？', function () {
                    ea.request.get({
                        url: 'addon.plugin/download',
                        prefix: true,
                        data: {
                            type: 'template',
                            name: name,
                            ver_id: verId
                        },
                    }, function (res) {
                        if (res.code == 1) {
                            ea.msg.success(res.msg, function () {
                                location.reload();
                            });
                        } else {
                            layer.alert(res.msg);
                        }
                    });
                })
            })
        }
    };

    return Controller;
});