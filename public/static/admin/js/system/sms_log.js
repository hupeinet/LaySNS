define(["jquery", "easy-admin"], function ($, ea) {


    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.smslog/index',
    };

    var Controller = {
        index: function () {
            var util = layui.util;
            ea.table.render({
                init: init,
                toolbar: ['refresh'],
                cols: [[
                    {field: 'id', width: 80, title: 'ID', search: false},
                    {field: 'type', width: 80, title: '邮件/短信', search: false},
                    {field: 'model', width: 80, title: '发送通道'},
                    {field: 'object', width: 100, title: '接收账号'},
                    {field: 'content', minWidth: 150, title: '操作内容'},
                    {field: 'ip', width: 120, title: 'IP地址'},
                    {field: 'back_msg', minWidth: 80, title: '发送结果'},
                    {field: 'create_time', minWidth: 80, title: '发送时间', search: 'range'},
                ]],
            });

            ea.listen();
        },
    };

    return Controller;
});