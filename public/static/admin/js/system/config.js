define(["jquery", "easy-admin", 'vue','xmSelect'], function ($, ea,Vue) {
    var form = layui.form;
    var colorpicker  = layui.colorpicker;

    var Controller = {
        index: function () {

            var app = new Vue({
                el: '#app'});

            function setEnv(name,value){
                ea.request.post({
                    url: 'system.config/setEnv',
                    prefix: true,
                    data: {
                        field:name,
                        value:value
                    },
                }, function (res) {
                    if(name=='key'){
                        $('#setKey').attr('data-key',value);
                    }
                    layer.msg(res.msg,function() {

                        layer.closeAll();
                    });

                });
            }

            form.on('switch(setEnv)', function(data){

                    setEnv(data.value,data.elem.checked)

            })

            $('body').on('click', '#setKey', function () {

                layer.prompt({area: ['400px', '200px'],btn: ['修改', '去官网查看','取消'], title: 'updateKey设置',value:$(this).attr('data-key'), btn2: function(index, elem){
                    // 得到输入的数据，这里采用jquery，当然可以更换成layui内的jquery
                    // var value = $('#layui-layer'+index + " .layui-layer-input").val();
                    // alert(value);
                    // if (!value) {
                    //     return false;　　// 如果value为空，不进入下一步
                    // }
                    // layer.close(index);
                        window.open("https://bbs.laysns.com/apps/developer/myupkey.html")
                },
                btnAlign: 'c',    // 设置按钮位置
            }, function(value, index) {
                    setEnv('key', value)
                });
            });

            ea.listen();


        },mail: function () {



            ea.listen();

        },sms: function () {



            ea.listen();

        },juhe: function () {



            ea.listen();

        },switch: function () {

            ea.listen();

        },upload: function () {

            var app = new Vue({
                el: '#app',
                data: {
                    upload_type: upload_type
                }
            });

            form.on("radio(upload_type)", function (data) {
                app.upload_type = this.value;
            });
            ea.listen();
        }
    };

    return Controller;
});