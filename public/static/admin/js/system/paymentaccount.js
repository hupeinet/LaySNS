define(["jquery", "easy-admin"], function ($, ea) {
    var param=window.location.search.substring(1);
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.paymentaccount/index?'+param,
        add_url: 'system.paymentaccount/add?'+param,
        edit_url: 'system.paymentaccount/edit',
        delete_url: 'system.paymentaccount/delete',
        modify_url: 'system.paymentaccount/modify',
        testpay_url: 'system.paymentaccount/testpay'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                limit:100,
                page: false,
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},

                    {field: 'title', minWidth: 80, title: '标题'},
                    {field: 'pay_types', minWidth: 80, title: '支付方式',templet:function (d) {
                        var str='';
                        console.log(d.pay_types);
                            $.each(d.pay_types,function(i,item){
// 这样每个item是一个json对象，
                               str +=item+' ';
                            })

                            return str;
                        }
                    },
                    {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            [{
                                text: '测试支付',
                                url: init.testpay_url,
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                                extend: 'data-full="true"',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });
            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        }
    };
    return Controller;
});