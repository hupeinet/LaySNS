define(["jquery", "easy-admin"], function ($, ea) {

    var param=window.location.search.substring(1);
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.crontab/index?'+param,
        add_url: 'system.crontab/add?'+param,
        edit_url: 'system.crontab/edit',
        modify_url: 'system.crontab/modify',
        delete_url: 'system.crontab/delete',
        import_to_bt: 'system.crontab/import_to_bt'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                limit:100,
                page: false,
                toolbar: ['refresh',
                    // 'add',
                    [{
                        text: '导入宝塔',
                        url: init.import_to_bt,
                        method: 'request',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus '
                    }],
                    'delete'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},
                    {field: 'title', minWidth: 80, title: '任务名称',edit:'text'},
                    // {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'cycle', minWidth: 80, title: '周期'},
                    {field: 'is_add', minWidth: 80, title: '导入状态',edit:'text'},
                    {field: 'job_url', minWidth: 80, title: '定时任务链接'},


                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            // 'edit',
                            // [{
                            //     text: '导入宝塔',
                            //     url: init.import_to_bt,
                            //     class: 'layui-btn layui-btn-xs layui-btn-normal',
                            //     extend: 'data-full="true"',
                            // }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        config: function () {
            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        }
    };
    return Controller;
});