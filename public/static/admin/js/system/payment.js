define(["jquery", "easy-admin"], function ($, ea) {

    var form=layui.form;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.payment/index',
        add_url: 'system.payment/add',
        edit_url: 'system.payment/edit',
        delete_url: 'system.payment/delete',
        modify_url: 'system.payment/modify',
        account_url: 'system.paymentaccount/index',

    };

    var Controller = {

        index: function () {

            ea.table.render({
                init:init,
                limit:100,
                page: false,
                toolbar: ['refresh',
                    [{
                        text: '添加',
                        url: init.add_url,
                        method: 'open',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                        extend: 'data-full="true"',
                    }],
                    'delete'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},

                    {field: 'title', minWidth: 80, title: '标题'},
                    {field: 'alias', minWidth: 80, title: '别称'},
                    {field: 'shou_fu', minWidth: 80, title: '收付类型'},



                    {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            [{
                                text: '账号列表',
                                url: init.account_url,
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                                extend: 'data-full="true"',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            form.verify({
                otherReq: function(value,item){
                    var $ = layui.jquery;
                    var verifyName=$(item).attr('lay-filter')
                        , verifyType=$(item).attr('type')
                        ,formElem=$(item).parents('.layui-form')//获取当前所在的form元素，如果存在的话
                        ,verifyElem=formElem.find('input[lay-filter='+verifyName+']')//获取需要校验的元素
                        ,isTrue= verifyElem.is(':checked')//是否命中校验
                        ,focusElem = verifyElem.next().find('i.layui-icon');//焦点元素
                    if(!isTrue || !value){
                        //定位焦点
                        focusElem.css(verifyType=='radio'?{"color":"#FF5722"}:{"border-color":"#FF5722"});
                        //对非输入框设置焦点
                        focusElem.first().attr("tabIndex","1").css("outline","0").blur(function() {
                            focusElem.css(verifyType=='radio'?{"color":""}:{"border-color":""});
                        }).focus();
                        return '必填项不能为空';
                    }
                }
            });
            ea.listen();
        },
        edit: function () {
            ea.listen();
        }
    };
    return Controller;
});