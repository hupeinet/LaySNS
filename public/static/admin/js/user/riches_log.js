define(["jquery", "easy-admin", 'vue','xmSelect'], function ($, ea,Vue) {
    var form = layui.form;
    var table = layui.table;
    var laytpl = layui.laytpl;

    var element = layui.element;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'user.RichesLog/index',
        add_url: 'user.RichesLog/add',
        edit_url: 'user.RichesLog/edit',
        delete_url: 'user.RichesLog/delete',
        password_url: 'user.RichesLog/changePassword',
        score_url: 'user.RichesLog/changeScore',
        export_url: 'user.RichesLog/export',
        modify_url: 'user.RichesLog/modify'
    };

    var Controller = {

        index: function () {

            var userTable=ea.table.render({
                init: init,
                toolbar: ['refresh'
                ],
                cols: [[
                    {type: "checkbox"},

                    {field: 'username', minWidth: 80, title: '用户名'},
                    {field: 'nickname', minWidth: 80, title: '昵称'},

                    {field: 'riches', minWidth: 80, title: '变化金额'},

                    {field: 'is_add', title: '增减', width: 100, search: 'select', selectList: {0: '减', 1: '增'}},
                    {field: 'content', minWidth: 80, title: '变动原因'},
                    {field: 'balance', minWidth: 80, title: '变化后余额'},
                   {field: 'create_time', minWidth: 80, title: '变动时间', search: 'range'},

                ]],
            });



            $('body').on('click', '[data-changecategory]', function () {
                var url = $(this).attr('data-url');
                layer.prompt({title: '输入用户数，2<=X<=20',value:8}, function(num, index){
                    layer.prompt({title: '输入统一的密码，6位以上数字或字母'}, function(password, index){
                        // layer.close(index);
                        // layer.msg('您的数字：'+ num +'<br>您最后写下了：'+password);
                        ea.request.post( {url:url, data:{num:num,password:password}},function(res) {
                            if (res.code == 1) {
                                layer.msg(res.msg, {
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    userTable.reload();
                                    layer.closeAll();
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 2,
                                    anim: 6,
                                    time: 1000
                                });
                            }
                        });
                    });
                });

                return false;
            });
            ea.listen();

        },add: function () {


            ea.listen();
        },
        edit: function () {

            ea.request.get({
                url: '../api/index/getTagSelect',
                prefix: true,
            }, function (res) {
                if(res.code==1){

                    var json=res.data;

                    var xmSelects = document.querySelectorAll("[data-xmSelect]");
                    if (xmSelects.length > 0) {

                        $.each(xmSelects, function (i, v) {
                            var arr=[];
                            var str=$(this).data('value');
                            if(str!=''){
                                arr=str.toString().split(",");
                                arr.forEach(function(k) {
                                    json.push({
                                        'id':k,
                                        'title':k
                                    });
                                })
                            }

                            var xm=xmSelect.render({
                                el:this,
                                name:$(this).attr('data-xmSelect'),
                                data:json2Select(json),
                                filterable: true,
                                create: function(val, arr){
                                    //返回一个创建成功的对象, val是搜索的数据, arr是搜索后的当前页面数据
                                    return {
                                        name: val,
                                        value: val
                                    }
                                }
                            });
                            if(arr.length !==0 ){
                                xm.setValue(arr);
                            }

                        });
                    }
                }
            })



            ea.listen();
        },
    };

    return Controller;
});