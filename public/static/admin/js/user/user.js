define(["jquery", "easy-admin", 'vue','xmSelect'], function ($, ea,Vue) {
    var form = layui.form;
    var table = layui.table;
    var laytpl = layui.laytpl;

    var element = layui.element;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'user.user/index',
        add_url: 'user.user/add',
        edit_url: 'user.user/edit',
        delete_url: 'user.user/delete',
        password_url: 'user.user/changePassword',
        score_url: 'user.user/changeScore',
        export_url: 'user.user/export',
        modify_url: 'user.user/modify'
    };

    var Controller = {

        index: function () {

            var userTable=ea.table.render({
                init: init,
                toolbar: ['refresh','add','delete', [{
                    text: '批量添加用户',
                    url: '/admin/user.user/batchAddUser',
                    method: 'click',
                    auth: 'changeCategory',
                    class: 'layui-btn  layui-btn-sm',
                    icon: 'fa fa-plus',
                    extend: 'data-changecategory="' + init.table_render_id + '"',
                }]
                ],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID'},
                    {field: 'avatar', minWidth: 80, title: '头像', search: false, templet: ea.table.image},
                    {field: 'username', minWidth: 80, title: '用户名'},
                    {field: 'nickname', minWidth: 80, title: '昵称'},
                    {field: 'score', minWidth: 80, title: '积分'},
                    {field: 'is_admin', title: '设为管理', width: 100, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'no_audit', title: '发文免审', width: 100, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            [{
                                text: '重置密码',
                                url: init.password_url,
                                method: 'prompt',
                                auth: 'password',
                                extend: 'data-table-id="' + init.table_render_id + '"',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            },{
                                text: '修改积分',
                                url: init.score_url,
                                method: 'prompt',
                                auth: 'password',
                                extend: 'data-table-id="' + init.table_render_id + '"',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });



            $('body').on('click', '[data-changecategory]', function () {
                var url = $(this).attr('data-url');
                layer.prompt({title: '输入用户数，2<=X<=20',value:8}, function(num, index){
                    layer.prompt({title: '输入统一的密码，6位以上数字或字母'}, function(password, index){
                        // layer.close(index);
                        // layer.msg('您的数字：'+ num +'<br>您最后写下了：'+password);
                        ea.request.post( {url:url, data:{num:num,password:password}},function(res) {
                            if (res.code == 1) {
                                layer.msg(res.msg, {
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    userTable.reload();
                                    layer.closeAll();
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 2,
                                    anim: 6,
                                    time: 1000
                                });
                            }
                        });
                    });
                });

                return false;
            });
            ea.listen();

        },add: function () {


            ea.listen();
        },
        edit: function () {

            ea.request.get({
                url: '../api/index/getTagSelect',
                prefix: true,
            }, function (res) {
                if(res.code==1){

                    var json=res.data;

                    var xmSelects = document.querySelectorAll("[data-xmSelect]");
                    if (xmSelects.length > 0) {

                        $.each(xmSelects, function (i, v) {
                            var arr=[];
                            var str=$(this).data('value');
                            if(str!=''){
                                arr=str.toString().split(",");
                                arr.forEach(function(k) {
                                    json.push({
                                        'id':k,
                                        'title':k
                                    });
                                })
                            }

                            var xm=xmSelect.render({
                                el:this,
                                name:$(this).attr('data-xmSelect'),
                                data:json2Select(json),
                                filterable: true,
                                create: function(val, arr){
                                    //返回一个创建成功的对象, val是搜索的数据, arr是搜索后的当前页面数据
                                    return {
                                        name: val,
                                        value: val
                                    }
                                }
                            });
                            if(arr.length !==0 ){
                                xm.setValue(arr);
                            }

                        });
                    }
                }
            })



            ea.listen();
        },
    };

    return Controller;
});