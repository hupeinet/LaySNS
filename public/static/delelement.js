layui.use(['layer', 'jquery','table','form'], function() {
    var layer = layui.layer,
        table = layui.table,
        $ = layui.jquery,
        url=window.location.href;
    var index = url.lastIndexOf("\/");
    var postUrl = url.substring(0,index+1);

    $('body').on('click', '[data-table-add]', function () {
        layer.open({
            title: '新增',
            type: 2,
            maxmin: true,
            shade: 0.5,
            area: ["100%","100%"],
            content:postUrl+'add.html'+window.location.search,
        });
    });
    $('body').on('click', '[data-table-delete]', function () {
        var checkStatus = table.checkStatus('tableId'),
            data = checkStatus.data;
        if (data.length <= 0) {
            layer.msg('请选择要删除的数据');
            return false;
        }
        var ids = [];
        $.each(data, function (i, v) {
            ids.push(v.id);
        });
        layer.confirm('确定删除？', function () {
            $.post({
                url: postUrl+'delete',
                data: {
                    id: ids
                },
            }, function (res) {

                    if (res.code == 1) {

                            location.reload();

                    } else {
                        layer.alert(res.msg);
                    }

            });
        });
        return false;
    });


    table.on("tool(tableFilter)", function (obj) {
        let data = obj.data;
        var id=data.id;
        switch (obj.event) {
            case "edit":
                layer.open({
                    title: '修改',
                    type: 2,
                    maxmin: true,
                    shade: 0.5,
                    area: ["100%","100%"],
                    content:postUrl+'edit?id='+id,
                });
                break;
            case "del":
                layer.confirm('确定删除？', function () {
                    $.post(postUrl + 'delete?id=' + id, {}, function (res) {
                        if (res.code == 1) {
                            location.reload();
                        } else {
                            layer.alert(res.msg);
                        }
                    });
                })
                break;
        }
    });
});