<?php
// [ 应用入口文件 ]
namespace think;

define('ADS_PATH', '../addons/');

require __DIR__ . '/../vendor/autoload.php';

// 声明全局变量
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', __DIR__ . DS . '..' . DS);
define('EXTEND_PATH', ROOT_PATH . 'extend' . DS);
define('CUR_DATE', date('Y-m-d'));
define('CUR_DATETIME', date('Y-m-d H:i:s'));
define('YEWU_FILE',ROOT_PATH.'/data/log/'.CUR_DATE.'.log');
if (!defined('WEB_ROOT')) {
    $url = rtrim(dirname(rtrim($_SERVER['SCRIPT_NAME'], '/')), '/');
    define('WEB_ROOT', (('/' == $url || '\\' == $url) ? '' : $url));
}
define('DOMAIN_URL', (!isset($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'].WEB_ROOT);

if(!is_file(__DIR__ . '/../data/install.lock')){
    $installUrl=domain_url('/install/index.php');
    header('Location:'.$installUrl);
    die;
}
// 执行HTTP应用并响应
$http = (new App())->http;
$response = $http->run();

$response->send();
$http->end($response);
