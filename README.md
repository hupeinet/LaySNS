
![LAYSNS-logo](public/static/common/images/logo.png)


[![Php Version](https://img.shields.io/badge/php-%3E=7.1.0-brightgreen.svg?maxAge=2592000&color=yellow)](https://github.com/php/php-src)
[![Mysql Version](https://img.shields.io/badge/mysql-%3E=5.7-brightgreen.svg?maxAge=2592000&color=orange)](https://www.mysql.com/)
[![Thinkphp Version](https://img.shields.io/badge/thinkphp-%3E=6.0.2-brightgreen.svg?maxAge=2592000)](https://github.com/top-think/framework)
[![Layui Version](https://img.shields.io/badge/layui-=2.5.5-brightgreen.svg?maxAge=2592000&color=critical)](https://github.com/sentsin/layui)
[![Layuimini Version](https://img.shields.io/badge/layuimini-%3E=2.0.4.2-brightgreen.svg?maxAge=2592000&color=ff69b4)](https://github.com/zhongshaofa/layuimini)

## 项目介绍

基于ThinkPHP6.0和layui的快速开发的后台管理系统。

技术交流QQ群：[227552979](https://jq.qq.com/?_wv=1027&k=JNFGmxQn) `加群请备注来源：如gitee、github、官网等`。

## 站点地址

* 官方网站：[http://www.laysns.com](http://www.laysns.com)

* 演示地址：[http://sns.laysns.com](http://sns.laysns.com)
  演示后台：[http://sns.laysns.com/admin](http://sns.laysns.com/admin)
## 代码仓库

* Gitee地址：[https://gitee.com/yunyang0726/LaySNS](https://gitee.com/yunyang0726/LaySNS)


## 项目特性
0、内核升级至ThinkPHP6.0.15（与官网同步）,后台UI采用EasyAdmin最新版。所有代码全部重写，数据库结构全部改写优化！

1、支持内容采集，官方服务器整体采集，用户端指定分类，定制站点定向拉取（自动手动皆可）

2、支持文章内容定时发布，无需插件，简单贴心功能（定时任务贴心实现）

3、批量图片本地化（定时任务贴心实现）、图片水印、封面图片自动压缩

4、集成社交账号登录：支持QQ登录、微信扫码登录、公众号登录、钉钉登录、

5、回复下载、付费下载，简单高效、安全实用、无需额外繁琐配置

6、集成支付宝当面付、微信支付、易支付、码支付，支持第三方支付接口接入

7、标签云（智能化算法，无需站长干预，用户真实点击）

8、高级广告管家插件，智能化管控图片广告、文字广告，到期自动下线

9、集成SEO推送，精心优化全部页面细节，更利于SEO收录

10、全方位防灌水：你看不到的地方，黑客或者别有用心者最喜欢尝试多输入口进行狙击，我们有全方位细节防护！

11、全网首创的附件链接插件已全新升级、简洁优美的UI设计，下载方案优化设计。已支持蓝奏云盘直连解析。

12、全新升级高级广告管家插件：拖拽布局广告位中的图片，支持自定义文字、图片广告列数。

## 特别感谢

以下项目排名不分先后

* ThinkPHP：[https://github.com/top-think/framework](https://github.com/top-think/framework)

* Layuimini：[https://github.com/zhongshaofa/layuimini](https://github.com/zhongshaofa/layuimini)

* EasyAdmin：[https://github.com/zhongshaofa/easyadmin](https://github.com/zhongshaofa/easyadmin)

* Annotations：[https://github.com/doctrine/annotations](https://github.com/doctrine/annotations)

* Layui：[https://github.com/sentsin/layui](https://github.com/sentsin/layui)

* Jquery：[https://github.com/jquery/jquery](https://github.com/jquery/jquery)

* RequireJs：[https://github.com/requirejs/requirejs](https://github.com/requirejs/requirejs)

* WangEditor：[https://github.com/wangeditor-team/wangEditor](https://github.com/wangeditor-team/wangEditor)

* Echarts：[https://github.com/apache/incubator-echarts](https://github.com/apache/incubator-echarts)
  
 ## 捐赠支持
 
开源项目不易，若此项目能得到你的青睐，可以购买商业版本，商业版本支持任意定制，感谢所有支持开源的朋友。

 [点击购买商业授权](https://bbs.laysns.com/apps/auth/index.html)
