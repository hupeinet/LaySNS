<?php
namespace app\admin\controller\system;

use app\admin\service\TriggerService;
use app\common\controller\AdminController;
use app\common\model\Article;
use app\common\model\ArticleCategory;
use app\common\model\Nav  as Model;
use think\App;

/**
 * @ControllerAnnotation(title="前台导航管理")
 * Class Nav
 * @package app\admin\controller\system
 */
class Nav extends AdminController
{

    use \app\admin\traits\Curd;
    protected $allowModifyFields = [
        'status',
        'sort',
        'title',
        'is_nav'
    ];
    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model=new Model();
        $position=1;
        $params=request()->param();
        if(isset($params['position'])){
            $position=$params['position'];
        }
        $pidCateList=Model::getPidCateList($position);
        $this->assign('pidCateList', $pidCateList);
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index($position=1)
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            $count = $this->model->where('position',$position)->count();
            $list = $this->model->where('position',$position)->order($this->sort)->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        $this->assign('position', $position);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add($id = null)
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $rule = [
                'pid|上级栏目'   => 'require',
                'title|栏目名称' => 'require',
                'icon|栏目图标'  => 'require',
            ];
            $this->validate($post, $rule);
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            if ($save) {
                TriggerService::updateMenu();
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }

        $this->assign('id', $id);

        return $this->fetch();
    }


    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {

        $row = $this->model->whereIn('id', $id)->select();
        $row->isEmpty() && $this->error('数据不存在');
        try {
            $navIds=[];
            foreach ($row as $v){
                if($v['category_id']){
                    array_push($navIds,$v['category_id']);
                }
            }
            if(!empty($navIds)) {
                (new ArticleCategory())->whereIn('id', $navIds)->save(['is_nav'=>0]);
            }
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }


}