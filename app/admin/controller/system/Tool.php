<?php

namespace app\admin\controller\system;


use app\admin\model\SystemLog;
use app\common\controller\AdminController;

use think\App;

/**
 * @ControllerAnnotation(title="工具素材管理")
 * Class Tool
 * @package app\admin\controller\system
 */
class Tool extends AdminController
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new SystemLog();
    }

    /**
     * @NodeAnotation(title="FA字体图标")
     */
    public function font_icon()
    {
        return $this->fetch();
    }

}