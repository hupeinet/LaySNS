<?php

namespace app\admin\controller\system;


use app\admin\service\TriggerService;
use app\common\constants\MenuConstant;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\common\controller\AdminController;
use app\common\model\PaymentAccount as Model;
use app\common\model\Payment;
use think\App;

/**
 * Class Paymentaccount
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="支付账号管理",auth=true)
 */
class Paymentaccount extends AdminController
{

    protected $alias;
    protected $payment;
    use \app\admin\traits\Curd;
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Model();
    }

    public function index($id)
    {
        $this->payment=(new Payment())->find($id);
        $this->alias=$this->payment->alias;
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            $count = $this->model->where('alias',$this->alias)->count();
            $list = $this->model->where('alias',$this->alias)->order($this->sort)->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        $this->assign('payment', $this->payment);
        return $this->fetch();
    }

    public function add($id=null)
    {
        if ($this->request->isAjax()) {
            $_data = $this->request->param();
            $data['alias'] = $_data['alias'];
            $data['title'] = $_data['title'];
            $data['pay_types'] = $_data['pay_types'];
            $data['config'] = $_data;
            try {
                $save = $this->model->save($data);
            } catch (\Exception $e) {
                $this->error('保存失败'.$e->getMessage());
            }
            if ($save) {

                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
        $this->payment=(new Payment())->find($id);
        $this->alias=$this->payment->alias;
        $this->assign('payment', $this->payment);
        return $this->fetch( $this->alias . '_add');
    }

    public function edit($id)
    {
        $item = $this->model->getItem(['id'=>$id]);
        $this->assign('item', $item);
        return $this->fetch($item['alias'] . '_edit');
    }

    public function save()
    {
        $_data = $this->request->post();
        $data['title'] = $_data['title'];
        $data['pay_types'] = $_data['pay_types'];
        $data['config'] = $_data;
            $model = $this->model->where('id', $_data['id'])->find();
        $result=$model->save($data);
        if ($result !== false) {
            $this->success('保存成功');
        } else {
            $this->success('保存失败');
        }
    }

    public function testpay($id){

        $account=$this->model->getItem(['id'=>$id]);

        $order=[
            'subject_id'=>0,
            'subject_num'=>1,
            'subject'=>$account['alias'].$id.'支付测试',
            'amount'=>mt_rand(3,20)/10,
            'order_no' => encrypt(date('YmdHis') . uniqid(),env('key'))
        ];

        $payments=[];
        $arr=$account['pay_types'];
        foreach ($arr as $vo){
            $payments[]=[
              'ico'=>'/static/common/images/pay/'.$vo.'.png',
              'pay_type'=>$vo,
              'id'=>$id
            ];
        }
        $this->assign('order', $order);
        $this->assign('payments', $payments);
        $order['is_test']=1;
        $order['user_ip']=request()->ip();
        $this->assign('orderEncode',encrypt(json_encode($order),env('key')));
        return $this->fetch();
    }
}
