<?php

namespace app\admin\controller\system;


use app\admin\service\TriggerService;
use app\common\constants\MenuConstant;
use app\common\model\Service;
use bt\Bt;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\common\controller\AdminController;
use app\common\model\Crontab as Model;
use think\App;
use think\Cache;

/**
 * Class Crontab
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="定时任务管理",auth=true)
 */
class Crontab extends AdminController
{

    protected $alias;
    protected $payment;
    use \app\admin\traits\Curd;
    protected $allowModifyFields = [
        'title','is_add'
    ];
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Model();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            [$page, $limit, $where] = $this->buildTableParames();

            $key=sysconfig('key.job_key');
            $count = $this->model
                ->where($where)
                ->select();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            foreach ($list as &$v){
                $wenhao=strpos($v['job_action'],'?')!==false?'&':'?';
                $v['job_url']=domain_url('/index/job/'.$v['job_action']).$wenhao.'key='.$key;
                $cycle=$this->model->getCycle($v);
                $v['cycle']=$cycle['cycle_str'];

            }

            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }
    /**
     * @NodeAnotation(title="配置")
     */
    function config(){
        return $this->fetch();
    }

    public function import_to_bt(){

        header('Content-type: application/json');

        $conf = sysconfig('key');
        $siteTitle= sysconfig('site.site_name');
        $bt = new Bt($conf['bt_panel_url'], $conf['bt_api_key']);
        $jobArr = $this->model->where('is_add',0)->select();
        $baseUrl = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'])) . '/';
        $weburl = (is_HTTPS() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . str_replace('//', '/', $baseUrl);
        $count = count($jobArr);
        $success = 0;
        foreach ($jobArr as $v) {
            $wenhao=strpos($v['job_action'],'?')!==false?'&':'?';
            $urladdress = $weburl . 'index/job/' . $v['job_action'] . $wenhao.'job_key=' . $conf['job_key'];
            $result = $bt->AddCrontab('【'.$siteTitle.'】'.$v['title'], $v['s_type'], $v['type'], $v['where1'], $v['hour'], $v['minute'], '', $urladdress);
            if(!is_array($result)){
                return fail($result);
            }
            if ($result['status'] == true){
                $success++;
                $this->model->where('id',$v['id'])->update(['is_add'=>1]);
            }
        }
        $this->success('新任务总数' . $count . '个，成功添加.' . $success . '个');

    }
}
