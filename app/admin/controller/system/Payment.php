<?php
namespace app\admin\controller\system;

use app\common\controller\AdminController;
use app\common\model\Payment as Model;
use think\App;

/**
 * Class PaymentMethod
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="支付方式管理",auth=true)
 */
class Payment extends AdminController
{
    use \app\admin\traits\Curd;
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Model();
    }

}
