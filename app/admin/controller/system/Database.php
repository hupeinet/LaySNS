<?php
namespace app\admin\controller\system;
use app\admin\model\Backup;
use app\common\controller\AdminController;
use think\App;
use think\facade\Request;
use think\facade\Session;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Env;

class Database extends AdminController
{

    protected $backPath;
    protected $lockFile;
    protected $tablePrefix;

    public function initialize() {
        parent::initialize();
        $this->backPath=root_path()."data".DS."backup".DS."database".DS;
        $this->lockFile=$this->backPath.'backup.lock';
        $this->tablePrefix=config('database.connections.mysql.prefix');
    }


    // 数据库列表
    public function index(){

        if (Request::isAjax()) {
            $dbtables = Db::query('SHOW TABLE STATUS');
            $list = array();
            $protectTables=$this->protectTables();
            foreach ($dbtables as $k => $v) {
                $v['Baoliu']=in_array($v['Name'],$protectTables)?'保留':'';
                $v['size'] = format_bytes($v['Data_length'] + $v['Index_length']);
                $list[$k] = $v;
            }
            @unlink($this->lockFile);
            //备份完成，清空缓存
            session::set('backup_tables', null);
            session::set('backup_file', null);
            session::set('backup_config', null);
            $result = ['code' => 0, 'data' => $list,'count'=>''];
            return $result;
        }
        return $this->fetch();
    }
    public function sql(){
        if (Request::isPost()) {
            $sql=input('sql' , '');
            if(!$sql){
              return fail('没有SQL内容');
            }
            try{
//                importsql($sql,input('prefix'));
                importsql(str_ireplace(['drop','delete','truncate'],'',$sql),input('prefix'));
            }catch (\Exception $e){
                return fail($e->getMessage());
            }

         return $this->success('执行完成');
        }
        return $this->fetch();
    }
    // 数据库优化
    public function optimize(){
        if (Request::isPost()) {
            $table[] = input('tablename' , '');
            if (empty($table)) {
                $result = ['code' => false, 'msg' => '请选择数据表！'];
                return $result;
            }
            $strTable = implode(',', $table);
            if (!DB::query("OPTIMIZE TABLE {$strTable} ")) {
                $strTable = '';
            }
            $result = ['code' => true, 'msg' => '全部优化成功'];
            return $result;
        }
        $result = ['code' => false, 'msg' => '操作失败！'];
        return $result;
    }

    // 数据库修复
    public function repair(){
        if (Request::isPost()) {
            $table[] = input('tablename' , '');
            if (empty($table)) {
                $result = ['code' => false, 'msg' => '请选择数据表！'];
                return $result;
            }
            $strTable = implode(',', $table);
            if (!DB::query("REPAIR TABLE {$strTable} ")) {
                $strTable = '';
            }
            $result = ['code' => true, 'msg' => '全部修复成功'];
            return $result;
        }
        $result = ['code' => false, 'msg' => '操作失败！'];
        return $result;
    }

    private function protectTables(){
        $protectTables = [
            'article_category',
            'crontab',
            'nav',
            'page',
            'payment',
            'payment_account',
            'system_admin',
            'auth',
            'auth_node',
            'system_config',
            'system_menu',
            'system_quick',
            'user_grade'
        ];
        array_walk($protectTables,
            function (&$value, $key,$prefix) {
                $value = $prefix . $value;
            }, $this->tablePrefix);
        return $protectTables;
    }
    //初始化表
    public function init(){
        //先备份一下表

        $allTables = Db::query("show tables");
        $protectTables=$this->protectTables();
        $database = config('database.connections.mysql.database');
        $msg='';
        foreach ($allTables as $v) {
            $table = $v['Tables_in_' . $database];
            if (!in_array($table, $protectTables)) {
                Db::execute("TRUNCATE TABLE ".$table);
                $msg.='已清空 '.$table.'<br>';
//                ob_flush();//把数据从PHP的缓冲（buffer）中释放出来。
//                flush();
            }
        }
        $this->success($msg);
    }

    // 数据备份
    public function export($tables = null,$id = null,$start = null,$optstep = 0)
    {
        //防止备份数据过程超时
        function_exists('set_time_limit') && set_time_limit(0);
        $path = $this->backPath;
        $lockFile = $this->lockFile;
        //升级完自动备份所有数据表
        if ('all' == $tables){
            $dbtables = Db::query('SHOW TABLE STATUS');
            $list = array();
            foreach ($dbtables as $k => $v) {
                $list[] = $v['Name'];
            }
            $tables = $list;
            @unlink($lockFile);
        }
        if(Request::isPost() && !empty($tables) && is_array($tables) && empty($optstep))
        {
            //初始化
            if(!empty($path) && !is_dir($path)){
                mkdir($path, 0755, true);
            }
            //读取备份配置
            $config = array(
                'path'     => $path,
                'part'     => 52428800,
                'compress' =>  0,
                'level'    => 9,
            );
            //检查是否有正在执行的任务
            if(is_file($lockFile)){
                $this->error('检测到有一个备份任务正在执行，请稍后再试！');
            }else{
                //创建锁文件
                file_put_contents($lockFile, $_SERVER['REQUEST_TIME']);
            }
            //检查备份目录是否可写
            if(!is_writeable($path)){
                $this->error('备份目录不存在或不可写，请检查后重试！');
            }
            //缓存要备份的备份信息
            session::set('backup_config', $config);
            //生成备份文件信息
            $file = array(
                'name' => date('Ymd-His', $_SERVER['REQUEST_TIME']),
                'part' => 1,
                'version'=>Env::get('VERSION')
            );
            //缓存要备份的文件
            session::set('backup_file', $file);
            //缓存要备份的表
            session::set('backup_tables', $tables);
            //创建备份文件
            $Database = new Backup($file, $config);
            if(false !== $Database->Backup_Init()){
                $speed = (floor((1/count($tables))*10000)/10000*100);
                $speed = sprintf("%.2f", $speed);
                $tab = array('id' => 0, 'start' => 0, 'speed'=>$speed, 'table'=>$tables[0], 'optstep'=>1);
                return json(['code' => 1, 'msg' => '初始化成功！','tab'=>$tab,'tables'=>$tables]);
            }else{
                $this->error( '初始化失败，备份文件创建失败！');
            }
        }elseif (Request::isPost() && is_numeric($id) && is_numeric($start) && 1 == intval($optstep)){
            $tables = session::get('backup_tables');
            //备份指定表
            $Database = new Backup(session::get('backup_file'), session::get('backup_config'));
            $start  = $Database->backup($tables[$id], $start);
            if(false === $start){
                $this->error('备份出错！');
            } elseif (0 === $start) { 
                //下一表
                if(isset($tables[++$id])){
                    $speed = (floor((($id+1)/count($tables))*10000)/10000*100);
                    $speed = sprintf("%.2f", $speed);
                    $tab = array('id' => $id, 'start' => 0, 'speed' => $speed, 'table'=>$tables[$id], 'optstep'=>1);
                    return ['code' => 1, 'msg' => '备份完成！','tab'=>$tab];
                } else { 
                    //备份完成，清空缓存
                    /*自动覆盖安装目录数据库*/
                    $install_time = time();
                    $install_path = root_path().'install';
                    if (!is_dir($install_path) || !file_exists($install_path)) {
                        $install_path = root_path().'install_'.$install_time;
                    }
                    if (is_dir($install_path) && file_exists($install_path)) {
                        $srcfile = session::get('backup_config.path').session::get('backup_file.name').'-'.session::get('backup_file.part').'-'.session::get('backup_file.version').'.sql';
                        $dstfile = $install_path.'/laysns.sql';
                        if(@copy($srcfile, $dstfile)){
                            //替换所有表的前缀为官方默认laysns_，并重写安装数据包里
                            $laysnsDbStr = file_get_contents($dstfile);
                            $dbtables = Db::query('SHOW TABLE STATUS');
                            foreach ($dbtables as $k => $v) {
                                $tableName = $v['Name'];
                                if (preg_match('/^'.$this->tablePrefix.'/i', $tableName)) {
                                    $laysnsTableName = preg_replace('/^'.$this->tablePrefix.'/i', 'laysns_', $tableName);
                                    $laysnsDbStr = str_replace('`'.$tableName.'`', '`'.$laysnsTableName.'`', $laysnsDbStr);
                                }
                            }
                            @file_put_contents($dstfile, $laysnsDbStr);
                        } else {
                            @unlink($dstfile);//复制失败就删掉，避免安装错误的数据包
                        }
                    }
                    @unlink(session::get('backup_config.path') . 'backup.lock');
                    session::set('backup_tables', null);
                    session::set('backup_file', null);
                    session::set('backup_config', null);
                    $this->success('备份完成！');
                }
            } else {
                $rate = floor(100 * ($start[0] / $start[1]));
                $speed = floor((($id+1)/count($tables))*10000)/10000*100 + ($rate/100);
                $speed = sprintf("%.2f", $speed);
                $tab  = array('id' => $id, 'start' => $start[0], 'speed' => $speed, 'table'=>$tables[$id], 'optstep'=>1);
                return json(['code' => 1, 'msg' => '正在备份...({$rate}%)','tab'=>$tab]);
            }
        }else{
            $result = ['code' => 1, 'msg' => '参数有误'];
            return $result;
        }
    }

    // 数据还原
    public function restore()
    {
        if (Request::isAjax()) {
            $path = $this->backPath;
            if(!empty($path) && !is_dir($path)){
                mkdir($path, 0755, true);
            }
            $flag = \FilesystemIterator::KEY_AS_FILENAME;
            $glob = new \FilesystemIterator($path,  $flag);
            $list = array();
            $filenum = $total = 0;
            foreach ($glob as $name => $file) {
                if(preg_match('/^\d{8,8}-\d{6,6}-\d+-\d+\.\d(.*)\.sql(?:\.gz)?$/', $name)){
                    $name = sscanf($name, '%4s%2s%2s-%2s%2s%2s-%d-%s');

                    $date = "{$name[0]}-{$name[1]}-{$name[2]}";
                    $time = "{$name[3]}:{$name[4]}:{$name[5]}";
                    $part = $name[6];
                    $version = preg_replace('#\.sql(.*)#i', '', $name[7]);
                    $info = pathinfo($file);
                    if(isset($list["{$date} {$time}"])){
                        $info = $list["{$date} {$time}"];
                        $info['part'] = max($info['part'], $part);
                        $info['size'] = $info['size'] + $file->getSize();
                    } else {
                        $info['part'] = $part;
                        $info['size'] = $file->getSize();
                    }
                    $info['compress'] = ($info['extension'] === 'sql') ? '-' : $info['extension'];
                    $info['id']=$info['time']  = strtotime("{$date} {$time}");
                    $info['showtime']  = friendlyDate(strtotime("{$date} {$time}"));
                    $info['version']  = $version;
                    $filenum++;
                    $info['size'] = format_bytes($info['size']);
                    $list["{$info['time']}"] = $info;
                }
            }
            array_multisort($list, SORT_DESC);
            $result = ['code' => 0, 'data' => $list,'count'=>$filenum];
            return $result;
        }
        return $this->fetch();
    }

    // 下载
    public function downFile($time = 0)
    {
        $path = $this->backPath.date('Ymd-His', $time) . '-*.sql*';
        $files = glob($path);
        if(is_array($files)){
            foreach ($files as $filePath){
                if (!file_exists($filePath)) {
                    return $this->errorNotice('该文件不存在，可能是被删除',true,3,false);
                }else{
                    $filename = basename($filePath);
                    header("Content-type: application/octet-stream");
                    header('Content-Disposition: attachment; filename="' . $filename . '"');
                    header("Content-Length: " . filesize($filePath));
                    readfile($filePath);
                }
            }
        }
    }

    // 删除备份文件
    public function delete()
    {
        $time_arr = input('id/a');
        if(!is_array($time_arr)) {
            $time_arr = [$time_arr];
        }
        $path=$this->backPath;
        if(is_array($time_arr) && !empty($time_arr)){
            foreach ($time_arr as $key => $val) {
                $fileName  = $path.date('Ymd-His', $val) . '-*.sql*';
                array_map("unlink", glob($fileName));
                if(count(glob($fileName))){
                    $result = ['code' => 0, 'msg' => '备份文件删除失败，请检查目录权限！'];
                    return $result;
                }
            }
            $result = ['code' => 1, 'msg' => '删除成功！'];
            return $result;
        } else {
            $result = ['code' => 0, 'msg' => '参数有误'];
            return $result;
        }
    }

    // 执行还原数据库操作
    public function new_import($time = 0)
    {
        function_exists('set_time_limit') && set_time_limit(0);
        $time = input('time');
        if(is_numeric($time) && intval($time) > 0){
            // 获取备份文件信息
            $files = glob($this->backPath.date('Ymd-His', $time) . '-*.sql*');
            $list  = array();
            foreach($files as $name){
                $basename = basename($name);
                $match    = sscanf($basename, '%4s%2s%2s-%2s%2s%2s-%d-%s');
                $gz       = preg_match('/^\d{8,8}-\d{6,6}-\d+-\d+\.\d(.*)\.sql(?:\.gz)?$/', $basename);
                $list[$match[6]] = array($match[6], $name, $gz);
            }
            ksort($list);
            // 检测文件正确性
            $last = end($list);
            $file_path_full = !empty($last[1]) ? $last[1] : '';
            if (file_exists($file_path_full)) {
                // 校验sql文件是否属于当前CMS版本
                preg_match('/(\d{8,8})-(\d{6,6})-(\d+)-(\d+\.\d(.*))\.sql/i', $file_path_full, $matches);
                $version = Env::get('VERSION');
                if ($matches[4] != $version) {
                    $result = ['code' => 0, 'msg' => 'sql不兼容当前版本：'.$version];
                    return $result;
                }
                $sqls = Backup::parseSql($file_path_full);
                if(Backup::install($sqls)){
                    // 清除缓存
                    Cache::clear();//清除数据缓存文件
                    $admin_temp = glob(root_path() . 'runtime/admin/temp/'. '*.php');//清除后台临时文件缓存
                    array_map('unlink', $admin_temp);
                    $result = ['code' => 1, 'msg' => '操作成功'];
                    return $result;
                }else{
                    $result = ['code' => 0, 'msg' => '操作失败'];
                    return $result;
                }
            }
        }else{
            $result = ['code' => 0, 'msg' => '参数有误'];
            return $result;
        }
    }
}
