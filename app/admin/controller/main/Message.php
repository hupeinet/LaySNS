<?php
namespace app\admin\controller\main;

use app\common\controller\AdminController;
use app\common\model\Message as Model;
use think\App;

/**
 * @ControllerAnnotation(title="消息管理")
 * Class Message
 * @package app\admin\controller\main
 */
class Message extends AdminController
{

    use \app\admin\traits\Curd;
    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Model();
    }




}