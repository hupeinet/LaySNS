<?php
namespace app\admin\controller\main;

use app\admin\service\TriggerService;
use app\common\constants\MenuConstant;
use app\common\controller\AdminController;
use app\common\model\Article;
use app\common\model\Nav;
use app\common\model\ArticleCategory  as Model;
use think\App;

/**
 * @ControllerAnnotation(title="文章管理")
 * Class ArticleCategory
 * @package app\admin\controller\main
 */
class ArticleCategory extends AdminController
{

    use \app\admin\traits\Curd;
    protected $allowModifyFields = [
        'status',
        'sort',
        'title',
        'is_nav'
    ];
    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model=new Model();
        $pidCateList=Model::getPidCateList();
        $this->assign('pidCateList', $pidCateList);
    }



    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            $count = $this->model->count();
            $list = $this->model->order($this->sort)->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add($id = null)
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $rule = [
                'pid|上级栏目'   => 'require',
                'title|栏目名称' => 'require',
                'icon|栏目图标'  => 'require',
            ];
            $this->validate($post, $rule);
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            if ($save) {
                TriggerService::updateMenu();
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
        $pidMenuList = $this->model->getPidCateList();
        $this->assign('id', $id);
        $this->assign('getPidCateList', $pidMenuList);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="属性修改")
     */
    public function modify()
    {
        $post = $this->request->post();
        $rule = [
            'id|ID'    => 'require',
            'field|字段' => 'require',
            'value|值'  => 'require',
        ];
        $this->validate($post, $rule);
        if (!in_array($post['field'], $this->allowModifyFields)) {
            $this->error('该字段不允许修改：' . $post['field']);
        }
        $id=$post['id'];
        $row = $this->model->find($id);
        empty($row) && $this->error('数据不存在');
        try {
            $row->save([
                $post['field'] => $post['value'],
            ]);

            if($post['field']=='is_nav'){
                if($post['value']==1&&!Nav::where('category_id',$id)->find()){

                        $categorys = getJsonData('ArticleCategory', 'list');
                        $cate = array_column($categorys, 'alias', 'id');
                        $_pid=$row->pid;
                        $pid=0;
                        if($_pid){
                            $nav=Nav::where('category_id',$_pid)->find();
                            if($nav){
                                $pid=$nav->id;
                            }
                        }

                        Nav::create([
                            'title' => $row['title'],
                            'pid' => $pid,
                            'category_id' => $id,
                            'link' => Article::categoryUrl($id, $cate),
                        ]);

                }elseif($post['value']==0){
                    Nav::where('category_id',$id)->delete();
                }
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('保存成功');
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {
        $this->isDemo && $this->error('演示环境下不允许修改');
        $row = $this->model->whereIn('id', $id)->select();
        $row->isEmpty() && $this->error('数据不存在');
        try {
            $row2 = (new Nav())->whereIn('category_id', $id)->select();
            $row2->delete();
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }
}