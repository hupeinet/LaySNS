<?php
namespace app\admin\controller\main;


use app\admin\model\SystemConfig;
use app\admin\service\TriggerService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;
/**
 * @ControllerAnnotation(title="内容配置管理")
 * Class Config
 * @package app\admin\controller\main
 */
class Config extends AdminController
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model = new SystemConfig();
    }

    /**
     * @NodeAnotation(title="文章配置")
     */
    public function article()
    {
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="评论配置")
     */
    public function comment()
    {
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="SEO配置")
     */
    public function seo()
    {
        return $this->fetch();
    }
    /**
     * @NodeAnotation(title="友链配置")
     */
    public function friendly_link()
    {
        return $this->fetch();
    }
}