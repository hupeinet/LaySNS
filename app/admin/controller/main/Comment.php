<?php
namespace app\admin\controller\main;

use app\common\controller\AdminController;
use app\common\model\Comment as Model;
use think\App;

/**
 * @ControllerAnnotation(title="单页管理")
 * Class Comment
 * @package app\admin\controller\main
 */
class Comment extends AdminController
{

    use \app\admin\traits\Curd;
    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Model();
    }




}