<?php

namespace app\admin\controller\main;

use app\common\controller\AdminController;
use app\common\model\SeoLog as Model;
use app\common\model\Article;
use seo\Baidu;
use think\App;

/**
 * @ControllerAnnotation(title="SEO推送记录")
 * Class SeoLog
 * @package app\admin\controller\main
 */
class SeoLog extends AdminController
{

    use \app\admin\traits\Curd;

    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model = new Model();
    }

    public function push2seo()
    {
        $data = request()->param();
        $type = $data['type'];
        return  Model::pushToBaidu($type);
    }

    public function checkSL($url)
    {
        $res = Baidu::checkBaidu($url);

        return json($res);
    }

}