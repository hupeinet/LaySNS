<?php
namespace app\admin\controller\main;

use app\common\controller\AdminController;

use app\common\model\CollectCategory as Model;


use think\App;



/**
 * @ControllerAnnotation(title="采集栏目配置")
 * Class SeoLog
 * @package app\admin\controller\main
 */
class CollectCategory extends AdminController
{
    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model=new Model();
    }


}