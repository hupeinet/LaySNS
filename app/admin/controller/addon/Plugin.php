<?php


namespace app\admin\controller\addon;


use app\common\controller\AdminController;
use think\App;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\facade\ThinkAddons;
use app\common\builder\FormBuilder;
use app\common\model\Article;
use think\facade\Request;
use think\facade\View;


/**
 * @ControllerAnnotation(title="插件管理")
 * Class Plugin
 * @package app\admin\controller\addon
 */
class Plugin extends AdminController
{

    use \app\admin\traits\Curd;

    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model=new Article();
    }

    /**
     * @NodeAnotation(title="插件列表")
     */
    public function index()
    {
        //目录下面有的

        if ($this->request->isAjax()) {

            $list = ThinkAddons::localAddons();
            $data = [
                'code' => 0,
                'msg' => '',
                'count' => count($list),
                'data' => $list,
            ];

            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="安装插件")
     */
    public function install(string $name)
    {
        return ThinkAddons::install($name);
    }

    /**
     * @NodeAnotation(title="删除插件")
     */
    public function delete(string $name)
    {
        $this->isDemo && $this->error('演示环境下不允许删除');
        $dir = ROOT_PATH . 'addons' . DS . $name;
        @rmdirs($dir);
        if (!is_dir($dir)) {
            return success('删除成功');
        } else {
            return fail('删除失败');
        }
    }

    /**
     * @NodeAnotation(title="已装插件")
     */
    public function installed()
    {
        return $this->model->column('name,version');
    }
    public function download(){
        $key = env('key');
        if (!$key) {
            return fail('没有配置KEY');
        }
        $params = $this->request->param();
        return \org\Laysns::update($params['type'],$params['ver_id'],$params['name']);
    }

    /**
     * @NodeAnotation(title="在线插件")
     */
    public function online()
    {

        if ($this->request->isAjax()) {
            [$page, $limit, $where] = $this->buildTableParames();
            $keyword='';
            if(isset($where['description'])){
                $keyword= $where['description'];
            }

            $data = \org\Laysns::getApps(['type'=>2,'page'=>$page,'keyword'=>$keyword]);
            $arr = json_decode($data, true);

            if (!is_array($arr)) {
                return fail($data);
            }
            if ($arr['code'] != 1) {
                return fail('获取列表失败');
            }
            $info = $arr['data'];

            $localAddons = ThinkAddons::localAddons();
            $addon_array = array_column($localAddons, null, 'name');

            foreach ($info as $k => &$v) {
                $v['is_down'] = 0;
                if (isset($addon_array[$v['name']])) {
                    $localItem = $addon_array[$v['name']];
                    $v['is_down'] = 1;
                    if ($localItem['version'] < $v['version']) {
                        $v['is_down'] = 2;
                    }
                }
            }
            $data = [
                'code' => 0,
                'msg' => '',
                'count' => count($info),
                'data' => $info,
            ];

            return json($data);
        }
        return view();
    }

    /**
     * @NodeAnotation(title="插件后台管理")
     */
    public function adminlist($name)
    {
        $addon = ThinkAddons::getInstance($name);
        if ($addon) {
            $this->redirect(addons_url("$name://Admin/index"));
        }
        return $this->fetch();
    }

    /**
     * 设置插件页面
     */
    public function config($name)
    {
        $config = ThinkAddons::config($name);
        // 如果插件自带配置模版的话加载插件自带的，否则调用表单构建器
        $file = app()->getRootPath() . 'addons' . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'config.html';
        if (file_exists($file)) {
            View::assign([
                'config' => $config
            ]);
            return View::fetch($file);
        } else {
            // 获取字段数据
            $columns = $this->makeAddColumns($config);
            // 判断是否分组
            $group = ThinkAddons::checkConfigGroup($config);
            // 构建页面
            $builder = FormBuilder::getInstance();
            $builder->setFormUrl(url('configSave'))
                ->addHidden('id', $name);
            $group ? $builder->addGroup($columns) : $builder->addFormItems($columns);
            return $builder->fetch();
        }
    }

    // 生成表单信息
    private function makeAddColumns(array $config)
    {
        // 判断是否开启了分组
        if (ThinkAddons::checkConfigGroup($config) === false) {
            // 未开启分组
            return $this->makeAddColumnsArr($config);
        } else {
            $columns = [];
            // 开启分组
            foreach ($config as $k => $v) {
                $columns[$k] = $this->makeAddColumnsArr($v);
            }

            return $columns;
        }
    }

    // 生成表单返回数组
    private function makeAddColumnsArr(array $config)
    {
        $columns = [];
        foreach ($config as $k => $field) {
            // 初始化
            $field['name'] = $field['name'] ?? $field['title'];
            $field['field'] = $k;
            $field['tips'] = $field['tips'] ?? '';
            $field['required'] = $field['required'] ?? 0;
            $field['group'] = $field['group'] ?? '';
            if (!isset($field['setup'])) {
                $field['setup'] = [
                    'default' => $field['value'] ?? '',
                    'extra_attr' => $field['extra_attr'] ?? '',
                    'extra_attr' => $field['extra_attr'] ?? '',
                    'extra_class' => $field['extra_class'] ?? '',
                    'placeholder' => $field['placeholder'] ?? '',
                ];
            }

            if ($field['type'] == 'text') {
                $columns[] = [
                    $field['type'],                // 类型
                    $field['field'],               // 字段名称
                    $field['name'],                // 字段别名
                    $field['tips'],                // 提示信息
                    $field['setup']['default'],    // 默认值
                    $field['group'],               // 标签组，可以在文本框前后添加按钮或者文字
                    $field['setup']['extra_attr'], // 额外属性
                    $field['setup']['extra_class'],// 额外CSS
                    $field['setup']['placeholder'],// 占位符
                    $field['required'],            // 是否必填
                ];
            } elseif ($field['type'] == 'textarea' || $field['type'] == 'password') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['extra_attr'],        // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['setup']['placeholder'] ?? '', // 占位符
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'radio' || $field['type'] == 'checkbox') {
                $columns[] = [
                    $field['type'],                // 类型
                    $field['field'],               // 字段名称
                    $field['name'],                // 字段别名
                    $field['tips'],                // 提示信息
                    $field['options'],             // 选项（数组）
                    $field['setup']['default'],    // 默认值
                    $field['setup']['extra_attr'], // 额外属性 extra_attr
                    '',                            // 额外CSS extra_class
                    $field['required'],            // 是否必填
                ];
            } elseif ($field['type'] == 'select' || $field['type'] == 'select2') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['options'],                    // 选项（数组）
                    $field['setup']['default'],           // 默认值
                    $field['setup']['extra_attr'],        // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['setup']['placeholder'] ?? '', // 占位符
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'number') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    '',                                   // 最小值
                    '',                                   // 最大值
                    '',              // 步进值
                    '',        // 额外属性
                    '',       // 额外CSS
                    '', // 占位符
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'hidden') {
                $columns[] = [
                    $field['type'],                      // 类型
                    $field['field'],                     // 字段名称
                    $field['setup']['default'] ?? '',    // 默认值
                    $field['setup']['extra_attr'] ?? '', // 额外属性 extra_attr
                ];
            } elseif ($field['type'] == 'date' || $field['type'] == 'time' || $field['type'] == 'datetime') {
                // 使用每个字段设定的格式
                if ($field['type'] == 'time') {
                    $field['setup']['format'] = str_replace("HH", "h", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("mm", "i", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("ss", "s", $field['setup']['format']);
                    $format = $field['setup']['format'] ?? 'H:i:s';
                } else {
                    $field['setup']['format'] = str_replace("yyyy", "Y", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("mm", "m", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("dd", "d", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("hh", "h", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("ii", "i", $field['setup']['format']);
                    $field['setup']['format'] = str_replace("ss", "s", $field['setup']['format']);
                    $format = $field['setup']['format'] ?? 'Y-m-d H:i:s';
                }
                $field['setup']['default'] = $field['setup']['default'] > 0 ? date($format, $field['setup']['default']) : '';
                $columns[] = [
                    $field['type'],                // 类型
                    $field['field'],               // 字段名称
                    $field['name'],                // 字段别名
                    $field['tips'],                // 提示信息
                    $field['setup']['default'],    // 默认值
                    $field['setup']['format'],     // 日期格式
                    $field['setup']['extra_attr'], // 额外属性 extra_attr
                    '',                            // 额外CSS extra_class
                    $field['setup']['placeholder'],// 占位符
                    $field['required'],            // 是否必填
                ];
            } elseif ($field['type'] == 'daterange') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['format'],            // 日期格式
                    $field['setup']['extra_attr'] ?? '',  // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'tag') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['extra_attr'] ?? '',  // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'image' || $field['type'] == 'images' || $field['type'] == 'file' || $field['type'] == 'files') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['size'],              // 限制大小（单位kb）
                    $field['setup']['ext'],               // 文件后缀
                    $field['setup']['extra_attr'] ?? '',  // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['setup']['placeholder'] ?? '', // 占位符
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'editor') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['heidht'] ?? 0,       // 高度
                    $field['setup']['extra_attr'] ?? '',  // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['required'],                   // 是否必填
                ];
            } elseif ($field['type'] == 'color') {
                $columns[] = [
                    $field['type'],                       // 类型
                    $field['field'],                      // 字段名称
                    $field['name'],                       // 字段别名
                    $field['tips'],                       // 提示信息
                    $field['setup']['default'],           // 默认值
                    $field['setup']['extra_attr'] ?? '',  // 额外属性
                    $field['setup']['extra_class'] ?? '', // 额外CSS
                    $field['setup']['placeholder'] ?? '', // 占位符
                    $field['required'],                   // 是否必填
                ];
            }
        }
        return $columns;
    }
    /**
     * 保存插件设置
     */
    // 插件配置信息保存
    public function configSave()
    {
        if (Request::isPost()) {
            $result = ThinkAddons::configPost(Request::except(['file'], 'post'));
            if ($result['code'] == 1) {
                $this->success($result['msg'], 'index');
            } else {
                $this->error($result['msg']);
            }
        }
    }

// 更改插件状态 [启用/禁用]
    public function state(string $name)
    {
        return ThinkAddons::state($name);
    }

    // 卸载插件
    public function uninstall(string $name)
    {
        $this->isDemo && $this->error('演示环境下不允许卸载');
        return ThinkAddons::uninstall($name);
    }
}
