<?php


namespace app\admin\controller\addon;


use app\admin\model\SystemConfig;
use app\common\controller\AdminController;
use think\App;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\facade\ThinkAddons;
use app\common\builder\FormBuilder;
use app\common\model\Article;
use think\facade\Request;
use think\facade\View;


/**
 * @ControllerAnnotation(title="模板管理")
 * Class Template
 * @package app\admin\controller\addon
 */
class Template extends AdminController
{

    use \app\admin\traits\Curd;

    /**
     * 初始化方法
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        parent::initialize();
        $this->model = new Article();
    }

    /**
     * @NodeAnotation(title="启用模板")
     */
    public function setUse()
    {
        $data=$this->request->param();
        $res=\org\Laysns::setTpl($data['controller'],$data['name']);
       if($res){
           return success('操作成功');
       }else{
           return fail('操作失败');
       }
    }


    /**
     * @NodeAnotation(title="模板列表")
     */
    public function index()
    {
        $res=\org\Laysns::getLocalTpls();

        $this->assign('use', $res['use']);
        $this->assign('not_use', $res['not_use']);
        return $this->fetch();
    }


    /**
     * @NodeAnotation(title="删除模板")
     */
    public function delete($controller,string $name)
    {
        $this->isDemo && $this->error('演示环境下不允许删除');
        $theme_url = ROOT_PATH . 'public' . DS . 'template' . DS . $controller . DS . $name . DS;
        @rmdirs($theme_url);
        if (!is_dir($theme_url)) {
            return success('删除成功');
        } else {
            return fail('删除失败');
        }
    }


    public function download()
    {
        $key = env('key');
        if (!$key) {
            return fail('没有配置KEY');
        }
        $params = $this->request->param();
        return \org\Laysns::update($params['type'], $params['ver_id'], $params['name']);
    }

    /**
     * @NodeAnotation(title="在线模板")
     */
    public function online()
    {

        if ($this->request->isAjax()) {
            [$page, $limit, $where] = $this->buildTableParames();
            $keyword = '';
            if (isset($where['description'])) {
                $keyword = $where['description'];
            }
            $ret = \org\Laysns::getApps(['type' => 3, 'page' => $page, 'keyword' => $keyword]);
            $arr = json_decode($ret, true);
            if (!is_array($arr)) {
                return fail($ret);
            }
            if ($arr['code'] != 1) {
                return fail('获取列表失败');
            }
            $addon_array = $arr['data'];
            $allTpl= \org\Laysns::getLocalTpls();
            $localTpls=$allTpl['all'];
            foreach ($addon_array as $contro => &$vv) {
                foreach ($vv as $k => &$v) {
                    $v['is_down'] = 0;
                    if (isset($localTpls[$contro][$v['name']])) {
                        $localItem = $localTpls[$contro][$v['name']];
                        $v['is_down'] = 1;
                        if ($localItem['version'] < $v['version']) {
                            $v['is_down'] = 2;
                        }
                    }
                }
            }
            $data = [
                'code' => 1,
                'msg' => '',
                'count' => count($addon_array),
                'data' => $addon_array,
            ];
            return json($data);
        }
        return view();
    }

}
