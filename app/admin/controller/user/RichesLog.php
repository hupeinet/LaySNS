<?php


namespace app\admin\controller\user;

use app\admin\model\SystemAdmin;
use app\admin\model\SystemAuth;
use app\user\model\UserRichesLog as Model;
use app\user\model\User;
use app\common\controller\AdminController;
use app\user\model\UserRichesLog;
use think\App;

/**
 * @ControllerAnnotation(title="用户管理")
 * Class User
 * @package app\admin\controller\system
 */
class RichesLog extends AdminController
{

    use \app\admin\traits\Curd;

    protected $sort = [
        'a.id'   => 'desc',
    ];
    protected $allowModifyFields = [
        'status',
        'sort',
        'no_audit',
        'is_admin',
        'title'
    ];

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new Model();

    }

    public function index(){

        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->alias('a')
                ->join('user b','a.user_id=b.id')
                ->where($where)
                ->count();
            $list = $this->model
                ->alias('a')
                ->join('user b','a.user_id=b.id')
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

}