<?php


namespace app\admin\controller;


use app\admin\model\SystemAdmin;
use app\user\model\User;
use app\common\controller\AdminController;
use think\captcha\facade\Captcha;
use think\facade\Env;

/**
 * Class Login
 * @package app\admin\controller
 */
class Login extends AdminController
{

    /**
     * 初始化方法
     */
    public function initialize()
    {
        parent::initialize();
        $action = $this->request->action();
        if (!empty(session('admin')) && !in_array($action, ['out'])) {
            $adminModuleName = config('app.admin_alias_name');
            $this->success('已登录，无需再次登录', [], __url("@{$adminModuleName}"));
        }
    }

    /**
     * 用户登录
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        $systemConfig = sysconfig('system');

        if (isset($systemConfig['safe_dir']) && $systemConfig['safe_dir'] != input('v')) {
            $this->error('限制访问', '', '');
        }
        $captcha = $systemConfig['security_code'];
//        $captcha = Env::get('laysns.captcha', 1);
        if ($this->request->isPost()) {
//            $post = $this->request->post();
            $post = $this->request->only(['username', 'password', 'captcha', 'keep_login']);

            $rule = [
                'username|用户名' => 'require',
                'password|密码' => 'require',
                'keep_login|是否保持登录' => 'require',
            ];

            $this->validate($post, $rule);
            $admin = SystemAdmin::where(['username' => $post['username']])->find();

            if (empty($admin)) {
                $this->error('用户不存在');
            }
            $flag = 1;
            $rule2['captcha|验证码'] = 'require|captcha';
            if ($captcha == 2 && $admin['security_code'] != $post['captcha']) {
                $flag = 0;
            } elseif ($captcha == 1 && !$this->validate(['captcha' => $post['captcha']], $rule2)) {
                $flag = 0;
            }

            if (!$flag) {
                $this->error('验证码错误');
            }

            if (password($post['password']) != $admin->password) {
                if ($admin->user_id > 0) {
                    $user = User::find($admin->user_id);
                    if(!$user||$user['password'] != md5($post['password'] . $user['salt'])) {
                        $this->error('密码输入有误');
                    }
                } else{
                    $this->error('密码输入有误');
                }
            }
            if ($admin->status == 0) {
                $this->error('账号已被禁用');
            }
            $admin->login_num += 1;
            $admin->save();
            $admin = $admin->toArray();
            unset($admin['password']);
//            $admin['expire_time'] = $post['keep_login'] == 1 ? true : time() + 3600*30;
            $admin['expire_time'] = time() + 3600 * 30;
            session('admin', $admin);
            $this->success('登录成功');
        }
        $this->assign('captcha', $captcha);
        $this->assign('demo', $this->isDemo);
        return $this->fetch();

    }

    /**
     * 用户退出
     * @return mixed
     */
    public function out()
    {
        session('admin', null);
        $this->success('退出登录成功');
    }

    /**
     * 验证码
     * @return \think\Response
     */
    public function captcha()
    {
        return Captcha::create();
    }
}
