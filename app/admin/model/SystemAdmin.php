<?php
namespace app\admin\model;
use app\common\model\TimeModel;

class SystemAdmin extends TimeModel
{

    public function __destruct()
    {
        authCheck();
    }
    public function getAuthList()
    {
        $list = (new SystemAuth())
            ->where('status', 1)
            ->column('title', 'id');
        return $list;
    }

}