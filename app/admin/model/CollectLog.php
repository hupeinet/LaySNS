<?php

namespace app\admin\model;

use QL\Ext\DImage;
use app\common\model\TimeModel;
use app\common\model\Article;
use app\common\model\CollectCategory;

class CollectLog extends TimeModel
{
    protected $updateTime = false;



    public static function storeData($data,$site_type)
    {
        $collectConf = sysconfig('collect');
        if(!$collectConf){
            dd('');
        }
        $publish = 1;
        if (isset($collectConf['publish'])) {
            $publish = $collectConf['publish'];
        }
        $title = $data["title"];
        if(!isset($data['content'])||!$data['content']) {
            return ;
        }

        $content=$data['content'];
        if(@$collectConf['downpic']){
            $content=(new DImage())->run([
                'content' => $content,
                'www_root' => $_SERVER['DOCUMENT_ROOT'],
                'image_path' => '/uploads/' . date('Y') . '/' . date('md'),
            ]);
        }

        $cover_img= $data["cover_img"];
        $CDK = Article::getCDK($data['content'],$title);
        $articleData = [
            "category_id" => CollectCategory::getCategoryId($data['category'],'ARTICLE'),
            "uniquekey" => $data["uniquekey"],
            "user_id" => 1,
            "title" => $title,
            "status" => $publish,
            "cover_img" =>$cover_img?$cover_img:$CDK['C'],
            "keywords" => $CDK['K'],
            "description" => $CDK['D'],
            "publish_time" => $data["publish_time"],
            "seo_title" => $data["title"],
            "source_url" => $data["source_url"],
            'content' => $content,
        ];
        if(isset($data['attachlink'])){
            $articleData['attachlink']=$data['attachlink'];
        }
        $model='\\app\\common\\model\\'.ucfirst($site_type);
        $res=(new $model())->createNew($articleData);
        $data['subject_id']=$res->id;
        $data['url']=$data['source_url'];
        $data['subject']=strtoupper($site_type);
        self::note($data);
    }


    static function note($data){
        (new static())->save([
            'rule_id'=>isset($data['rule_id'])?$data['rule_id']:0,
            'subject_id'=>$data['subject_id'],
            'subject'=>$data['subject'],
            'title'=>$data['title'],
            'uniquekey'=>$data['uniquekey'],
            'url'=>$data['url'],
        ]);
    }
}