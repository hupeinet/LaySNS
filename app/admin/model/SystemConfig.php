<?php
namespace app\admin\model;

use app\common\model\TimeModel;

class SystemConfig extends TimeModel
{
    public function setValueAttr($value){
        return encrypt($value);
    }
}