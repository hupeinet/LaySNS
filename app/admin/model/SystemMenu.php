<?php



namespace app\admin\model;



use app\common\constants\MenuConstant;
use app\common\model\TimeModel;

class SystemMenu extends TimeModel
{


//    protected $deleteTime = 'delete_time';

    public function getPidMenuList()
    {
        $list        = $this->field('id,pid,title')
            ->where([
                ['pid', '<>', MenuConstant::HOME_PID],
                ['status', '=', 1],
            ])
            ->select()
            ->toArray();
        $pidMenuList = buildPidTree(0, $list);
        $pidMenuList = array_merge([[
            'id'    => 0,
            'pid'   => 0,
            'title' => '顶级菜单',
        ]], $pidMenuList);
        return $pidMenuList;
    }



}