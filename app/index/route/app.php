<?php
use think\facade\Route;
Route::rule('/', 'index');
Route::rule('news/:id','index/index/news');
Route::rule('pay/submit','index/pay/submit');
Route::rule('notify/:paymethod','index/pay/notify');
Route::rule('order/query','index/order/query');
Route::rule('submit','index/pay/submit');
Route::rule('qrcode','index/api/qrcode');
Route::rule('oauth/:name','index/SnsOauth/login');
Route::rule('bind/:name','index/SnsOauth/bind');
Route::rule('unbind/:name','index/SnsOauth/unbind');
Route::rule('callback/:name','index/SnsOauth/callback');
Route::rule('page/:alias','index/page/detail');
Route::rule('search/:keyword','index/index/search');
Route::rule('tag/:keyword','index/tag/detail');
Route::rule('tags','index/tag/index');

$listMode=sysconfig('article.list_url_mode');
$listPrefix=sysconfig('article.list_url_prefix');
$detailMode=sysconfig('article.detail_url_mode');
$detailPrefix=sysconfig('article.detail_url_prefix');

if ($listMode == 4&&$listPrefix) {
    Route::rule($listPrefix . '<alias>$', 'index/list');
}elseif ($listMode == 3&&$listPrefix) {
    Route::rule($listPrefix . '<id>$', 'index/list')->pattern(['id' => '[0-9]*']);
}elseif ($listMode == 2) {
    Route::rule('<alias>$', 'index/list')->pattern(['alias' => '[b-za]*']);
}elseif ($listMode == 1) {
    Route::rule( 'list/<alias>$', 'index/list')->pattern(['alias' => '[b-za]*']);
}
    Route::rule('list/:id','index/list');

if ($detailMode == 3&&$detailPrefix) {
    Route::rule($detailPrefix . '<id>$', 'index/html');
}elseif ($detailMode == 1) {
    $categorys = getJsonData('ArticleCategory', 'list');
    foreach ($categorys as $v) {
        if($v['alias']) {
            Route::rule($v['alias'] . '/<id>$', 'index/html');
        }
    }
}elseif($detailMode == 2){
    Route::rule(':id', 'Index/html')->pattern(['id' => '[0-9]*']);
}
Route::rule('html/:id', 'Index/html')->pattern(['id' => '[0-9]*']);