<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\model\Article;
use app\common\model\Comment;
use app\common\model\Tag as Model;


class Tag extends HomeBase
{
    public function initialize()
    {
        parent::initialize();
    }
    public function index()
    {
        $this->assign('hotTags', getJsonData('Tag','hotCount',[],32));
        return view();
    }

    public function detail($keyword = '')
    {
        $keyword = trim(remove_xss($keyword));
        if (!$keyword) {
            $this->redirect(url('index/tags/index'));
        }
        $page=isset($this->params['page'])?$this->params['page']:1;
        $this->assign('keyword', $keyword);
        $count=getJsonData('Tag','count',['keyword'=>$keyword],5);
        $result=getJsonData('Tag','list',['keyword'=>$keyword,'page'=>$page],5);
        if(!$count){
            (new Model())->clearTag($keyword);
        }
        $tag=Model::where('tag',$keyword)->find();
        if(!$tag){
            $tag=[
                'keywords'=>'',
                'description'=>'',
            ];
        }
        $this->assign('tag', $tag);
        $this->assign('count', $count);
        $this->assign('list', $result['data']);
        $this->assign('pages', $result['pages']);
        $comments=Comment::getItems('list');
        $this->assign('comments', $comments);

        $hotLimit=sysconfig('article.detail_hot_show');
        $this->assign('viewTopDay',getJsonData('ArticleAttribute','viewtop',['field'=>'day_view','limit'=>$hotLimit],6));
        $this->assign('viewTopWeek',getJsonData('ArticleAttribute','viewtop',['field'=>'week_view','limit'=>$hotLimit],30));
        $this->assign('viewTopMonth',getJsonData('ArticleAttribute','viewtop',['field'=>'month_view','limit'=>$hotLimit],60));
        return view();
    }



}