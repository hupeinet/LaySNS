<?php
namespace app\index\controller;

use app\common\model\PaymentAccount;
use app\common\model\Order as Model;
use app\common\controller\HomeBase;
use Exception;
use InvalidArgumentException;

class Order extends HomeBase
{

    protected $params;

    public function __construct()
    {
        $this->model = new PaymentAccount();
        $this->params = request()->param();
    }

    public function create()
    {
        $params = $this->params;
        $payType = $params['pay_type'];
        $payments= PaymentAccount::getShouPayments();
        if(!isset($payments[$payType])){
            return  fail('支付方式未配置');
        }
        $accountId = $payments[$payType][0]['id'];
        $account = (new PaymentAccount())->getItem(['id' => $accountId]);
        if (!$account) {
            return fail('服务器问题，请稍后再试');
        }
        $subject = $params['subject'];

        $outTradeNo = encrypt(date('YmdHis') . uniqid(),env('key'));

        //商品描述，可空
        $subject_num=isset($params['subject_num'])?$params['subject_num']:1;
        $subject_id = isset($params['subject_id'])?$params['subject_id']:0;
        if($subject=='RECHARGE'){
            $subject='积分充值';
            $price=sysconfig('score.score_price');
            $min_recharge=sysconfig('score.min_recharge');
            $max_recharge=sysconfig('score.max_recharge');
            if(!$price||!is_numeric($price)||$subject_num<$min_recharge||$subject_num>$max_recharge){
                return fail('系统未设置单位积分的价格，或数量不合规');
            }
            $amount=round($price*$subject_num,2);
        }else{
            return fail('订单项目代码不正确');
            $price=0;
        }


        $data['order_no'] = $outTradeNo;
        $data['subject'] = $subject;
        $data['subject_id'] = $subject_id;
        $data['subject_num'] =$subject_num;
        $data['price'] =$price;
        $data['user_id'] =cacheUserid();
        $data['user_ip'] =request()->ip();
        $data['amount'] = $amount;
//        $res = (new Model())->save($data);
        $data['pay_type']=$payType;
        $payTypeName=[
            'alipay'=>'支付宝',
            'wxpay'=>'微信支付',
            'qqpay'=>'微信支付',
        ];
        $data['account_id']=$accountId;
        $data['pay_type_name']=$payTypeName[$payType];
        $this->assign('order',$data);
        $this->assign('orderEncode',encrypt(json_encode($data),env('key')));

        return view();
    }


    public function wxJsPay()
    {
        return view();
    }

    public function aliJsPay()
    {
        return view();
    }

    public function query()
    {
        if (!request()->isPost()) {
            return json(array('code' => 1000, 'msg' => '必须是post请求'));
        }
        $params = $this->params;
        $orderNo = $params['orderid'];
        if (!$orderNo) {
            return json(array('code' => 1000, 'msg' => '丢失参数'));
        }
        $where['order_no'] = decrypt($orderNo,env('key'));;
        $order = (new \app\common\model\Pay())->where('create_time','>',todayTime())->where($where)->find();
        if (empty($order)) {
            $data = array('code' => 1002, 'msg' => '订单不存在(最近1天)'.lsql());
        } elseif ($order['status'] == 1) {
            $data = array('code' => 1, 'msg' => 'success', 'data' => array('redirect' => '/user/score/recharge_log'));
        } else {
            $data = array('code' => 1003, 'msg' => '未支付');
        }
        return json($data);
    }
}