<?php


namespace app\index\controller;


use app\common\controller\HomeBase;
use app\common\model\Nav;
use app\common\model\Page as Model;

class Page extends HomeBase
{
    public function initialize() {
        parent::initialize();
        $this->model=new Model();
    }
        public function detail(){
            $data=$this->params;
            if(isset($data['id'])){
                $where['id']=$data['id'];
            }else if(isset($data['alias'])){
                $where['alias']=$data['alias'];
            }
            $page=$this->model->where($where)->find();
            $this->assign('item', $page);
            $this->assign('alias',$page->alias);
            $this->assign('nav3',Nav::getItems(3));
            return view();
        }
}