<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\model\Article;
use app\common\model\ArticleAttribute;
use app\common\model\ArticleCategory;
use app\common\model\Comment;
use app\common\model\FriendlyLink;

class Index extends HomeBase
{
    public function initialize()
    {
        parent::initialize();
    }
    public function index()
    {
        $this->assign('URI','/');
        $this->assign('homepage_plate1',Article::homeCategory(1));
        $this->assign('homepage_plate2',Article::homeCategory(2));
        $this->assign('homepage_plate3',Article::homeCategory(3));
        //轮播
        $this->assign('carousel',Article::carouselArticle());
        //最近更新
        $this->assign('newArticles',Article::newArticle());
        //今日更新数量
        $this->assign('todayUpdateNum',Article::todayPublishCount());
        //周排行 月排行
        $categoryIds=sysconfig('article.homepage_plate_cates1');
        $hotLimit=sysconfig('article.homepage_hot_show');
        $this->assign('viewTopWeek',getJsonData('ArticleAttribute','viewtop',['categoryIds'=>$categoryIds,'field'=>'week_view','limit'=>$hotLimit],10));
        $this->assign('viewTopMonth',getJsonData('ArticleAttribute','viewtop',['categoryIds'=>$categoryIds,'field'=>'month_view','limit'=>$hotLimit],10));
        //友链数据
        $this->assign('friendlinks', (new FriendlyLink())->getItems(sysconfig('friendly_link.show_num')));
        return view();
    }

    /**
     */
    public function html($id)
    {

        $item=Article::getDetail($id);
        if(!$item){
            $this->error('文章不存在','','/');
        }
        $cateId=$item['category_id'];
        if($item['out_link']){
            //是广告判断要不要跳转
            $this->redirect(html_entity_decode($item['out_link']));
        }
        $nowHere=Article::now_here($cateId);
        $comments=Comment::getItems('list');
        $cate=Article::pidCates($cateId);
        $pidCateId=$cate?array_keys($cate)[0]:0;
        $this->assign('URI',Article::categoryUrl($pidCateId,$cate));
        //根据栏目ID显示其付栏目和本栏目
        $this->assign('categoryId',$cateId);
        $this->assign('pidCates',$cate);
        $this->assign('lift', ArticleAttribute::lift($id));
        $this->assign('comments', $comments);
        $this->assign('nowWhere', $nowHere);
        $this->assign('item', $item);
        $hotLimit=sysconfig('article.detail_hot_show');
        $this->assign('viewTopDay',getJsonData('ArticleAttribute','viewtop',['field'=>'day_view','limit'=>$hotLimit],6));
        $this->assign('viewTopWeek',getJsonData('ArticleAttribute','viewtop',['field'=>'week_view','limit'=>$hotLimit],30));
        $this->assign('viewTopMonth',getJsonData('ArticleAttribute','viewtop',['field'=>'month_view','limit'=>$hotLimit],60));
        return view();
    }
    public function list()
    {
        $params=$this->params;
        $page=isset($params['page'])?$params['page']:1;
        $category=null;
        if(isset($params['alias'])){
            $category=ArticleCategory::where('alias',$params['alias'])->find();
        }elseif(isset($params['id'])){
            $category=ArticleCategory::find($params['id']);
        }
        if(!$category){
            $this->error('列表不存在','','/');
        }
        $id=$category['id'];
        $count=getJsonData('ArticleCategory','articles_count',['id'=>$id],8);
        $result=getJsonData('ArticleCategory','articles',['id'=>$id,'page'=>$page],8);
        $this->assign('count', $count);
        $this->assign('list', $result['data']);
        $this->assign('pages', $result['pages']);
        //定位导航的Hover
        $categorys = getJsonData('ArticleCategory', 'list');
        $cate = array_column($categorys, 'alias', 'id');
        $cateParentId=$category['pid']?get_top_pid($id):$id;
        $this->assign('URI', Article::categoryUrl($cateParentId,$cate));
        $comments=Comment::getItems('list');
        //根据栏目ID显示其付栏目和本栏目
        $this->assign('categoryId',$id);
        $this->assign('pidCates',Article::pidCates($id));
        $this->assign('category', $category);
        $this->assign('comments', $comments);

        $hotLimit=sysconfig('article.detail_hot_show');
        $this->assign('viewTopDay',getJsonData('ArticleAttribute','viewtop',['field'=>'day_view','limit'=>$hotLimit],6));
        $this->assign('viewTopWeek',getJsonData('ArticleAttribute','viewtop',['field'=>'week_view','limit'=>$hotLimit],30));
        $this->assign('viewTopMonth',getJsonData('ArticleAttribute','viewtop',['field'=>'month_view','limit'=>$hotLimit],60));
        return view('list');
    }

    public function search($keyword = '')
    {
        $keyword = trim(remove_xss($keyword));
        if (!$keyword) {
            $this->redirect(url('index/tags/index'));
        }
        $page=isset($this->params['page'])?$this->params['page']:1;
        $this->assign('keyword', $keyword);

        $_list = (new Article())->alias('a')
            ->where('title', 'like',"%{$keyword}%")
            ->where('a.status', 1)
            ->order('a.sort', 'desc')
            ->order('a.id', 'desc')
            ->field('a.id,category_id,a.title,cover_img,description,publish_time')
            ->paginate(['list_rows'=>10,'query' => ['page'=>$page]]);
        $categorys = getJsonData('ArticleCategory', 'list');
        $cateTitle = array_column($categorys, 'title', 'id');
        $cate = array_column($categorys, 'alias', 'id');
        $list=$_list->toArray()['data'];
        foreach ($list as &$v) {
            $v['category']=isset($cateTitle[$v['category_id']])?$cateTitle[$v['category_id']]:'未分类';
            $v['url'] = Article::detailUrl($v['id'], $v['category_id'], $cate);
        }
        $count=(new Article())->alias('a')
            ->where('title', 'like',"%{$keyword}%")
            ->where('a.status', 1)->count('title');

        $this->assign('count', $count);
        $this->assign('list', $list);
        $this->assign('pages', $_list->render());
        $comments=Comment::getItems('list');
        $this->assign('comments', $comments);

        $hotLimit=sysconfig('article.detail_hot_show');
        $this->assign('viewTopDay',getJsonData('ArticleAttribute','viewtop',['field'=>'day_view','limit'=>$hotLimit],6));
        $this->assign('viewTopWeek',getJsonData('ArticleAttribute','viewtop',['field'=>'week_view','limit'=>$hotLimit],30));
        $this->assign('viewTopMonth',getJsonData('ArticleAttribute','viewtop',['field'=>'month_view','limit'=>$hotLimit],60));
        return view();
    }
}