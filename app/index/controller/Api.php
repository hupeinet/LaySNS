<?php
namespace app\index\controller;

class Api
{

    public function qrcode($str)
    {

        if (!$str) {
            echo '参数丢失';
            exit();
        }
        //增加安全判断
        if (isset($_SERVER['HTTP_REFERER'])) {
            $referer_url = parse_url($_SERVER['HTTP_REFERER']);
            if ($referer_url['host'] != $_SERVER['HTTP_HOST']) {
               dd('非法请求');
            }
        }
        try {
            \PHPQRCode\QRcode::png($str);
            exit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }


    //最近更新的文章列表


}