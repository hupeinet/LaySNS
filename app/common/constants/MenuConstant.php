<?php



namespace app\common\constants;

/**
 * 菜单常量
 * Class MenuConstant
 * @package app\common\constants
 */
class MenuConstant
{

    /**
     * 首页的PID
     */
    const HOME_PID = 9999;
    const ADDON_PID = 23;

    /**
     * 模块名前缀
     */
    const MODULE_PREFIX = 'easyadmin_';

}