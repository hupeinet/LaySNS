<?php


namespace app\common\service;


use anerg\OAuth2\OAuth;

class SnsOauthService
{

    public  static function auth($name){
        //获取配置
        $config = Sysconfig($name);

        $state = md5(uniqid(rand(), TRUE));
        session('oauth_state',$state);
        $config['state']=$state;
        //设置回跳地址
        $config['callback'] = self::makeCallback($name);

        //可以设置代理服务器，一般用于调试国外平台
//        $this->config['proxy'] = 'http://127.0.0.1:1080';

        /**
         * 对于微博，如果登录界面要适用于手机，则需要设定->setDisplay('mobile')
         *
         * 对于微信，如果是公众号登录，则需要设定->setDisplay('mobile')，否则是WEB网站扫码登录
         *
         * 其他登录渠道的这个设置没有任何影响，为了统一，可以都写上
         */

        $snsOauth=OAuth::$name($config);
//        if(request()->isMobile()){
//            $snsOauth->setDisplay('mobile');
//        }
        return redirect($snsOauth->getRedirectUrl());
    }


    /**
     * 生成回跳地址
     *
     * @return string
     */
    public static function makeCallback($name)
    {
        //注意需要生成完整的带http的地址
        return DOMAIN_URL.'/callback/' . $name.'.html';
    }

}