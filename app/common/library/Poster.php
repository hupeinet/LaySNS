<?php


namespace app\common\library;


class Poster
{
    public $imageRes = null;
    public $totalWidth = 580;
    public $totalHeight = 960;
    public $fontColor = null;
    /**
     * 初始化海报
     * @param int $w 海报宽度
     * @param int $h 海报高度
     * @param string $c 海报背景色 RGB 如"255,255,255";
     */
    public function __construct($w=580,$h=960,$c="255,255,255")
    {
        $imageRes = imageCreatetruecolor($w,$h);
        list($R,$G,$B) = explode(',', $c);
        $color = imagecolorallocate($imageRes, $R, $G, $B);
        imagefill($imageRes,0,0,$color);
        $this->totalWidth = $w;
        $this->totalHeight = $h;
        $this->imageRes = $imageRes;
        $this->fontFile = __DIR__ . '/vendor/topthink/think-captcha/assets/zhttfs/1.ttf';
    }

    /**
     * 设置字体
     *
     * @param string $fontPath 字体文件绝对路径
     * @return void
     */
    public function setFont(string $fontPath)
    {
        if(file_exists($fontPath)){
            $this->fontFile = $fontPath;
        }
    }
    /**
     * 绘制文字，单行文字，超出自动截断
     * @param string $text 要绘制的文字
     * @param float $size 文字大小
     * @param int $left 起始位置的X坐标
     * @param int $top 起始位置的Y坐标
     * @param string $color RGB颜色代码逗号隔开，不要空格
     * @param int $maxRight 文字最大X坐标
     */
    public function drawSingleLineText($text,$size,$left,$top,$color="0,0,0",$maxRight=0)
    {
        $width = $this->getTextWidth($text,$size);
        list($R,$G,$B) = explode(',', $color);
        $color = imagecolorallocate($this->imageRes, $R, $G, $B);
        $maxRight = $maxRight?:$this->totalWidth;
        if($width + $left < $maxRight){
            imagettftext($this->imageRes,$size,0,$left,$top+$size,$color,$this->fontFile,$text);
        }else{
            $textArray = $this->stringToArray($text);
            $text="";
            for ($i=0; $i < count($textArray); $i++) {
                $temp_text = $text.$textArray[$i]."...";
                $textwidth = $this->getTextWidth($temp_text,$size);
                if($textwidth + $left < $maxRight){
                    $text .=$textArray[$i];
                }else{
                    break;
                }
            }
            imagettftext($this->imageRes,$size,0,$left,$top+$size,$this->fontColor,$this->fontFile,$text."...");
        }
    }
    /**
     * 绘制多行文本
     *
     * @param string $text 不带换行的纯文本，会自动换行，截取
     * @param float $size 文字大小
     * @param array $startPosition 起始位置坐标[0,0]
     * @param array $drawRect 绘制区域宽高[300,200]
     * @return void
     */
    public function drawMultiLineText(string $text,float $size,array $startPosition,array $drawRect)
    {
        #好麻烦，懒得写了
    }
    /**
     * 绘制图片到海报上
     *
     * @param string $url 图片的URL 如 http://a.com/b.png
     * @param integer $left 起始位置的X坐标
     * @param integer $top 起始位置的Y坐标
     * @param integer $width 缩放后的宽度
     * @param integer $height 缩放后的高度
     * @return void
     */
    public function drawImage(string $url,int $left,int $top, int $width,int $height)
    {
        $info = getimagesize($url);
        $function = 'imagecreatefrom'.image_type_to_extension($info[2], false);
        $res = $function($url);
        $resWidth = $info[0];
        $resHeight = $info[1];
        //建立画板 ，缩放图片至指定尺寸
        $canvas=imagecreatetruecolor($width, $height);
        $color = imagecolorallocate($canvas, 255, 255, 255);
        imagefill($canvas, 0, 0, $color);
        $x=0;$y=0;$max = $resWidth;
        if($resWidth < $resHeight){
            $max = $resWidth;
            $y = ceil(($resHeight - $resWidth) / 2);
        }else if($resWidth > $resHeight){
            $max = $resHeight;
            $x = ceil(($resWidth - $resHeight) /2);
        }
        //关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
        imagecopyresampled($canvas, $res, 0, 0, $x, $y,$width,$height, $max, $max);
        //放置图像
        imagecopymerge($this->imageRes,$canvas, $left,$top,0,0,$width,$height,100);//左，上，右，下，宽度，高度，透明度
    }

    /**
     * 保存图片到指定位置
     *
     * @param string $filePath 要保持的路径（绝对路径）
     * @return void
     */
    public function savePng(string $filePath)
    {
        if(file_exists($filePath))unlink($filePath);
        imagepng ($this->imageRes,$filePath,5);
    }
    /**
     * =============================
     *         辅助私有方法
     * =============================
     */

    /**
     * 获取文本绘制后的宽度
     *
     * @param string $text
     * @param float $size
     * @return int
     */
    private function getTextWidth(string $text,float $size):int
    {
        $rect = imagettfbbox ( $size, 0, $this->fontFile, $text );
        return $rect[4] - $rect[6];
    }
    /**
     * 字符串转化为逐字数组
     *
     * @param string $str 要转化的字符串
     * @return array
     */
    private function stringToArray(string $str):array
    {
        $charset = "utf8";
        $strlen=mb_strlen($str);
        while($strlen){
            $array[]=mb_substr($str,0,1,$charset);
            $str=mb_substr($str,1,$strlen,$charset);
            $strlen=mb_strlen($str);
        }
        return $array;
    }
}