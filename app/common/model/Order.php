<?php
namespace app\common\model;



use app\user\model\UserRichesLog;

class Order extends TimeModel
{

    public function setUserIpAttr($value){
        return ip2long($value);
    }

    public function getUserIpAttr($value){
        return long2ip($value);
    }
    public function getPayStatusAttr($value){
        $arr=[
            0=>'未支付',
            1=>'已支付',
            -1=>'已退款',
        ];
        return $arr[$value];
    }
    static function checkAuth(){
        //检测是不是过期了
        //检测
        return true;
    }

    public static function note($params)
    {

        $uid = cacheUserid();

        $payAmount = $params['amount'];
        $paymentId=$params['account_id'];
        $outTradeNo=$params['order_no'];
        //创建一个
        $insertData['user_id'] = $uid;
        $insertData['price'] = $params['price'];
        $insertData['amount'] = $payAmount;
        $insertData['user_ip'] =  $params['user_ip'];
        $insertData['order_no'] = $outTradeNo;
        $insertData['subject'] = ($params['subject']=='积分充值')?'DEFAULT':$params['subject'];
        $insertData['subject_num'] = $params['subject_num'];
        $insertData['subject_id'] = $params['subject_id'];
        (new static())->save($insertData);


        $data['order_no'] = $outTradeNo;
        $data['subject'] = $params['subject'];
        $data['body'] =  isset($params['body'])?$params['body']:$params['subject'];
        $data['pay_type'] = $params['pay_type'];
        $data['payment_account_id'] = $paymentId;
        $data['money'] = $payAmount;
        $data['user_ip'] = request()->ip();
        (new Pay())->save($data);
        (new PaymentAccount())->where('id',$paymentId)->inc('use_count')->update();
    }

    static function processing($orderId,$payStatus=1,$hand=0){
        $order=static::where('order_no',$orderId)->find();
            if($order&&$order['deal_status']==0){
                $order->pay_status=$payStatus;
                $order->is_hand=$hand;
                $order->deal_status=1;
                $order->save();
                if($order['subject_id']==0){
                   UserRichesLog::note($order['subject_num'], $order['user_id'], '积分充值', 1,$order['id'],'ORDER');
                }elseif($order['subject_id']>0){

                }
            }
    }
}