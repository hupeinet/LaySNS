<?php
namespace app\common\model;

class Page extends TimeModel
{
    public function getContentAttr($value)
    {
        return htmlspecialchars_decode($value);
    }
}