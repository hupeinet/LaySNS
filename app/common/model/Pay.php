<?php
namespace app\common\model;

class Pay extends TimeModel
{

    public function setUserIpAttr($value){
        return ip2long($value);
    }

    public function getUserIpAttr($value){
        return long2ip($value);
    }

}