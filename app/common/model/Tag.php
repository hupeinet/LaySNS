<?php
namespace app\common\model;
use app\user\model\User;
use app\common\model\Article;

class Tag extends TimeModel
{

    static function note($tag)
    {
        $ip2long=ip2long(request()->ip());
        $data = [
            'tag' => $tag,
            'ip2long' =>$ip2long
        ];
        //查询有没有
        $tagArticles=(new Article())
            ->where('keywords' ,'like','%'.$tag.'%')
            ->where('status' ,1)
            ->count('status');
        if(!$tagArticles){
            return;
        }
        $count=(new TagClick())
            ->where('create_time' ,'>', todayTime())
            ->where($data)
            ->count('tag');
        if (!$count) {
            (new TagClick())->save($data);
        }
    }

    public function getJsonData($name,$param=[])
    {
        $page=1;
        $limit=20;
        if(isset($param['page'])){
            $page= $param['page'];
        }
        if(isset($param['limit'])){
            $limit= $param['limit'];
        }
        switch ($name){
            case 'list':
                $keyword=$param['keyword'];
                $_list = (new Article())
                    ->where("status", 1)
                    ->where('keywords', 'like', '%' . $keyword . '%')
                    ->order('id', 'desc')
                    ->field('id,keywords')
                    ->paginate(['list_rows'=>10,'query' => ['page'=>$page,'keyword'=>$keyword]]);
                $pages = $_list->render();

                $list=$_list->toArray()['data'];
                $aids=array_column($list,'id');
                $articles=(new Article())->whereIn('id',$aids)
                    ->field('id,category_id,user_id,title,keywords,description,publish_time')
                    ->select()->toArray();
                $uids=array_unique(array_column($articles,'user_id'));

                $users=(new User())->whereIn('id',$uids)->column('id,nickname');

                $categorys = getJsonData('ArticleCategory', 'list');
                $cateTitle = array_column($categorys, 'title', 'id');
                $cate = array_column($categorys, 'alias', 'id');
                foreach($articles as &$v){
                    $v['url']=Article::detailUrl($v['id'],$v['category_id'],$cate);
                    $v['category']=isset($cateTitle[$v['category_id']])?$cateTitle[$v['category_id']]:'';
                    $v['nickname']=isset($users[$v['user_id']])?$users[$v['user_id']]:'';
                }
                return ['pages'=>$pages,'data'=>$articles];
            case 'count':
                $keyword=$param['keyword'];
                $count = (new Article())
                    ->where("status", 1)
                    ->where('keywords', 'like', '%' . $keyword . '%')
                    ->count('status');
                return $count;
            case 'hot':
                $list=static::hot();
                return $list;
            case 'hotCount':
                $list=static::hot(1);
                return $list;
                break;
        }
    }

    private static function hot($count=0)
    {

        $tagClicks= $systemTags = [];
        $systemTagArr=(New static())
            ->where('status',1)
            ->field('tag,clicks')
            ->select()
            ->toArray();
        if(!empty($systemTagArr)){
            $systemTags=array_column($systemTagArr,'tag');
        }
        $config=sysconfig('article');
        $tag_hot_show=$config['tag_hot_show'];
        if($tag_hot_show>0) {
            $tagClicks = (New TagClick())
                ->whereNotIn('tag', $systemTags)
                ->field('tag,sum(click) as clicks')
                ->group('tag')
                ->order('clicks desc')
                ->limit($tag_hot_show)
                ->select()->toArray();
        }
        $arr=array_merge($systemTagArr,$tagClicks);
        if($count) {
            $quchong = [];
            foreach ($arr as $k => &$v) {
                $tag = $v['tag'];
                if (!in_array($tag, $quchong)) {
                    array_push($quchong, $tag);
                    $v['count'] = (new Article())
                        ->where("status", 1)->where('keywords', 'like', '%' . $v['tag'] . '%')
                        ->count('status');
                } else {
                    unset($arr[$k]);
                }
            }
        }
        return $arr;
    }

    static function clearTag($tag)
    {
        (new TagClick())->where('tag', $tag)->delete();
    }
}