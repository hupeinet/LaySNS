<?php
namespace app\common\model;

use app\common\model\SeoLog as Model;
use seo\Baidu;

class SeoLog extends TimeModel
{

    static function pushToBaidu($type,$increment=0){

        $baiduApi = htmlspecialchars_decode(sysconfig('seo.baidu_api'));
        $arr = parse_url($baiduApi);
        $arr_query = convertUrlQuery($arr['query']);
        $baiduZyDomain = $arr_query['site'];
        $site_url = domain_url('');
        if (strpos($baiduZyDomain,$_SERVER['HTTP_HOST'])===false) {
            return fail('API地址中的域名是：' . $baiduZyDomain . '，本服务器的域名是' . $_SERVER['HTTP_HOST']);
        }

        $urls = $data = [];
        $categorys = getJsonData('ArticleCategory', 'list');
        if (!$categorys) {
            return fail('文章栏目为空，如果栏目已更新，请点击右上角【清理缓存】');
        }
        $cate = array_column($categorys, 'alias', 'id');
        $batchNum = date('YmdHis');
        switch ($type) {
            case 'today':
                $ids = (new Article())->where('publish_time', '>=', todayTime())
                    ->where('status', 1)
                    ->where('description', '<>', '')
                    ->column('id,title,category_id');
                if (!$ids) {
                    return fail('今日新增文章为空');
                }

                foreach ($ids as $k => $v) {
                    $url = Article::detailUrl($v['id'],$v['category_id'],$cate);
                    $urls[] = $site_url.$url;
                    $data[] = [
                        'url' => $site_url.$url,
                        'batch_id' => $batchNum,
                        'subject_id' => $v['id'],
                        'subject' => 'ARTICLE',
                        'subject_title' => $v['title'],
                    ];
                }
                break;
            case 'sevenDays':
                $ids = (new Article())->where('publish_time', '>=', todayTime(-7))
                    ->where('status', 1)
                    ->where('description', '<>', '')
                    ->column('id,title,category_id');
                if (!$ids) {
                    return fail('七天内新增文章为空');
                }

                foreach ($ids as $k => $v) {
                    $url = Article::detailUrl($v['id'],$v['category_id'],$cate);
                    $urls[] = $site_url.$url;
                    $data[] = [
                        'url' => $site_url.$url,
                        'batch_id' => $batchNum,
                        'subject' => 'ARTICLE',
                        'subject_id' => $v['id'],
                        'subject_title' => $v['title'],
                    ];
                }
                break;
            case 'cates':

                foreach ($categorys as $k => $v) {

                    $url = Article::categoryUrl($v['id'],$cate);
                    $urls[] = $site_url.$url;
                    $data[] = [
                        'url' => $site_url.$url,
                        'batch_id' => $batchNum,
                        'subject' => 'ART_CATEGORY',
                        'subject_id' => $v['id'],
                        'subject_title' => '[栏目] '.$v['title'],
                    ];
                }
                break;
            case 'sysTags':
                $list=Tag::column('tag');
                if(empty($list)){
                    return fail('未设置系统标签或关键词');
                }
                foreach ($list as $v) {
                    $url = '/tag/'.$v.'.html';
                    $urls[] = $site_url.$url;
                    $data[] = [
                        'url' => $site_url.$url,
                        'batch_id' => $batchNum,
                        'subject' => 'TAG',
                        'subject_id' => 0,
                        'subject_title' => '[TAG] '.$v,
                    ];
                }
                break;
            case 'hotTags':
                $list=getJsonData('Tag','hot',[],28);
                if(empty($list)){
                    return fail('暂时没有热门标签');
                }
                foreach ($list as $v) {
                    $url = '/tag/'.$v['tag'].'.html';
                    $urls[] = $site_url.$url;
                    $data[] = [
                        'url' => $site_url.$url,
                        'batch_id' => $batchNum,
                        'subject' => 'HOT_TAG',
                        'subject_id' => 0,
                        'subject_title' => '[热门标签] '.$v['tag'],
                    ];
                }
                break;
        }

        $res = Baidu::push($urls, $baiduApi);
        $arr = json_decode($res, true);
        if (isset($arr['success']) && $arr['success'] > 0) {
            (new Model())->saveAll($data);
            return success('推送成功，本次推送'.count($urls).'条，额度剩余'.$arr['remain'].'条', $arr);
        } else {
            return fail($res);
        }
    }

}