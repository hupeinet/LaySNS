<?php
namespace app\common\model;


class Message extends TimeModel
{
    public static function note($title,$content,$uid=0,$toUid=0){
        static::create([
                'title'=>$title,
                'content'=>$content,
                'user_id'=>$uid,
                'to_uid'=>$toUid
        ]);
    }
}