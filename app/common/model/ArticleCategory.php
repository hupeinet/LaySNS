<?php
namespace app\common\model;


use think\facade\Cache;


class ArticleCategory extends TimeModel
{
    static function onAfterInsert()
    {
        $file_path = runtime_path() . '../'.env('KEY') . DS;
        @unlink($file_path.'ArticleCategory-list.json');
    }

    static function onAfterUpdate()
    {
        $file_path = runtime_path() . '../'.env('KEY') . DS;
        @unlink($file_path.'ArticleCategory-list.json');
    }
    static function onAfterDelete()
    {
        $cacheNames = ['nav_1'];
        foreach ($cacheNames as $cacheName) {
            if (Cache::has($cacheName)) {
                Cache::delete($cacheName);
            }
        }
    }
    public function getJsonData($name,$where=[]){
        //最新数据
        switch ($name){
            case 'list':
                $map=[];
                if(isset($where['categoryIds'])){
                    $map[]=['category_id','in',$where['categoryIds']];
                }
                $list = $this
                    ->where($map)
                    ->where('status',1)
                    ->order('sort', 'desc')
                    ->column('id,title,alias');
                return $list;
                break;
            case 'tree':
                return $this->tree();
                break;
            case 'cannot_contribute':
                return $this->cannot_contribute();
                break;
            case 'articles_count':
                $cateId=$where['id'];
                $count = (new Article())
                    ->where('category_id', $cateId)
                    ->where("status", 1)
                    ->count('status');
                return $count;
                break;
            case 'articles':
                return self::getArticles($where);
                break;
        }
    }

    static function getItemById($cateId,$field=''){

        $item=static::find($cateId);
        if($item&&$field){
            return @$item->$field;
        }
        return $item;
    }

    static function getArticles($params)
    {
           $page=1;
            if(isset($params['page'])){
                $page= $params['page'];
            }
            $categorys = getJsonData('ArticleCategory', 'list');
            $cateTitle = array_column($categorys, 'title', 'id');
            $cate = array_column($categorys, 'alias', 'id');
            $cateId=$params['id'];
            $_list = (new Article())
                ->where('category_id', $cateId)
                ->where('status', 1)
                ->order('is_top', 'desc')
                ->order('is_ad', 'desc')
                ->order('sort', 'desc')
                ->order('publish_time', 'desc')
                ->field('id,is_ad,is_top,category_id,title,cover_img,description,create_time,publish_time')
                ->paginate(['list_rows'=>10,'query' => ['page'=>$page]]);
            $list=$_list->toArray()['data'];
            foreach ($list as &$v) {
                $v['category']=isset($cateTitle[$v['category_id']])?$cateTitle[$v['category_id']]:'未分类';
                $v['url'] = Article::detailUrl($v['id'], $v['category_id'], $cate);
            }
        return ['pages'=>$_list->render(),'data'=>$list];
    }

    static function findChildIds($parentId = '0', &$childIds = '') {
        $data = (new static())->order('sort desc,parent_id asc')->select()->toArray();
        foreach ((array) $data as $value) {
            if ($value['parent_id'] == $parentId) {
                $childIds .= ',' . $value['id'];
                static::findChildIds($value['id'], $childIds);
            }
        }
        return $childIds;
    }

    public function cannot_contribute(){
        $cannot_contribute=sysconfig('article.cannot_contribute');
        $map=[];
        if($cannot_contribute){
            $condition[]=['id','not_in',explode(',',$cannot_contribute)];
        }
        return $this->tree($map);
    }

    public static function getPidCateList(){
          $list = (new static())->field('id,pid,title')
            ->select()
            ->toArray();
        $pidList = buildPidTree(0, $list);
        $pidList = array_merge([[
            'id'    => 0,
            'pid'   => 0,
            'title' => '顶级栏目',
        ]], $pidList);
        return $pidList;
    }

    public function tree($map=[])
    {
        $list = (new ArticleCategory())->field('id,pid,title')
            ->where('status', 1)
            ->select()
            ->toArray();
        return  buildPidTree(0, $list);
    }
}