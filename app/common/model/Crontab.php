<?php
namespace app\common\model;



class Crontab extends TimeModel
{

    public function getCycle($item){

        $arr=[
          'month'=>'每月',
          'day'=>'每天',
          'week'=>'每星期',
          'hour'=>'每小时',
          'minute'=>'分钟',
          'hour-n'=>'小时',
          'minute-n'=>'分钟',
          'day-n'=>'天',
        ];
        $weekCn=['日','一','二','三','四','五','六'];
        $sType=$item['type'];
        $cycle=isset($arr[$sType])?$arr[$sType]:'';
        $where1=$item['where1'];
        $week=$item['week'];
        $hour=$item['hour'];
        $minute=$item['minute'];
        if(strpos($sType,"-n")!==false){
            $cycle='每隔'.$where1.$cycle;
            $cycleStr=1;
        }else{
            $cycleStr=$cycle.($where1!=''?($where1.'日 '):'').($week!=''?$weekCn[$week]:'');
        }
        $cycleStr.=($hour?:$hour.'点').($minute?:$minute.'分');
        return   [
            'cycle'=>$cycle,
            'cycle_str'=>$cycleStr,
        ];
    }

}