<?php
namespace app\common\model;

use org\Http;
use app\admin\model\CollectLog;
use think\Model;

class CollectCategory extends TimeModel
{
    public static function getCategoryId($category,$subject='ARTICLE'){
        if(!$category) return 0;
        $where['subject']=$subject;
        $where['title']=$category;
        $find=(new static())->where($where)
            ->find();
        if($find) return $find->category_id;
        (new static())->save($where); return 0;
    }
}