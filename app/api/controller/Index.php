<?php
namespace app\api\controller;
use app\common\controller\HomeBase;
use app\common\model\Article;
use app\common\model\ArticleAttribute;
use addons\attachlink\library\lanzou;
use app\common\model\Tag;
use freeApi\api;
use think\App;


class Index extends HomeBase
{
    public function initialize()
    {

        parent::initialize();

    }

    function ajaxReturn($array){
        $content=json_encode($array);
        if(empty($_GET['callback'])){
            echo $content;exit;
        }else{
            echo $_GET['callback']."(".$content.")";exit;
        }
    }
    public function index()
    {

//        $url = isset($_GET['url']) ? $_GET['url'] : "";
//        $pwd = isset($_GET['pwd']) ? $_GET['pwd'] : "";
//        $type = isset($_GET['type']) ? $_GET['type'] : "";
//
//        $lz = new lanzou();
//        $res=$lz->getUrl($url,$pwd);
//       dd($res);

dd(api::kuaidi('552041672012900'));




//        $rule_arr = explode("\n", $config['linkanalyze']);
//        $wp_arr=[];
//        for ($i = 0; $i < count($rule_arr); $i++) {
//            $arr0 = explode('|', $rule_arr[$i]);
//            $wparr[$i] = $arr0[1];
//            $wp_arr[$arr0[1]]['name'] = $arr0[0];
//            $wp_arr[$arr0[1]]['url'] = $arr0[1];
//            $wp_arr[$arr0[1]]['img'] = $arr0[2];
//        }
//
//
//        $keys =[
//            '房'=>"fang",
//            '厅'=>"ting",
//            '卫'=>"wei",
//            '梯'=>"ti",
//            '户'=>"hu",
//        ];
//        $keyString = "2房13户6梯4厅";
//        $reg = "/房|户|厅|卫|梯/";  // 关键字正则字符串
//        preg_match_all($reg,$keyString,$m,PREG_OFFSET_CAPTURE);
//        $location = array();
//        foreach ($m[0] as $key=>$value){
//            if ($key > 0){
//                $start = $m[0][$key-1][1]+3;
//                $length = $value[1]-($m[0][$key-1][1]+3);
//                $location[$keys[$value[0]]] =  substr($keyString,$start,$length);
//            }else{
//                $location[$keys[$value[0]]] =  substr($keyString,0,$value[1]);
//            }
//        }
//
//        print_r($location);
    }

    /**
     * 获取分类接口
     */
    public function getCategory($type = '')
    {
        return json(['code' => 1, 'data' => getJsonData('ArticleCategory', 'list')]);
    }

    function getTagSelect(){
        $systemKeywords=(new Tag())->where('status',1)->column('tag as title,tag as id');

        return json(['code'=>1,'data'=>$systemKeywords]);
    }

    /**
     * 获取分类接口
     */
    public function getCategoryTree()
    {
        return json(['code' => 1, 'data' => getJsonData('ArticleCategory', 'tree')]);
    }

    public function showqr()
    {
        $url = input('url');
        if ($url) {
            //增加安全判断
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer_url = parse_url($_SERVER['HTTP_REFERER']);
                $web_url = $_SERVER['HTTP_HOST'];
                if ($referer_url['host'] != $web_url) {
                    $img = APP_PATH . '/static/images/pay/weburl-error.png';
                    @header("Content-Type:image/png");
                    echo file_get_contents($img);
                    exit();
                }
            }
            try {
                \PHPQRCode\QRcode::png($url);
                exit();
            } catch (\Exception $e) {
                echo $e->getMessage();
                exit();
            }
        } else {
            echo '参数丢失';
            exit();
        }
    }

    public function poster()
    {
        $params=$this->params;
        $imgurl=$params['img'];
        if($imgurl&&strpos($imgurl,'http')===false){
            $imgurl=domain_url($imgurl);
        }
        $logo_image=sysconfig('site.logo_image');
        return json([
            'head' => ($imgurl&&uri_valid($imgurl))?$imgurl:'/static/common/images/leisen.png',
            'logo' => $logo_image?$logo_image:'/static/common/images/logo.png',
            'excerpt' => $params['desc'],
            'timestamp' => time(),
            'title' => $params['title'],
        ]);
    }
    public function tagclick(){
        $params=$this->params;
        Tag::note($params['tag']);
    }
    public function add_view()
    {
        $only=['aid', 'mid'];
        $rule = [
            'aid|文章ID'       => 'require|number',
//            'mid|MID'       => 'require|number:1,2'
        ];
        $data=validate_ext($only,$rule,0,[],0);
        if(isset($data['code'])&&$data['code']===0){
            return $data['msg'];
        }

        $article=(new Article())->field('view,day_view,week_view,month_view,comment_num,praise_num')->find($data['aid']);

        (new Article())->where('id',$data['aid'])->update([
            'view'=>$article->view+1,
            'day_view'=>$article->day_view+1,
            'week_view'=>$article->week_view+1,
            'month_view'=>$article->month_view+1
        ]);

        return success('success',[
            'view'=>$article->view+1,
            'comment_num'=>$article->comment_num,
            'praise_num'=>$article->praise_num
        ]);

    }

}
