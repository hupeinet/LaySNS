<?php
namespace app\user\controller;
use app\common\controller\HomeBase;

use app\common\model\PaymentAccount;
use think\captcha\facade\Captcha;


class Score extends HomeBase
{
    protected $model;


    public function index()
    {

        return view();
    }

    public function recharge()
    {
        if(!cacheUserid()){
            $this->error('你没有登录', [],'user/login/index');
        }
        if(isset($_SERVER["HTTP_REFERER"])){
            session('login_redirect',$_SERVER["HTTP_REFERER"]);
        }
        try{
            $list=PaymentAccount::getShouPayments();
        } catch (\Exception $e) {
            $this->error('系统没有配置支付方式', [],'user/index/index');
        }
        $this->assign('paymentList', $list);
        //如果在微信客户端那么用微信登录
        $config=sysconfig('score');
        $this->assign('scoreConfig', $config);
        return view();
    }



    public function recharge_log()
    {

        return view();
    }

}
