<?php


namespace app\user\controller;
use app\common\model\Article;
use app\common\model\ArticleCategory;
use app\user\model\User;

use app\common\controller\HomeBase;
use app\user\model\UserAttribute;
use app\user\model\UserOauth;

class Index extends HomeBase
{
    protected $uid;
    public function initialize()
    {
        $this->uid=cacheUserid();
        if(!$this->uid){
            $this->error('请先登录',[],'user/login/index');
        }

        parent::initialize();
        $this->model = new User();
    }
    public function index(){
        $info=(new User())->find($this->uid);
        $att=$info->attribute->toArray();
        $this->assign('user',array_merge($att,$info->toArray()));
        return view();
    }
    public function edit(){
        $info=(new User())->find($this->uid);
        $this->assign('user',$info);
        return view();
    }
    public function contribute(){
        //是否有投稿奖励
        $suijima=create_random_str(12);
        session('tg_suijima',$suijima);
        $articleConfig=sysconfig('article');
        $this->assign('articleConfig',$articleConfig);
        $this->assign('category', getJsonData('ArticleCategory','cannot_contribute'));
        return view();
    }

    public function contributes(){
        //统计用户总共投稿的
        $item=(new Article())->where('user_id',$this->uid)
            ->field('count(*) as count,sum(if (status = 1,1,0)) as passed_num,sum(if (status = 2,1,0)) as faild_num,sum(if (status = 0,1,0)) as wait_check')
            ->find()->toArray();

            $this->assign('total',$item);
        return view();
    }


    public function tg_log(){
        return view();
    }

    public function favorites(){
        return view();
    }
    public function score(){
        return view();
    }

    public function comments(){
        return view();
    }
    public function change_password(){
        $user=$this->model->find($this->uid);
        $this->assign('user',$user);
        return view();
    }

    public function bind(){
        $uid=cacheUserid();
        if(!$uid){
            $this->loginError();
        }
        $apploginName=[
            'qq'=>'QQ登录',
            'wechart'=>'微信登录',
            'weixin'=>'微信登录',
            'alipay'=>'支付宝登录',
            'dingtalk'=>'钉钉登录'
        ];
        //查询系统开通的绑定
        $appLoginArr=sysconfig('applogin.login_types');

        //查询已经绑定的快速登录方式
        $list=UserOauth::where('user_id',$uid)->column('nick','type');
        $bindStatus=0;

        $userInfo=(new User())->find($uid);
        if($userInfo->email){
            $bindStatus+=1;
        }
        if($userInfo->mobile){
            $bindStatus+=2;
        }

        $this->assign('bindStatus',$bindStatus);
        $this->assign('apploginName',$apploginName);
        $this->assign('appLoginArr',$appLoginArr);
        $this->assign('userInfo',$userInfo);
        $this->assign('list', $list);
        return view();
    }

    public function trim()
    {
        return view();
    }
}