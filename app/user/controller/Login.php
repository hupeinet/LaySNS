<?php
namespace app\user\controller;
use app\common\controller\HomeBase;

use think\captcha\facade\Captcha;


class Login extends HomeBase
{
    protected $model;
//    public function initialize()
//    {
//        parent::initialize(); $redirect=session('login_redirect')?session('login_redirect'):url('user/index/index');
//
//        $this->model = new User();
//    }
    //普通登录
    public function index()
    {
        if(cacheUserid()){
            $this->error('你已经登录过了', [],'user/index/index');
        }
        if(isset($_SERVER["HTTP_REFERER"])){
            session('login_redirect',$_SERVER["HTTP_REFERER"]);
        }
        //如果在微信客户端那么用微信登录


        return view();
    }

    //忘记密码
    public function forget()
    {
        if (session('userid')) {
            $this->error('你已经登录过了', [],'user/index/index');
        }
        $data = $this->params;
        $forget['step'] = 1;
        if (isset($data['key'])) {
            $jiemi=decrypt($data['key']);
            $forget = json_decode($jiemi, true);
            if(!$forget){
                dd($data['key']);
                $this->error('KEY不正确，非法操作', [],'user/login/forget');
            }

        }
        $this->assign('forget', $forget);
        return view();
    }
    //验证码登录
    public function code_login()
    {
        $code='';
        if(cookie('pid')){
            $code=cookie('pid');
        }
        $this->assign('code', $code);
        return view();
    }
    //注册
    public function register()
    {
        if (session('userid')) {
            $this->error('你已经登录过了', [],'user/index/index');
        }

        $switch=sysconfig('switch.register');
        if (!$switch) {
            $msg=sysconfig('switch.register_close_msg');
            $this->error($msg?:'系统已经关闭注册', [],'index/index/index');
        }

        $code='';
        if(cookie('pid')){
            $code=cookie('pid');
        }
        $this->assign('code', $code);
        $this->assign('title', '用户注册');
        return view();
    }

    public function captcha()
    {
        return Captcha::create();
    }
}
