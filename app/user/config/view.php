<?php
$use=sysconfig('template.use');
$theme = $use?$use['user']:'default';
if(request()->isMobile() && file_exists(ROOT_PATH."public/template/user/{$theme}/mobile")) {
    $themeTitle = "mobile";
} else {
    $themeTitle = "pc";
}
return [
    // 模板后缀
    'view_suffix'  => 'html',
    // 模板路径
    'view_path'    => 'template/user/' .$theme.'/'.$themeTitle.'/',
    'view_depr'    => '_',
    // 视图输出字符串内容替换
    'tpl_replace_string'       => [
        '__HOME__' =>  '/template/user/' .$theme.'/'.$themeTitle,
        '__COMMON__' =>  '/template/common',
        '__STATIC__' =>  '/static',
    ],
];