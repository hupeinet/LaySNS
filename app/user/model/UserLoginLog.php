<?php
namespace app\user\model;

use app\common\model\TimeModel;
use org\Http;

class UserLoginLog extends TimeModel
{
    protected $updateTime = false;


    public static function getTodayErrorCount(){
        return static::where('create_time','>',strtotime(date('Y-m-d')))
            ->where('code',0)->where('subject','LOGIN')->count();
    }

}