<?php
namespace app\user\model;
use app\common\model\TimeModel;

class UserRichesLog extends TimeModel
{

    public static function note($riches, $uid, $content, $isAdd = 0, $objectId = 0, $object = '',$richesType='SCORE')
    {
            $user=(new User())->find($uid);
            $userRichesType=strtolower($richesType);
            $balance = $user[$userRichesType];

            if (!$uid||!$user||$riches <= 0) {
                return array('code' => -1, 'msg' => '失败', 'data' => []);
            }
            //先查询原来多少
            if ($isAdd == 0&&($balance == 0 || $balance < $riches)) {
                return array('code' => -1, 'msg' => '失败', 'data' => []);
            }

            $data['is_add'] = $isAdd;
            $data['user_id'] = $uid;
            $data['subject'] = $object;
            $data['subject_id'] = $objectId;
            $data['content'] = $content;
            $data['riches'] = $riches;
            $data['riches_type'] = $richesType;

            if ($isAdd) {
                $data['balance']=$balance+$riches;
            } else {
                $data['balance']=$balance-$riches;
            }
            $user->$userRichesType=$data['balance'];
            (new static())->save($data);
            $user->save();
          return array('code' => 1, 'msg' => '成功');
    }

}