<?php
namespace app\user\model;

use app\common\model\TimeModel;
use app\common\model\Article;

class UserFavorite extends TimeModel
{
    public function setSubjectIdAttr($value, $data){
        $article=(new Article())->find($data['subject_id']);
        $this->set('subject_id',$value);
        $this->set('subject_title',$article->title);
    }
    static function note($uid,$aid)
    {
        $data = [
            'user_id' => $uid,
            'subject_id' => $aid,
        ];
        $count=(new static())
            ->where($data)
            ->count();
        if (!$count) {
            (new static())->save($data);
            return true;
        }else{
            return ['msg'=>'已收藏过','code'=>0,'data'=>[]];
        }
    }

}