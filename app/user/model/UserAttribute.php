<?php
namespace app\user\model;
use app\common\model\TimeModel;
use think\facade\Cache;

class UserAttribute extends TimeModel
{
    static function onAfterUpdate(){
        $uid=session('userid');
        if($uid) {
            Cache::delete('user_'.$uid);
        }
    }
    public static function createAttribute($userId,$data){
        $promo_code='';
        if(isset($data['promo_code'])) {
            $promo_code=remove_xss($data['promo_code']);
        }else if(cookie('promo_code')) {
            $promo_code=cookie('promo_code');
        }
        $saveData['user_id']=$userId;
        $saveData['promo_code']=create_random_str(5,'license_plate');
        if($promo_code){
            $referrerUser = static::where('promo_code',$promo_code)->find();
            if ($referrerUser) {
                $saveData['referrer_id'] = $referrerUser->user_id;
//                $saveData['referrer2_id'] = $referrerUser->referrer_id;
//                $saveData['referrer3_id'] = $referrerUser->referrer2_id;
            }
        }
        self::create($saveData);
    }
}