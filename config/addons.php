<?php

return [

    // 自动加载，插件命令行模式下必须
    'autoload'       => true,

    // 默认插件入口路径
    'path'           => 'addons',
    'hooks' => [
        // 可以定义多个钩子
        'testhook'=>'store', // 键为钩子名称，用于在业务中自定义钩子处理，值为实现该钩子的插件，
        // 多个插件可以用数组也可以用逗号分
        'payhook'=>'pay'
    ],
    // 默认方法
//    'default_action' => 'index',
    'route' => [],
    'service' => [],
];