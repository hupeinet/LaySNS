<?php

namespace anerg\OAuth2\Gateways;

use anerg\OAuth2\Connector\Gateway;
use anerg\OAuth2\Helper\Str;

class Dingtalk extends Gateway
{
    const RSA_PRIVATE = 1;
    const RSA_PUBLIC  = 2;

    const API_BASE            = 'https://oapi.dingtalk.com/';
    protected $AuthorizeURL   = 'https://oapi.dingtalk.com/connect/qrconnect';
    protected $AccessTokenURL = 'https://oapi.dingtalk.com/sns/getuserinfo_bycode';
    /**
     * 得到跳转地址
     */
    public function getRedirectUrl()
    {
        $params = [
            'appid'       => $this->config['app_id'],
            'redirect_uri' => $this->config['callback'],
            'response_type'        => 'code',
            'scope'        => $this->config['scope'],
            'state'        => $this->config['state'],
        ];
        return $this->AuthorizeURL . '?' . http_build_query($params);
    }

    /**
     * 获取当前授权用户的openid标识
     */
    public function openid()
    {
        $this->getToken();

        if (isset($this->token['openid'])) {
            return $this->token['openid'];
        } else {
            throw new \Exception('没有获取到支付宝用户ID！');
        }
    }

    /**
     * 获取格式化后的用户信息
     */
    public function userinfo()
    {
        $rsp = $this->userinfoRaw();

        $userinfo = [
            'openid'  => $rsp['openid'],
            'unionid'  => $rsp['unionid'],
            'channel' => 'dingtalk',
            'gender'  => isset($rsp['gender'])?$rsp['gender']:'',
            'avatar'  => isset($rsp['avatar'])?$rsp['avatar']:'',
            'nick'    => $rsp['nick'],
        ];
        return $userinfo;
    }

    /**
     * 获取原始接口返回的用户信息
     */
    public function userinfoRaw()
    {
//        $this->getToken();

        $rsp = $this->call('sns/getuserinfo_bycode');
//      ^ array:3 [▼
//  "errcode" => 0
//  "errmsg" => "ok"
//  "user_info" => array:5 [▼
//    "nick" => "张宗朋"
//    "unionid" => "CBKPP201XwD0VnaIOLOYUAiEiE"
//    "dingId" => "$:LWCP_v1:$Pk9gvso8B9PjE4L362dMTQ=="
//    "openid" => "8zM0QTM9pBJJcHvve9aZQgiEiE"
//    "main_org_auth_high_level" => true
//  ]
//]
        if($rsp['errcode']==0){
            return $rsp['user_info'];
        }else{
            throw new \Exception($rsp['errmsg']);
        }

    }

    /**
     * 发起请求
     *
     * @param string $api
     * @param array $params
     * @param string $method
     * @return array
     */
    private function call($api, $params = [], $method = 'POST')
    {
        $time=$this->getMicrotime();
        $_params = [
            'accessKey'     => $this->config['app_id'],
            'timestamp'    => $time,
            'signature'     => $this->signature($time),
        ];
        $url=self::API_BASE.$api.'?'.http_build_query($_params);
        $data = $this->$method($url, ['tmp_auth_code'=> isset($_GET['code']) ? $_GET['code'] : ''],[],1);
        return json_decode($data, true);
    }


    /**
     * 时间戳 - 精确到毫秒
     * @return float
     */
    public function getMicrotime()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }


    /**
     * 支付宝签名
     */
    private function signature($time)
    {
        // 根据timestamp, appSecret计算签名值
        $s = hash_hmac('sha256', $time, $this->config['app_secret'], true);
        return base64_encode($s);
    }
}
