<?php



namespace EasyAdmin\upload\driver;

use EasyAdmin\upload\FileBase;
use EasyAdmin\upload\driver\ftp\Oss;
use EasyAdmin\upload\trigger\SaveDb;

/**
 * Ftp上传
 * Class Local
 * @package EasyAdmin\upload\driver
 */
class Ftp extends FileBase
{

    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        $upload = Oss::instance($this->uploadConfig)
            ->save($this->completeFileUrl, $this->completeFilePath);
           if($upload['save']==true) {
               return [
                   'save' => true,
                   'msg'  => '上传成功',
                   'data'  => [
                       'upload_type'   => $this->uploadType,
                       'original_name' => $this->file->getOriginalName(),
                       'mime_type'     => $this->file->getOriginalMime(),
                       'file_ext'      => strtolower($this->file->getOriginalExtension()),
                       'url'           => $upload['url'],
                       'sha1'           => $this->hash,
                       'create_time'   => time(),
                   ]
               ];
           }else{
               return false;
           }
    }

}