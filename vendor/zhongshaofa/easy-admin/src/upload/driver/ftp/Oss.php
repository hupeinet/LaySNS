<?php



namespace EasyAdmin\upload\driver\ftp;

use EasyAdmin\upload\interfaces\OssDriver;
use OSS\Core\OssException;
use FtpClient\FtpClient;

class Oss implements OssDriver
{

    protected static $instance;

    protected $accessKeyId;

    protected $port;

    protected $endpoint;

    protected $remoteFile;

    protected $domain;

    protected $ossClient;

    protected function __construct($config)
    {
        $this->host = $config['host'];
        $this->port = $config['port'];
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->domain = $config['domain'];

        $this->ossClient = new FtpClient();
        $this->ossClient->connect($this->host,false,$this->port);
        $this->ossClient->login($this->username, $this->password);
        return $this;
    }

    public static function instance($config)
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($config);
        }
        return self::$instance;
    }

    public function save($remoteFile,$localFile)
    {
        $this->remoteFile=$remoteFile;
        try {
            $link=$this->ossClient->getConnection();
            ftp_pasv($link, true);
            $this->ossClient->mkdir(pathinfo($remoteFile,PATHINFO_DIRNAME),true);
            $upload=ftp_put($link, $remoteFile, $localFile, FTP_BINARY);
        } catch (\Exception $e) {
            return [
                'save' => false,
                'msg'  => $e->getMessage(),
            ];
        }
        if ($upload!==true) {
            return [
                'save' => false,
                'msg'  => '保存失败',
            ];
        }
        return [
            'save' => true,
            'msg'  => '上传成功',
            'url'  => $this->domain.$this->remoteFile,
        ];
    }

}