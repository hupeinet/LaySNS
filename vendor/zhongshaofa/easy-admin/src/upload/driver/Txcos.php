<?php



namespace EasyAdmin\upload\driver;

use EasyAdmin\upload\FileBase;
use EasyAdmin\upload\driver\txcos\Cos;
use EasyAdmin\upload\trigger\SaveDb;

/**
 * 腾讯云上传
 * Class Txcos
 * @package EasyAdmin\upload\driver
 */
class Txcos extends FileBase
{

    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        $upload = Cos::instance($this->uploadConfig)
            ->save($this->completeFileUrl, $this->completeFilePath);
        $this->rmLocalSave();
        if ($upload['save'] == true) {
            return [
                'save' => true,
                'msg'  => '上传成功',
                'data'  => [
                    'upload_type'   => $this->uploadType,
                    'original_name' => $this->file->getOriginalName(),
                    'mime_type'     => $this->file->getOriginalMime(),
                    'file_ext'      => strtolower($this->file->getOriginalExtension()),
                    'url'           => $upload['url'],
                    'sha1'           => $this->hash,
                    'create_time'   => time(),
                ]
            ];
        }else{
            return false;
        }
    }

}